function H = tightbindinghamiltonian(L,v,varargin)

% default option
pbc = false;

% parsing option
while ~isempty(varargin)
    switch varargin{1}
        case 'pbc'
            pbc = varargin{2};
            varargin(1:2) = [];
        otherwise
            disp2(varargin{1});
            error('ERR: Unknown input');
    end
end

H = zeros(L,L);
for i = 1:L-1
    H(i,i+1) = -v;
  H(i+1,i) = -v;
end

if pbc
    H(1,end) = -v;
    H(end,1) = -v;
end

end
