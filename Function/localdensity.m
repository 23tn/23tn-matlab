function dens = localdensity(U,N)
    UN = U(:,1:N);
    dens = diag(UN*UN');
end
