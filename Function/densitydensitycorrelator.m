function densdens = densitydensitycorrelator(U,N)

% square of lower truncated matrix
ULN = U(:,1:N);
UL2 = ULN*ULN.';

% square of upper truncated matrix
UUN = U(:,N+1:end);
UU2 = UUN*UUN.';

% calculate density-density correlation (Eq. 44 in solution)
densdens = zeros(size(U,1),1);
ih = floor(size(U,1)/2);
for i = 1:size(U,1)
	densdens(i) = UL2(i,i)*UL2(ih,ih) + UU2(i,ih)*UL2(ih,i);
end

end