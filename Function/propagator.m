function propag = propagator(E,U,N,t)
    UN = U(:,N+1:end);
    % This is the expression given in [iv].
    propag = UN * diag(exp(-1i * E(N+1:end) * t)) * UN';
end