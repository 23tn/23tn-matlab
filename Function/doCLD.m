function [ff,gg] = doCLD (ozin,RhoV2in,Lambda,N,varargin)
% < Description >
%
% [ff,gg] = doCLD (ozin,RhoV2in,Lambda,N [, option] )
%
% This function performs the Campo--Oliveira logarithmic discretization [V.
% L. Campo and L. N. Oliveira, Phys. Rev. B 72, 104432 (2005)] of the input
% hybridization function (paramterized by the inputs 'ozin' and 'RhoV2in')
% and maps the resulting star-geometry Hamiltonian onto the chain-geometry
% Hamiltonian, so-called the Wilson chain. The output 'ff' and 'gg'
% describe the hopping amplitudes and the on-site energies of the chain.
%
% < Input >
% ozin, Hybin : [numeric vector] The values of definining the hybridization
%       function \Delta(\omega). RhoV2in(n) is the value of the
%       hybridization function at frequency ozin(n). The first and last
%       elements of ozin define the bandwidth of the bath.
% N : [integer] Wilson chain length.
% Lambda : [numeric] Discretization parameter.
%
% < Option >
% 'estep', ... : [numeric] Number of step for resolving frequencies for
%       each discretization interval.
%       (Default: 10)
% 'emax', ... : [numeric] Maximum frequency value in defining the
%       hybridization function.
%       (Default: max(abs(ozin)))
% 'emin', ... : [numeric] Minimum frequency value in defining the
%       hybridization function.
%       (Default: 1e-5*eps)
% 
% < Output >
% ff : [vector] Hopping amplitudes of the Wilson chain. The hopping
%       amplitudes correspond to the 1st diagonals (i.e., diag(..,+1) and
%       diag(..,-1)] of the tridiagonal matrix representation of the Wilson
%       chain Hamiltonian. ff(L) means the hopping between the (L-1)th and
%       Lth site. E.g., ff(L=1) means the hopping between the impurity
%       (L=0) and the first (L=1) bath site, and ff(L=2) means the hopping
%       between the first (L=1) and second (L=2) bath sites. For more
%       details, refer to the lecture note [NRG-I.5].
% gg : [vector] On-site energies of the Wilson chain. The on-site
%       energies correspond to the main diagonal of the Wilson chain
%       Hamiltonian. gg(L) means the on-site energy of the Lth bath site.
%       E.g., gg(L=1) means the on-site energy of the first (L=1) bath
%       site, and gg(L=2) means the on-site energy of the second (L=2) bath
%       site. For more details, refer to the lecture note [NRG-I.5].
%
% Written by S.Lee (May 5,2017); edited by S.Lee (May 9,2017)
% Revised by S.Lee (Jun.15,2020): Revised for SoSe 2020.
% Updated by J.Shim (Jun.25,2022): Revised for SoSe 2022.
% Revised by J.Shim (Jul.26.2022): Considered ff(itN) < 0 for itN < N.
% Revised by J.Shim (Jul.19,2023): Update explanation of ff and gg.
%   Now the way to apply the Lanczos method in doCLD is consistent with the
%   way explained in the lecture note and the tutorial solution.

% default parameter
estep = 10;
emax = max(abs(ozin));
emin = 1e-5*eps;

while ~isempty(varargin)
    switch varargin{1}
        case 'estep'
            estep = varargin{2};
            varargin = varargin(3:end);
        case 'emax'
            emax = varargin{2};
            varargin = varargin(3:end);
        case 'emin'
            emin= varargin{2};
            varargin = varargin(3:end);
        otherwise
            error('ERR: check input!');
    end
end

% parsing input
if isempty(ozin)
    error('ERR: Empty frequency input (1st input)');
elseif isempty(RhoV2in)
    error('ERR: Empty hybridization input (2nd input)');
elseif numel(ozin) ~= numel(RhoV2in)
    error('ERR: Different # of elements between frequency and hybridization inputs (1st & 2nd)');
end

% logarithmic grid
xs = ((log(emax)/log(Lambda)*estep):-1:(log(emin)/log(Lambda)*estep))/estep;
xs = flipud(xs(:)); % increasing, column
oz = Lambda.^xs; % increasing

rho1 = interp1(ozin,RhoV2in,+oz,'linear','extrap');
rho1(rho1<0) = 0; % to ensure positivity
[repE1,repT1] = doCLD_1side (oz,rho1,estep);

rho2 = interp1(ozin,RhoV2in,-oz,'linear','extrap');
rho2(rho2<0) = 0; % to ensure positivity
[repE2,repT2] = doCLD_1side (oz,rho2,estep);

if (numel(repE1)+numel(repE2)) < N
    fprintf(['WRN: Number of discretization intervals (= ', ...
        sprintf('%i',numel(repE1)+numel(repE2)),') is smaller than the chain length N (= ', ...
        sprintf('%i',N),'\n']);
    N = numel(repE1) + numel(repE2);
end

% % Lanczos tridiagonalization
% star-geometry Hamiltonian
Xis = [flipud(repE1); 0; -repE2];
Gammas = [flipud(sqrt(repT1)); 0; sqrt(repT2)];
H = diag(Xis);
id = numel(repE1)+1;
H(:,id) = Gammas;
H(id,:) = Gammas';

% Lanczos method: Refer to the Summary in the lecture note [DMRG.2] and the
% tutorial solution for (c) Lanczos method of Single-site Density Matrix
% Renormalization group 
%
% Step 1. initialize Cinit
Cinit = zeros(size(H,1),1);
Cinit(id,1) = 1;

% Krylov vectors
Ckr = zeros(numel(Cinit),N);
Ckr(:,1) = Cinit;

ff = zeros(N,1); % 1st diagonal; corresponds to the hopping amplitudes of
                 % the Wilson chain Hamiltonian
gg = zeros(N,1); % main diagonal; corresponds to the on-site energies of
                 % the Wilson chain Hamiltonian

for itN = (1:N+1)
    % Step 2. contract Hamiltonian with ket vector
    Ctmp = H*Ckr(:,itN);
    
    if itN > 1, gg(itN-1) = Ckr(:,itN)'*Ctmp; end % main diagonal element; "on-site energy"
    
    if itN < (N+1)
        % Step 3. orthogonalize Ctmp w.r.t. the previous ket vectors Ckr(:1) to Ckr(:,itN)
        Ctmp = Ctmp - Ckr(:,(1:itN))*(Ckr(:,(1:itN))'*Ctmp);
        % twice, to reduce numeical noise
        Ctmp = Ctmp - Ckr(:,(1:itN))*(Ckr(:,(1:itN))'*Ctmp);
        
        % Step 4. norm
        ff(itN) = norm(Ctmp); % 1st diagonal element; "hopping amplitude"
        
        if ff(itN) > 0
            Ckr(:,itN+1) = Ctmp/ff(itN);
        else
            % stop iteration; truncate ff, gg
            fprintf(['WRN: ff(',sprintf('%i',itN),') = 0 so the chain length is set ',...
            sprintf('%i',itN),' < N (= ',sprintf('%i',N),')\n']);
            ff(itN:end) = [];
            gg(itN+1:end) = [];
            Ckr(:,(itN+1):end) = [];
            break
        end
    end
end

end


function [repE,repT] = doCLD_1side (oz,rho,nstep)
% Obtain the representative energies (repE, \mathcal{E} in Campo & Oliveira) and
% the integral of the hybridization function (repT) for each discretization
% interval, for either positive or negative energy side.

ids = (numel(rho):-nstep:1); % index of oz and rho at the points of the discretization grids

repT = zeros(numel(ids)-1,1);
repE = zeros(size(repT));

for itx = (1:numel(repT))
    % compute the integrals for each interval
    ozp = oz(ids(itx+1):ids(itx));
    rhop = rho(ids(itx+1):ids(itx));
    repT(itx) = sum((rhop(2:end)+rhop(1:end-1)).*(ozp(2:end)-ozp(1:end-1)))/2;
    repE(itx) = (rhop(end)-rhop(1)) + ...
            sum( (ozp(2:end).*rhop(1:end-1) - ozp(1:end-1).*rhop(2:end)) ./ ...
                (ozp(2:end) - ozp(1:end-1)) .* log(abs(ozp(2:end)./ozp(1:end-1))) );
end

repE = repT./repE;

end