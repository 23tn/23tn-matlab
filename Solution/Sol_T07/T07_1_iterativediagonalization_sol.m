% test NRG_IterDiag
clear

% Hamiltonian parameters
Gamma = 2.5e-5; % hybridization function
U = 2e-3; % local interaction
epsd = -U/2; % d-level position

% NRG parameters
Lambda = 2; % discretization parameter
L = 85; % Wilson chain length
Nkeep = 300; % # of kept states

% construct local operators
[F,Z,S,I] = getLocalSpace('FermionS');

% particle number operator
NF = cat(3,contract(conj(F(:,1,:)),3,[1 2],F(:,1,:),3,[1 2]), ...
           contract(conj(F(:,2,:)),3,[1 2],F(:,2,:),3,[1 2]));

% impurity Hamiltonian
H0 = U*(NF(:,:,1)*NF(:,:,2)) + epsd*(NF(:,:,1)+NF(:,:,2));

% ket tensor for the impurity
A0 = getIdentity(1,2,I,2); % 1 for dummy leg

% logarithmic discretization
[ff,gg] = doCLD([-1 1],[1 1]*Gamma,Lambda,L);


% NRG iterative diagonalization
Inrg = NRG_IterDiag(H0,A0,Lambda,ff,F,gg,sum(NF,3),Z,Nkeep);

%%
% Eflow: Bath without impurity

Eshow = 3; % energy window to show (from 0 to Eshow)

Ebatheven = cell(1,floor(L/2));
Ebathodd = cell(1,floor((L+1)/2));
for itL = (1:L)
    Hbath = zeros(itL); % single-particle bath Hamiltonian of length itL
    Hbath = Hbath + diag(ff(2:itL),1);
    Hbath = Hbath + diag(ff(2:itL),-1);
    Hbath = Hbath + diag(gg(1:itL));
    Ebath = eig(Hbath); % single-particle energy spectrum
    Ebath = Ebath/Inrg.EScale(itL+1); % rescale the energy
    % consider single-particle excitation energy within Eshow
    Ebath = Ebath(logical((-Eshow < Ebath).*(Ebath < Eshow)));
    if mod(itL,2) == 0
        itL2 = floor(itL/2); % count even steps: 2,4,6,...
        Ebatheven{itL2} = 0;
        for itE = (1:numel(Ebath)) % count (itE)th level excitation
            % element of Ebath > 0: E for creating an electron
            % element of Ebath < 0: |E| for creating a hole
            Ebatheven{itL2} = kron(Ebatheven{itL2},ones(2,1)) ...
            + kron(ones(size(Ebatheven{itL2})),abs([0;Ebath(itE)]));
        end
        % count spin-up and -down
        Ebatheven{itL2} = kron(Ebatheven{itL2},ones(size(Ebatheven{itL2}))) ...
            + kron(ones(size(Ebatheven{itL2})),Ebatheven{itL2});
        Ebatheven{itL2} = sort(Ebatheven{itL2});
    else
        itL2 = floor((itL+1)/2); % count odd steps: 1,3,5,...
        Ebathodd{itL2} = 0;
        for itE = (1:numel(Ebath)) % count (itE)th level excitation
            % element of Ebath > 0: E for creating an electron
            % element of Ebath < 0: |E| for creating a hole
            Ebathodd{itL2} = kron(Ebathodd{itL2},ones(2,1)) ...
            + kron(ones(size(Ebathodd{itL2})),abs([0;Ebath(itE)]));
        end
        % count spin-up and -down
        Ebathodd{itL2} = kron(Ebathodd{itL2},ones(size(Ebathodd{itL2}))) ...
            + kron(ones(size(Ebathodd{itL2})),Ebathodd{itL2});
        Ebathodd{itL2} = sort(Ebathodd{itL2});
    end
end

% % % Energy flow diagram: START % % %
Ebatheven = cellfun(@(x) x(x <= Eshow), Ebatheven, 'UniformOutput', 0);
maxEeven = max(cellfun('prodofsize',Ebatheven));
Ebatheven = cellfun(@(x) [x;nan(maxEeven-numel(x),1)], Ebatheven, 'UniformOutput', 0);
Ebatheven = cell2mat(Ebatheven).';

Ebathodd = cellfun(@(x) x(x <= Eshow), Ebathodd, 'UniformOutput', 0);
maxEodd = max(cellfun('prodofsize',Ebathodd));
Ebathodd = cellfun(@(x) [x;nan(maxEodd-numel(x),1)], Ebathodd, 'UniformOutput', 0);
Ebathodd = cell2mat(Ebathodd).';

figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 500 450]);
set(gcf,'paperunits','points');
set(gcf,'papersize',[500 450]);
set(gcf,'paperpositionmode','manual');
set(gcf,'paperposition',[0 0 500 450]);
% upper panel
subplot(2,1,1);
set(gca,'units','points');
set(gca,'innerposition',[50 280 400 150]);
plot((2:2:L),Ebatheven,'LineWidth',2);
xlabel('Even iterations','fontsize',18);
xlim([0 L]);
ylim([0 Eshow]);
set(gca,'LineWidth',1,'FontSize',18);
%lower panel
subplot(2,1,2);
set(gca,'units','points');
set(gca,'innerposition',[50 50 400 150]);
plot((1:2:L),Ebathodd,'LineWidth',2);
xlabel('Odd iterations');
xlim([0 L]);
ylim([0 Eshow]);
set(gca,'LineWidth',1,'FontSize',18);
% % % Energy flow diagram: END % % %

%%
% Eflow: Bath without impurity and 1st site

Eshow = 3; % energy window to show (from 0 to Eshow)

Ebatheven2 = cell(1,floor(L/2));
Ebathodd2 = cell(1,floor((L+1)/2));
for itL = (2:L) % now Wilson chain begins from 2nd site
    Hbath = zeros(itL-1); % single-particle bath Hamiltonian of length itL-1
    Hbath = Hbath + diag(ff(3:itL),1);
    Hbath = Hbath + diag(ff(3:itL),-1);
    Hbath = Hbath + diag(gg(2:itL));
    Ebath = eig(Hbath); % single-particle energy spectrum
    Ebath = Ebath/Inrg.EScale(itL+1); % rescale the energy
    % consider single-particle excitation energy within Eshow
    Ebath = Ebath(logical((-Eshow < Ebath).*(Ebath < Eshow)));
    if mod(itL,2) == 0
        itL2 = floor(itL/2); % count even steps: 2,4,6,...
        Ebatheven2{itL2} = 0;
        for itE = (1:numel(Ebath)) % count (itE)th level excitation
            % element of Ebath > 0: E for creating an electron
            % element of Ebath < 0: |E| for creating a hole
            Ebatheven2{itL2} = kron(Ebatheven2{itL2},ones(2,1)) ...
            + kron(ones(size(Ebatheven2{itL2})),abs([0;Ebath(itE)]));
        end
        % count spin-up and -down
        Ebatheven2{itL2} = kron(Ebatheven2{itL2},ones(size(Ebatheven2{itL2}))) ...
            + kron(ones(size(Ebatheven2{itL2})),Ebatheven2{itL2});
        Ebatheven2{itL2} = sort(Ebatheven2{itL2});
    else
        itL2 = floor((itL+1)/2); % count odd steps: 1,3,5,...
        Ebathodd2{itL2} = 0;
        for itE = (1:numel(Ebath)) % count (itE)th level excitation
            % element of Ebath > 0: E for creating an electron
            % element of Ebath < 0: |E| for creating a hole
            Ebathodd2{itL2} = kron(Ebathodd2{itL2},ones(2,1)) ...
            + kron(ones(size(Ebathodd2{itL2})),abs([0;Ebath(itE)]));
        end
        % count spin-up and -down
        Ebathodd2{itL2} = kron(Ebathodd2{itL2},ones(size(Ebathodd2{itL2}))) ...
            + kron(ones(size(Ebathodd2{itL2})),Ebathodd2{itL2});
        Ebathodd2{itL2} = sort(Ebathodd2{itL2});
    end
end

% % % Energy flow diagram: START % % %
Ebatheven2 = cellfun(@(x) x(x <= Eshow), Ebatheven2, 'UniformOutput', 0);
maxEeven = max(cellfun('prodofsize',Ebatheven2));
Ebatheven2 = cellfun(@(x) [x;nan(maxEeven-numel(x),1)], Ebatheven2, 'UniformOutput', 0);
Ebatheven2 = cell2mat(Ebatheven2).';

Ebathodd2 = cellfun(@(x) x(x <= Eshow), Ebathodd2, 'UniformOutput', 0);
maxEodd = max(cellfun('prodofsize',Ebathodd2));
Ebathodd2 = cellfun(@(x) [x;nan(maxEodd-numel(x),1)], Ebathodd2, 'UniformOutput', 0);
Ebathodd2 = cell2mat(Ebathodd2).';
Ebathodd2 = Ebathodd2(2:end,:);

figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 500 450]);
set(gcf,'paperunits','points');
set(gcf,'papersize',[500 450]);
set(gcf,'paperpositionmode','manual');
set(gcf,'paperposition',[0 0 500 450]);
% upper panel
subplot(2,1,1);
set(gca,'units','points');
set(gca,'innerposition',[50 280 400 150]);
plot((2:2:L),Ebatheven2,'LineWidth',2);
xlabel('Even iterations','fontsize',18);
xlim([0 L]);
ylim([0 Eshow]);
set(gca,'LineWidth',1,'FontSize',18);
%lower panel
subplot(2,1,2);
set(gca,'units','points');
set(gca,'innerposition',[50 50 400 150]);
plot((3:2:L),Ebathodd2,'LineWidth',2);
xlabel('Odd iterations');
xlim([0 L]);
ylim([0 Eshow]);
set(gca,'LineWidth',1,'FontSize',18);
% % % Energy flow diagram: END % % %

%%
% Eflow: SIAM

Eshow = 3; % energy window to show (from 0 to Eshow)

% % % Energy flow diagram: START % % %
% since we start from A0; consider the step for H0 as 0, i.e., even
Eeven = Inrg.EK(1:2:end);
Eeven = cellfun(@(x) x(x <= Eshow), Eeven, 'UniformOutput', 0);
maxEeven = max(cellfun('prodofsize',Eeven));
Eeven = cellfun(@(x) [x;nan(maxEeven-numel(x),1)], Eeven, 'UniformOutput', 0);
Eeven = cell2mat(Eeven).';

Eodd = Inrg.EK(2:2:end);
Eodd = cellfun(@(x) x(x <= Eshow), Eodd, 'UniformOutput', 0);
maxEodd = max(cellfun('prodofsize',Eodd));
Eodd = cellfun(@(x) [x;nan(maxEodd-numel(x),1)], Eodd, 'UniformOutput', 0);
Eodd = cell2mat(Eodd).';

figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 500 450]);
set(gcf,'paperunits','points');
set(gcf,'papersize',[500 480]);
set(gcf,'paperpositionmode','manual');
set(gcf,'paperposition',[0 0 500 450]);
% upper panel
subplot(2,1,1);
set(gca,'units','points');
set(gca,'innerposition',[50 280 400 150]);
% color FO regime
area([0 -2*log(max(U,Gamma))/log(Lambda)],[3 3],'linestyle','none','facecolor','#e6f5ff');
TK = sqrt(U*pi*Gamma/2)*exp(-pi*U/8/pi/Gamma+pi*pi*Gamma/2/U); % Kondo temperature
hold on
% put text 'FO'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'FO','interpreter','tex','fontsize',24,'color','#0072bd','edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [50-log(max(U,Gamma))/log(Lambda)/L*200 290 35 25];
% color LM regime
area([-2*log(max(U,Gamma))/log(Lambda) -2*log(TK)/log(Lambda)],[3 3],'linestyle','none',...
    'facecolor','#ffede6');
% put text 'LM'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'LM','interpreter','tex','fontsize',24,'color','#d95319','edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [25-log(max(U,Gamma))/log(Lambda)/L*400-log(TK)/log(Lambda)/L*400 290 35 25];
% put text 'SC'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'SC','interpreter','tex','fontsize',24,'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [25-log(TK)/log(Lambda)/L*400+200 290 35 25];
% plot Eflow
plot((1:2:numel(Inrg.EK))-1,Eeven,'LineWidth',2);
hold off
xlabel('Even iterations','fontsize',18);
xlim([0 numel(Inrg.EK)-1]);
ylim([0 Eshow]);
set(gca,'LineWidth',1,'FontSize',18);
%lower panel
subplot(2,1,2);
set(gca,'units','points');
set(gca,'innerposition',[50 50 400 150]);
% color FO regime
area([0 -2*log(max(U,Gamma))/log(Lambda)],[3 3],'linestyle','none','facecolor','#e6f5ff');
TK = sqrt(U*pi*Gamma/2)*exp(-pi*U/8/pi/Gamma+pi*pi*Gamma/2/U); % Kondo temperature
hold on
% put text 'FO'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'FO','interpreter','tex','fontsize',24,'color','#0072bd','edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [50-log(max(U,Gamma))/log(Lambda)/L*200 70 35 25];
% color LM regime
area([-2*log(max(U,Gamma))/log(Lambda) -2*log(TK)/log(Lambda)],[3 3],'linestyle','none',...
    'facecolor','#ffede6');
% put text 'LM'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'LM','interpreter','tex','fontsize',24,'color','#d95319','edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [25-log(max(U,Gamma))/log(Lambda)/L*400-log(TK)/log(Lambda)/L*400 70 35 25];
% put text 'SC'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'SC','interpreter','tex','fontsize',24,'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [25-log(TK)/log(Lambda)/L*400+200 60 35 25];
% plot Eflow
plot((2:2:numel(Inrg.EK))-1,Eodd,'LineWidth',2);
hold off
xlabel('Odd iterations');
xlim([0 numel(Inrg.EK)-1]);
ylim([0 Eshow]);
set(gca,'LineWidth',1,'FontSize',18);
% % % Energy flow diagram: END % % %