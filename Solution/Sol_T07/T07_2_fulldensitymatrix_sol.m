% SIAM: T dependence of static observables
clear

% Hamiltonian parameters
Gamma = 2.5e-5; % hybridization function
U = 2e-3; % local interaction
epsd = -U/2; % d-level position

% NRG parameters
Lambda = 2.5; % discretization parameter
L = 70; % Wilson chain length
Nkeep = 300; % # of kept states

% temperature and observables
Ts = 10.^(-12:.2:1); % 5 grid points per decade
DFs = zeros(size(Ts)); % double occupation at T
SSs = zeros(size(Ts)); % spin correlation at T

% construct local operators
[F,Z,S,I] = getLocalSpace('FermionS');

% particle number operator
NF = cat(3,contract(conj(F(:,1,:)),3,[1 2],F(:,1,:),3,[1 2]), ...
           contract(conj(F(:,2,:)),3,[1 2],F(:,2,:),3,[1 2]));

% impurity Hamiltonian
H0 = U*(NF(:,:,1)*NF(:,:,2)) + epsd*(NF(:,:,1)+NF(:,:,2));

% ket tensor for the impurity
A0 = getIdentity(1,2,I,2); % 1 for dummy leg

% logarithmic discretization
[ff,gg] = doCLD([-1 1],[1 1]*Gamma,Lambda,L);

% NRG iterative diagonalization
Inrg = NRG_IterDiag(H0,A0,Lambda,ff,F,gg,sum(NF,3),Z,Nkeep);

% % % compute static observables: START % % %
% % double occupation
DF = NF(:,:,1)*NF(:,:,2);
% DF_KK: DF projected onto KK sector at L0-th shell
% i.e., DF_KK{1} = P_K DF P_K at L0-th shell
DF_KK = cell(1,1);
DF_KK{1} = DF;

% % spin correlation function
% SS_XX{itL}: spin correlation function for the impurity and (itL)th bath
% site projected onto XX sector (XX=KK, DD) at (itL)th shell
% summing up SS_XX{itL} for XX = KK, DD and itL gives spin correlation
% function for impurity and all bath sites
SS_KK = cell(1,L);
SS_DD = cell(1,L);

% % project operators onto KK sectors
% double occupation
for itL = (1:L+1) % find L0
    if ~isempty(Inrg.AD{itL})
        L0 = itL-1;
        break
    end
end

for itL = (1:L0)
    AK = Inrg.AK{itL};
    if itL == 1
        DF_KK{1} = updateLeft([],[],AK,DF_KK{1},2,AK);
    else
        DF_KK{1} = updateLeft(DF_KK{1},2,AK,[],[],AK);
    end
end

% % spin correlation function
for itL = (1:L)
    % spin correlation function btw. impurity and (itL)th bath site
    for itL2 = (1:itL+1) % itL2 is ranged from impurity (itL2=1) to (itL)th site (itL2=itL+1)
        % initialize
        if itL2 == 1
            AK = Inrg.AK{itL2};
            % Simp: impurity spin operators
            Simp = updateLeft([],[],AK,permute(S,[3 2 1]),3,AK);
        elseif itL2 < itL+1
            AK = Inrg.AK{itL2};
            Simp = updateLeft(Simp,3,AK,[],[],AK);
        elseif itL2 == itL+1
            AK = Inrg.AK{itL2};
            AD = Inrg.AD{itL2};
            % spin correlation function btw. impurity and (itL)th bath site
            SS_KK{itL} = updateLeft(Simp,3,AK,S,3,AK); % at KK sector
            SS_DD{itL} = updateLeft(Simp,3,AD,S,3,AD); % at DD sector
        end
    end
end

% % compute observables at T
for itT = (1:numel(Ts))
    % full density matrix
    T = Ts(itT);
    Inrg = getRhoFDM(Inrg,T);
    % double occupation
    RK = Inrg.RK{L0};
    DFs(itT) = trace(RK*DF_KK{1});
    % spin correlation function
    for itL = (1:L)
        if ~isempty(SS_KK{itL}) && ~isempty(Inrg.RK{itL})
            RK = Inrg.RK{itL+1};
            SSs(itT) = SSs(itT) + trace(RK*SS_KK{itL});
        end
        if ~isempty(SS_DD{itL}) && ~isempty(Inrg.RD{itL})
            RD = diag(Inrg.RD{itL+1});
            SS_D = SS_DD{itL};
            SSs(itT) = SSs(itT) + trace(RD*SS_DD{itL});
        end
    end
end
% % % compute static observables: END % % %

% display results
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 300]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 300]);
set(gca,'units','points');
set(gca,'innerposition',[100,60,300,225]);
% color FO regime
area([max(U,Gamma) max(Ts)],[-0.75 -0.75],'linestyle','none','facecolor','#e6f5ff');
hold on
area([max(U,Gamma) max(Ts)],[0.25 0.25],'linestyle','none','facecolor','#e6f5ff');
TK = sqrt(U*pi*Gamma/2)*exp(-pi*U/8/pi/Gamma+pi*pi*Gamma/2/U); % Kondo temperature
% put text 'FO'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'FO','interpreter','tex','fontsize',24,'color','#0072bd','edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = ...
    [80+(log(max(U,Gamma)/min(Ts))+log(max(Ts)/min(Ts)))/log(max(Ts)/min(Ts))*150 100 35 25];
% color LM regime
area([TK max(U,Gamma)],[-0.75 -0.75],'linestyle','none','facecolor','#ffede6');
area([TK max(U,Gamma)],[0.25 0.25],'linestyle','none','facecolor','#ffede6');
% put text 'LM'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'LM','interpreter','tex','fontsize',24,'color','#d95319','edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = ...
    [80+(log(max(TK)/min(Ts))+log(max(U,Gamma)/min(Ts)))/log(max(Ts)/min(Ts))*150 100 35 25];
% color SC regime
area([min(Ts) TK],[-0.75 -0.75],'linestyle','none','facecolor','#ffffff');
hold on
area([min(Ts) TK],[0.25 0.25],'linestyle','none','facecolor','#ffffff');
% put text 'SC'
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'SC','interpreter','tex','fontsize',24,'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = ...
    [80+(log(min(Ts)/min(Ts))+log(TK/min(Ts)))/log(max(Ts)/min(Ts))*150 100 35 25];
plot1 = semilogx(Ts,DFs,'-','linewidth',2,'Color','#0072bd',...
    'displayname','$\langle n_{d\uparrow} n_{d\downarrow} \rangle$');
plot2 = semilogx(Ts,SSs,'--','linewidth',2,'Color','#d95319',...
    'displayname','$\langle \mathbf{S}_d \cdot \mathbf{S}_\mathrm{bath}\rangle$');
hold off
set(gca,'xscale','log');
xlabel('$T$','Interpreter','latex','fontsize',18);
xlim([min(Ts) max(Ts)]);
xticks(Ts(1:15:end));
ylim([-0.75 0.25]);
yticks([-0.75 -0.5 -0.25 0 0.25])
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend([plot1 plot2],'Interpreter','latex','fontsize',18,'location','east');