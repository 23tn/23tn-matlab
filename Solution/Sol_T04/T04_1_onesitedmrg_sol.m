%%
% Exact GS energy Eex of Hff from numerical diagonalization
% row indices: |1>,|2>,..
% column indces: <1|,<2|,..
clear
L = 100; % chain length
H1 = zeros(L,L); % Single-particle Hamiltonian
H1 = H1 - diag(ones(L-1,1),1); % |l><l+1| terms
H1 = H1 - diag(ones(L-1,1),-1); % |l+1><l| terms
[U,E] = eig(H1);
E = diag(E); % E(i): ith lowest eigenenergy
% Eex: GS energy
% GS: electrons occupy eigenstates with E<0
Eex = sum(E(E<=0));
% display result
sprintf('Exact GS energy Eex: %.6f',Eex)

%%
% GS and its energy of Hff from iterative diagonalization
[F,Z,I] = getLocalSpace('Fermion');
Nkeep = 50; % maximal # of states to keep
% % GS MPS for Hff: START % %
tol = Nkeep*100*eps; % numerical tolerance for degeneracy
H0 = I*0; % 1st site Hamiltonian
A0 = getIdentity(1,2,I,2); % 1st leg: dummy
MPSiter = cell(L,1); % GS MPS from iterative diagonalization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
MPSiter{1} = contract(A0,3,3,V,2,1);
Hprev = D;
for itL = (2:L)
    % Fermion aniihilation operator at the current site
    Fprev = updateLeft([],[],MPSiter{itL-1},F,3,MPSiter{itL-1});
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % hopping terms
    Hhop = -updateLeft(Fprev,3,Anow,permute(F,[3 2 1]),3,Anow);
    Hhop = Hhop + Hhop';
    Hnow = Hnow + Hhop;
    [V,D] = eig((Hnow+Hnow')/2);
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    % GS energy at the last step
    if itL == L
        Eiter = min(D);
    end
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    oks = (D <= (Etr + tol));
    if itL < L
        MPSiter{itL} = contract(Anow,3,3,V(:,oks),2,1);
    elseif itL == L
        % Choose GS at the last step
        MPSiter{itL} = contract(Anow,3,3,V(:,1),2,1);
    end
    Hprev = diag(D(oks));
    disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
        'NK=',sprintf('%i/%i',[size(MPSiter{itL},3),size(Hnow,2)])]);
end
% % GS MPS for Hff : END % %
% display result
sprintf('Eiter: %e',Eiter)
% err: Relative error
err = (Eiter-Eex)/abs(Eex);
sprintf('err: %.6f %%',100*err)

%%
% MPO representation of Hff and comparing ground state energies
% % MPO for Hff: START % %
W = cell(L,1); % MPO of Hff
% First site
W{1} = zeros(1,2,4,2); % ordering: left bottom right top
W{1}(1,:,2,:) = -squeeze(F)'; % -creation
W{1}(1,:,3,:) = -squeeze(F); % -annihilation
W{1}(1,:,4,:) = I;
% Last site
W{L} = zeros(4,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = I;
W{L}(2,:,1,:) = squeeze(F); % annihilation
W{L}(3,:,1,:) = squeeze(F)'; % creation
% Other sites
for itL = (2:L-1)
    W{itL} = zeros(4,2,4,2); % ordering: left bottom right top
    W{itL}(1,:,1,:) = I;
    W{itL}(2,:,1,:) = squeeze(F); % annihilation
    W{itL}(3,:,1,:) = squeeze(F)'; % creation
    W{itL}(4,:,2,:) = -squeeze(F)'; % -creation
    W{itL}(4,:,3,:) = -squeeze(F); % -annihilation
    W{itL}(4,:,4,:) = I;
end
% Compare Eiter and E_MPO = <MPS|H_MPO|MPS>
E_MPO = 1;
for itL = (1:L)
    E_MPO = updateLeft(E_MPO,3,MPSiter{itL},W{itL},4,MPSiter{itL});
end
% display result
sprintf('Eiter - E_MPO : %e',Eiter-E_MPO)
% % MPO for Hff: END % %

%%
% Shift isometry center for MPS from iterative diagonalization
Liso = 50; % isometry center site
MPSiso = MPSiter; % initialize MPSiso
% % get left canonical tensors : START % %
for itL = (1:Liso-1)
    [U,S,V] = svdTr(MPSiso{itL},3,[1 2],[],[]);
    MPSiso{itL} = U;
    MPSiso{itL+1} = contract(diag(S)*V,2,2,MPSiso{itL+1},3,1);
end
% % get left canonical tensors : END % %
% % get right canonical tensors : START % %
for itL = (L:-1:Liso+1)
    [U,S,V] = svdTr(MPSiso{itL},3,1,[],[]);
    MPSiso{itL} = V;
    MPSiso{itL-1} = contract(MPSiso{itL-1},3,3,U*diag(S),2,1);
end
% % get right canonical tensors : END % %
% Check left isometry for sites 1,..,49(=Liso-1)
for itL=(1:Liso-1)
    M = MPSiso{itL};
    MMdag = contract(M,3,[1 2],conj(M),3,[1 2]);
    I = getIdentity(M,3);
    sprintf('norm(|MMdag - I|) at site %d : %.2e',itL,norm(abs(I-MMdag)))
end
% Check right isometry for sites 51(=Liso+1),..,100(=L)
for itL=(Liso+1:L)
    M = MPSiso{itL};
    MMdag = contract(M,3,[2 3],conj(M),3,[2 3]);
    I = getIdentity(M,1);
    sprintf('norm(|MMdag - I|) at site %d : %.2e',itL,norm(abs(I-MMdag)))
end

%%
% Compute left and right environments
Hleft = 1; % left environment
Hright = 1; % right environment
% % update left environment : START % %
for itL = (1:Liso-1)
    Mleft = MPSiso{itL}; % ordering : left bottom right
    Wleft = W{itL}; % ordering : left bottom right up
    Hleft = updateLeft(Hleft,3,Mleft,Wleft,4,Mleft);
end
% % update left environment : END % %
% % update right environment : START % %
for itL = (L:-1:Liso+1)
    % note: leg orderings are changed to use updateLeft
    Mright = permute(MPSiso{itL},[3 2 1]); % ordering : right bottom left
    Wright = permute(W{itL},[3 2 1 4]); % ordering : right bottom left up
    Hright = updateLeft(Hright,3,Mright,Wright,4,Mright);
end
% % update right environment : END % %

%%
% Compute left and right environments
Cinit = MPSiso{Liso}; % C at isometry center
Hloc = W{Liso}; % W at isometry center
% HC: contracting Hleft, Hright, Wcenter, and Ccenter
HC = contract(Hleft,3,3,Cinit,3,1);
HC = contract(HC,4,[2 3],Hloc,4,[1 4]);
HC = contract(HC,4,[2 4],Hright,3,[3 2]);
% E: energy expectation value
E = contract(HC,3,[1 2 3],conj(Cinit),3,[1 2 3]);
% var: local energy variance
HEC = HC - E*Cinit; % HC - EC
var = contract(HEC,3,[1 2 3],conj(HEC),3,[1 2 3]); % var: |HC-EC|^2
% display result
sprintf('E : %e',E)
sprintf('Eiter - E : %e',Eiter-E)
sprintf('var : %e',var)

%%
% Diagonalizing H1s
% construct H1s
H1s = contract(Hleft,3,2,Hloc,4,1);
H1s = contract(H1s,5,4,Hright,3,2);
H1s = permute(H1s,[1 3 5 2 4 6]);
% Hdir: H1s in matrix form (to diagonalize)
Hdir = reshape(H1s,[size(H1s,1)*size(H1s,2)*size(H1s,3), ...
                    size(H1s,4)*size(H1s,5)*size(H1s,6)]);
% Edir: GS energy & Cdir: GS
[Cdir,Edir] = eig((Hdir+Hdir')/2);
[~,minid] = min(diag(Edir));
Edir = diag(Edir);
Edir = Edir(minid);
Cdir = Cdir(:,minid);
Cdir = reshape(Cdir,[size(H1s,1),size(H1s,2),size(H1s,3)]);
% vardir : variance from Cdir
HC = contract(Hleft,3,3,Cdir,3,1);
HC = contract(HC,4,[2 3],Hloc,4,[1 4]);
HC = contract(HC,4,[2 4],Hright,3,[3 2]);
HEC = HC - Edir*Cdir; % HC - EC
vardir = contract(HEC,3,[1 2 3],conj(HEC),3,[1 2 3]); % var: |HC-EC|^2
% display result
ratio = (Edir-Eiter)/abs(Eiter);
sprintf('(Edir - Eiter) / |Eiter| : %.6f %%',100*ratio)
ratio = (Edir-Eex)/abs(Eex);
sprintf('(Edir - Eex) / |Eex| : %.6f %%',100*ratio)
sprintf('vardir : %e',vardir)

%%
% Lanczos method
NLZs = (0:10); % number of Lanczos steps
ELZs = zeros(size(NLZs)); % energy specfic number of stpeps
varLZs = zeros(size(NLZs)); % variance at specfic number of steps
for itN = (1:numel(NLZs))
    N = NLZs(itN);
    [Ceff,Eeff] = eigs_1site(Hleft,Hloc,Hright,Cinit,'N',N);
    % energy
    ELZs(itN) = Eeff;
    % variance
    HC = contract(Hleft,3,3,Ceff,3,1);
    HC = contract(HC,4,[2 3],Hloc,4,[1 4]);
    HC = contract(HC,4,[2 4],Hright,3,[3 2]);
    HEC = HC - ELZs(itN)*Ceff; % HC - EC
    varLZs(itN) = contract(HEC,3,[1 2 3],conj(HEC),3,[1 2 3]); % var: |HC-EC|^2
end
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 770 270]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [770 270]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 770 270]);
subplot(1,3,1);
set(gca,'units','points');
set(gca,'innerposition',[100,50,150,200]);
plot(NLZs,ELZs,'-o','linewidth',2);
hold on;
plot(NLZs,ones(size(NLZs))*Edir,'--k','linewidth',2);
hold off;
xlabel('$N$','Interpreter','latex','fontsize',18);
ylabel('GS energy','Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
legend('$E_\mathrm{LZ}$','$E_\mathrm{dir}$','Interpreter','latex',...
    'fontsize',18,'location','northeast');
subplot(1,3,2);
set(gca,'units','points');
set(gca,'innerposition',[350,50,150,200]);
semilogy(NLZs,varLZs,'-o','linewidth',2);
hold on;
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    '$\Delta^\mathrm{loc,dir}_E=0$','interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [375 75 100 25];
hold off;
xlabel('$N$','Interpreter','latex','fontsize',18);
ylabel('local variance','Interpreter','latex','fontsize',18);
ylim([1e-6 1]);
set(gca,'FontSize',18);
legend('$\Delta^\mathrm{loc,LZ}_E$',...
    'Interpreter','latex','fontsize',18,'location','northeast');
subplot(1,3,3);
set(gca,'units','points');
set(gca,'innerposition',[600,50,150,200]);
semilogy(NLZs,(ELZs-Edir)./abs(ELZs),'-o','linewidth',2);
hold on;
semilogy(NLZs,(ELZs-Eex)/abs(Eex),'-x','linewidth',2);
semilogy(NLZs,ones(size(NLZs))*(Edir-Eex)/abs(Eex),'--k','linewidth',2);
hold off;
xlabel('$N$','Interpreter','latex','fontsize',18);
ylabel('relative error','Interpreter','latex','fontsize',18);
ylim([1e-10 1e0]);
yticks([1e-10 1e-8 1e-6 1e-4 1e-2 1e0]);
set(gca,'FontSize',18);
legend('$\frac{E_\mathrm{LZ}-E_\mathrm{dir}}{|E_\mathrm{LZ}|}$',...
    '$\frac{E_\mathrm{LZ}-E_\mathrm{ex}}{|E_\mathrm{ex}|}$', ...
    '$\frac{E_\mathrm{dir}-E_\mathrm{ex}}{|E_\mathrm{ex}|}$','Interpreter','latex',...
    'fontsize',18,'location','southwest');


%%
% Right-to-left sweep
Liso = L; % isometry center site
MPS = MPSiter; % initialize MPS
Hleft = cell(L,1); % left environments
Hright = cell(L,1); % right environments
Esweep = zeros(L,1); % energy under sweeping
% % get left canonical tensors : START % %
for itL = (1:Liso-1)
    [U,S,V] = svdTr(MPS{itL},3,[1 2],[],[]);
    MPS{itL} = U;
    MPS{itL+1} = contract(diag(S)*V,2,2,MPS{itL+1},3,1);
end
% % get left canonical tensors : END % %
% Check left isometry for sites 1,..,99(=Liso-1)
% for itL=(1:Liso-1)
%     M = MPS{itL};
%     MMdag = contract(M,3,[1 2],conj(M),3,[1 2]);
%     I = getIdentity(M,3);
%     sprintf('norm(|MMdag - I|) at site %d : %.2e',itL,norm(abs(I-MMdag)))
% end
% % compute left environments for 1,...,L-1 : START % %
for itL = (1:L-1)
    if itL == 1
        % "remove" the left leg (1st leg) which is dummy by permuting to the last
        W2 = permute(W{itL},[2 3 4 1]);
        Hleft{itL+1} = updateLeft([],[],MPS{itL},W2,3,MPS{itL});
    else
        Hleft{itL+1} = updateLeft(Hleft{itL},3,MPS{itL},W{itL},4,MPS{itL});
    end
end
% % compute left enivronments for 1,...,L-1 : END % %
% % Right-to-left sweep : START % %
for itL = (L:-1:1)
    % 1. update C_itL using Lanczos algorithm
    % Use eigs_1site to obtain the variationally chosen ket tensor
    % Ceff and energy expectation value Eeff
    [Ceff,Eeff] = eigs_1site(Hleft{itL},W{itL},Hright{itL},MPS{itL});
    
    Esweep(itL) = Eeff;
    
    % 2. Use SVD to move isometry center
    % update MPS{itN} and MPS{itN-1} by using Ceff, via SVD
    % decompose Ceff
    [UT,ST,MPS{itL}] = svdTr(Ceff,3,1,Nkeep,[]);
    % contract UT*ST with MPS{itN}, to update MPS{itN}
    if itL > 1
        MPS{itL-1} = contract(MPS{itL-1},3,3,UT*diag(ST),2,1);
    end
    
    % 3. Update right environment
    % update the Hamiltonian in effective basis
    T = permute(MPS{itL},[3 2 1]); % permute left<->right, to make use of updateLeft
    if itL == L
        % "remove" the right leg (3rd leg) of Hs{itN}, which is dummy,
        % by permuting to the last; MATLAB automatically suppresses the
        % trailing singleton dimensions
        W2 = permute(W{itL},[2 1 4 3]); % bottom-left-top (-right)
        Hright{itL-1} = updateLeft([],[],T,W2,3,T);
    elseif itL == 1
        continue
    else
        % permute left<->right for W{itL} as well, to make use of updateLeft
        W2 = permute(W{itL},[3 2 1 4]); % right-bottom-left-top
        Hright{itL-1} = updateLeft(Hright{itL},3,T,W2,4,T);
    end
    % 4. Go to site itL-1 (for-loop)
end
% % Right-to-left sweep : END % %
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf, 'paperunits', 'points');
set(gcf, 'papersize', [450 300]);
set(gcf, 'paperpositionmode', 'manual');
set(gcf, 'paperposition', [0 0 450 300]);
semilogy((1:L),(Esweep-Eex)/abs(Eex),'linewidth',2);
hold on;
semilogy((1:L),ones(L,1)*(Eiter-Eex)/abs(Eex),'--k','linewidth',2);
hold off;
xlabel('$\ell$','Interpreter','latex','fontsize',18);
ylabel('relative error','Interpreter','latex','fontsize',18);
ylim([1e-6 1e-1]);
yticks([1e-6 1e-5 1e-4 1e-3 1e-2 1e-1]);
set(gca,'FontSize',18);
set(gca,'units','points');
set(gca,'innerposition',[100,50,300,225]);
legend('$(E_\mathrm{sweep}-E_\mathrm{ex})/|E_\mathrm{ex}|$',...
    '$(E_\mathrm{iter}-E_\mathrm{ex})/|E_\mathrm{ex}|$','Interpreter','latex',...
    'fontsize',18,'location','southeast');

%%
% Left-to-right sweep
MPS2 = MPS; % initialize MPS
Esweep2 = zeros(L,1); % energy under sweeping
% % Left-to-right sweep : START % %
for itL = (1:L)
    [Ceff,Eeff] = eigs_1site(Hleft{itL},W{itL},Hright{itL},MPS2{itL});
    
    Esweep2(itL) = Eeff;
    
    % update M{itN} and M{itN+1} by using Ceff, via SVD
    % decompose Ceff
    [MPS2{itL},ST,VT] = svdTr(Ceff,3,[1 2],Nkeep,[]);
    % contract UT*ST with M{itN}, to update M{itN}
    if itL < L
        MPS2{itL+1} = contract(diag(ST)*VT,2,2,MPS2{itL+1},3,1);
    end
    
    % update the Hamiltonian in effective basis
    if itL == 1
        % "remove" the left leg (1st leg) of Hs{itN}, which is dummy,
        % by permuting to the last; MATLAB automatically suppresses the
        % trailing singleton dimensions
        W2 = permute(W{itL},[2 3 4 1]); % bottom-right-top (-left)
        Hleft{itL+1} = updateLeft([],[],MPS2{itL},W2,3,MPS2{itL});
    elseif itL == L
        Hleft{itL+1} = [];
    else
        Hleft{itL+1} = updateLeft(Hleft{itL},3,MPS2{itL},W{itL},4,MPS2{itL});
    end
end
% % Left-to-right sweep : END % %
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf, 'paperunits', 'points');
set(gcf, 'papersize', [450 300]);
set(gcf, 'paperpositionmode', 'manual');
set(gcf, 'paperposition', [0 0 450 300]);
semilogy((1:L),(Esweep2-Eex)/abs(Eex),'linewidth',2);
hold on;
semilogy((1:L),ones(L,1)*(Eiter-Eex)/abs(Eex),'--k','linewidth',2);
hold off;
xlabel('$\ell$','Interpreter','latex','fontsize',18);
ylabel('relative error','Interpreter','latex','fontsize',18);
ylim([1e-7 1e-1]);
yticks([1e-7 1e-6 1e-5 1e-4 1e-3 1e-2 1e-1]);
set(gca,'FontSize',18);
legend('$(E_\mathrm{sweep}-E_\mathrm{ex})/|E_\mathrm{ex}|$',...
    '$(E_\mathrm{iter}-E_\mathrm{ex})/|E_\mathrm{ex}|$','Interpreter','latex',...
    'fontsize',18,'location','east');
set(gca,'units','points');
set(gca,'innerposition',[100,50,300,225]);

%%
% 1s DMRG vs iterative diagonalization: Energy error
Nsweep = 100; % number of DMRG sweeps will be 2*Nsweep
Econv = 1e-10; % Convergence criterion for energy
[MPS1s,EGS,Esweep] = DMRG_1site(W,MPSiter,Nkeep,Nsweep,'Econv',Econv);
Esweep(isnan(Esweep)) = [];
Esweep = reshape(Esweep,L,numel(Esweep)/L);
% plot result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf, 'paperunits', 'points');
set(gcf, 'papersize', [450 300]);
set(gcf, 'paperpositionmode', 'manual');
set(gcf, 'paperposition', [0 0 450 300]);
semilogy((1:size(Esweep,2)/2),(Esweep(end,2:2:end)-Eex)/abs(Eex),'linewidth',2);
hold on;
semilogy((1:size(Esweep,2)/2),ones(size(Esweep,2)/2,1)*(Eiter-Eex)/abs(Eex),...
    '--k','linewidth',2);
hold off;
xlabel('iter. \#','Interpreter','latex','fontsize',18);
ylabel('relative error','Interpreter','latex','fontsize',18);
ylim([1e-9 1e-1]);
yticks([1e-9 1e-7 1e-5 1e-3 1e-1]);
set(gca,'FontSize',18);
legend('$(E_\mathrm{sweep}-E_\mathrm{ex})/|E_\mathrm{ex}|$',...
    '$(E_\mathrm{iter}-E_\mathrm{ex})/|E_\mathrm{ex}|$','Interpreter','latex',...
    'fontsize',18,'location','east');
set(gca,'units','points');
set(gca,'innerposition',[100,50,300,225]);

%%
% 1s DMRG vs iterative diagonalization: Local variance
variter = zeros(L,1); % local variace of MPSiter
var1s = zeros(L,1); % local variance from MPS1s
for itL=(1:L)
    Liso = itL; % isometry center site
    % initialize MPSs
    MPSiter2 = MPSiter; % initialize MPSiter
    MPS1s2 = MPS1s; % initialize MPS1s
    % % get left canonical tensors : START % %
    for itL2 = (1:Liso-1)
        [U,S,V] = svdTr(MPSiter2{itL2},3,[1 2],[],[]);
        MPSiter2{itL2} = U;
        MPSiter2{itL2+1} = contract(diag(S)*V,2,2,MPSiter2{itL2+1},3,1);
        [U,S,V] = svdTr(MPS1s2{itL2},3,[1 2],[],[]);
        MPS1s2{itL2} = U;
        MPS1s2{itL2+1} = contract(diag(S)*V,2,2,MPS1s2{itL2+1},3,1);
    end
    % % get left canonical tensors : END % %
    % % get right canonical tensors : START % %
    for itL2 = (L:-1:Liso+1)
        [U,S,V] = svdTr(MPSiter2{itL2},3,1,[],[]);
        MPSiter2{itL2} = V;
        MPSiter2{itL2-1} = contract(MPSiter2{itL2-1},3,3,U*diag(S),2,1);
        [U,S,V] = svdTr(MPS1s2{itL2},3,1,[],[]);
        MPS1s2{itL2} = V;
        MPS1s2{itL2-1} = contract(MPS1s2{itL2-1},3,3,U*diag(S),2,1);
    end
    % % get right canonical tensors : END % %
    Hliter = 1; % left environment for MPSiter
    Hriter = 1; % right environment for MPSiter
    Hl1s = 1; % left environment for MPS1s
    Hr1s = 1; % right environment for MPS1s
    % % update left environments : START % %
    for itL2 = (1:Liso-1)
        Mliter = MPSiter2{itL2}; % ordering : left bottom right
        Ml1s = MPS1s2{itL2}; % ordering : left bottom right
        Wleft = W{itL2}; % ordering : left bottom right up
        Hliter = updateLeft(Hliter,3,Mliter,Wleft,4,Mliter);
        Hl1s = updateLeft(Hl1s,3,Ml1s,Wleft,4,Ml1s);
    end
    % % update left environments : END % %
    % % update right environments : START % %
    for itL2 = (L:-1:Liso+1)
        % note: leg orderings are changed to use updateLeft
        Mriter = permute(MPSiter2{itL2},[3 2 1]); % ordering : right bottom left
        Mr1s = permute(MPS1s2{itL2},[3 2 1]); % ordering : right bottom left
        Wright = permute(W{itL2},[3 2 1 4]); % ordering : right bottom left up
        Hriter = updateLeft(Hriter,3,Mriter,Wright,4,Mriter);
        Hr1s = updateLeft(Hr1s,3,Mr1s,Wright,4,Mr1s);
    end
    % % update right environments : END % %
    % % get variances : START % %
    Citer = MPSiter2{Liso}; % C at isometry center for MPSiter
    C1s = MPS1s2{Liso}; % C at isometry center for MPS1s
    Hloc = W{Liso}; % W at isometry center
    % HC: contracting Hleft, Hright, Wcenter, and Ccenter
    HCiter = contract(Hliter,3,3,Citer,3,1);
    HCiter = contract(HCiter,4,[2 3],Hloc,4,[1 4]);
    HCiter = contract(HCiter,4,[2 4],Hriter,3,[3 2]);
    HC1s = contract(Hl1s,3,3,C1s,3,1);
    HC1s = contract(HC1s,4,[2 3],Hloc,4,[1 4]);
    HC1s = contract(HC1s,4,[2 4],Hr1s,3,[3 2]);
    % E: energy expectation value
    Eiter = contract(HCiter,3,[1 2 3],conj(Citer),3,[1 2 3]);
    E1s = contract(HC1s,3,[1 2 3],conj(C1s),3,[1 2 3]);
    % var: local energy variance
    HECiter = HCiter - Eiter*Citer; % HC - EC
    variter(itL) = contract(HECiter,3,[1 2 3],conj(HECiter),3,[1 2 3]); % var: |HC-EC|^2
    HEC1s = HC1s - E1s*C1s; % HC - EC
    var1s(itL) = contract(HEC1s,3,[1 2 3],conj(HEC1s),3,[1 2 3]); % var: |HC-EC|^2
    % % get variances : END % %
end
% plot result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf, 'paperunits', 'points');
set(gcf, 'papersize', [450 300]);
set(gcf, 'paperpositionmode', 'manual');
set(gcf, 'paperposition', [0 0 450 300]);
plot((1:L),variter,'linewidth',2);
hold on;
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05], 'String',...
    '$\Delta^\mathrm{loc,1s}_E=0$','interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [280 85 100 25];
hold off;
xlabel('$\ell$','Interpreter','latex','fontsize',18);
ylabel('local variance','Interpreter','latex','fontsize',18);
ylim([0 0.5]);
set(gca,'FontSize',18);
legend('$\Delta^\mathrm{loc,iter}_E$',...
    'Interpreter','latex','fontsize',18,'location','best');
set(gca,'units','points');
set(gca,'innerposition',[100,50,300,225]);