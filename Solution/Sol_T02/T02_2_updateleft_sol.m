clear

% bond dimensions
d = 4;
w = 6;
Dset = (100:20:300);

% number of trials to be averaged
num_avg = 100;

% computational time
time_C = zeros(size(Dset));

% changing bond dimensions
for itD = (1:numel(Dset))
    D = Dset(itD);
    % each trial
    for itn = (1:num_avg)
        A = rand(D,d,D);
        B = rand(D,d,D);
        C = rand(D,w,D);
        X = rand(w,d,w,d);
        tobj = tic2;
        [~] = updateLeft(C,3,B,X,4,A);
        [~,cput] = toc2(tobj); % get cpu time
        time_C(itD) = time_C(itD) + cput;
    end
end

% take an average
time_C = time_C/num_avg;

% plot the result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot(Dset,time_C,'ko','MarkerSize',6);
plot(Dset,time_C(end)*(Dset/max(Dset)).^3,'r--');
hold off;
box on;
grid on;
set(gca,'Xscale','log');
set(gca,'Yscale','log');
xlabel('$D$','Interpreter','latex','FontSize',16);
ylabel('average CPU time (sec.)','Interpreter','latex','FontSize',16);
set(gca,'FontSize',16);
set(gca,'XMinorTick','on');
set(gca,'TickLength',[0.03 0.025]);
legend('time for getting $C_\mathrm{new}$', '$\mathcal{O}(D^3)$',...
    'Interpreter','latex','FontSize',16,'Location','southeast');
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);