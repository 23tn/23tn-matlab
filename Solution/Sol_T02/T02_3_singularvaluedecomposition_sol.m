clear
help svd

%%
open svd

%%
M = rand(3,100);
[U,S,V] = svd(M,'econ');

%%
size(U)
size(S)
size(V')

%%
MMdag = M*M'; % M M^\dagger
MdagM = M'*M; % M^\dagger M
[U1,E] = eig(MMdag);
% sort eigenvalues and eigenvectors in the order of increasing eigenvalues
[E,ids] = sort(diag(E),'descend');
U1 = U1(:,ids);
S1 = sqrt(E);
[V2,E] = eig(MdagM);
% sort eigenvalues and eigenvectors in the order of increasing eigenvalues
[E,ids] = sort(diag(E),'descend');
V2 = V2(:,ids);
S2 = sqrt(E);
S2 = S2(1:3); % only 3 largest values are nonzero
V2 = V2(:,1:3);

%%
% U1, S1: U, S from M M^\dagger
% V2, S2: V, S from M^\dagger M
norm(S-diag(S1))
norm(S-diag(S2))
norm(abs(U)-abs(U1))
norm(abs(V')-abs(V2'))

%%
% matrix dimensions
m = 3;
nset = (10:10:500);

% number of trials to be averaged
% NOTE: num_avg = 1000 can take some time!
% You can decrease num_avg (but the result 
% could be less accurate)
num_avg = 1000;

% computational time
time_svd = zeros(size(nset));
time_eig1 = zeros(size(nset));
time_eig2 = zeros(size(nset));

% changing matrix dimensions
for itn = (1:numel(nset))
    n = nset(itn);
    for ita = (1:num_avg)
        M = rand(m,n);
        tobj = tic2;
        [~,~,~] = svd(M,'econ');
        [~,cput] = toc2(tobj); % get cpu time
        time_svd(itn) = time_svd(itn) + cput;
        MMdag = M*M';
        tobj = tic2;
        [~,~] = eig(MMdag);
        [~,cput] = toc2(tobj); % get cpu time
        time_eig1(itn) = time_eig1(itn) + cput;
        MdagM = M'*M;
        tobj = tic2;
        [~,~,~] = eig(MdagM);
        [~,cput] = toc2(tobj); % get cpu time
        time_eig2(itn) = time_eig2(itn) + cput;
    end
end

% take an average
time_svd = time_svd/num_avg;
time_eig1 = time_eig1/num_avg;
time_eig2 = time_eig2/num_avg;

%%
% plot the result
figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot(nset,time_svd,'ko','MarkerSize',6);
plot(nset,time_eig1,'bsquare','MarkerSize',6);
plot(nset,time_eig2,'r^','MarkerSize',6);
plot(nset,time_svd(end)*(nset/max(nset)),'k--');
plot(nset,mean(time_eig1)*ones(size(nset)),'b--');
plot(nset,time_eig2(end)*(nset/max(nset)).^3,'r--');
hold off;
box on;
grid on;
set(gca,'Xscale','log');
set(gca,'Yscale','log');
xlabel('$n$','Interpreter','latex','FontSize',16);
ylabel('average CPU time (sec.)','Interpreter','latex','FontSize',16);
set(gca,'FontSize',16);
set(gca,'XMinorTick','on');
set(gca,'TickLength',[0.03 0.025]);
legend('svd($M$)', 'eig($M M^\dagger$)', 'eig($M^\dagger M$)', ...
    '$\mathcal{O}(m^2n)$','$\mathcal{O}(n^3)$','$\mathcal{O}(m^3)$', ...
    'Interpreter','latex','FontSize',16,'Location','southeast');
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);