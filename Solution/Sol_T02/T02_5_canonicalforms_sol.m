clear
% bond dimensions
d = 4;
D = 300;

% random tensors
M1 = rand(D,d,D);
M2 = rand(D,d,d,D);

% decomposition (i)
M = reshape(M1,[D,d*D]);
[U,S,V] = svd(M,'econ');
Lambda = U*S;
B = V';
B = reshape(B,[size(S,2),d,D]);
% check the isometry property of B
BB = contract(B,3,[2 3],conj(B),3,[2 3]);
I = getIdentity(B,1);
sprintf('Difference between BB'' and the expected identity matrix: %.2e',...
    sum(abs(I-BB),'all'))
% verify the correctness of the decomposition
M1new = contract(Lambda,2,2,B,3,1);
sprintf('Difference between the original M1 and the reconstructed M1: %.2e',...
    sum(abs(M1-M1new),'all'))
% decomposition (ii)
M = reshape(M1,[D*d,D]);
[U,S,V] = svd(M,'econ');
Lambda = S*V';
A = reshape(U,[D,d,size(S,1)]);
% check the isometry property of A
AA = contract(A,3,[1 2],conj(A),3,[1 2]);
I = getIdentity(A,3);
sprintf('Difference between A''A and the expected identity matrix: %.2e',...
    sum(abs(I-AA),'all'))
% verify the correctness of the decomposition
M1new = contract(A,3,3,Lambda,2,1);
sprintf('Difference between the original M1 and the reconstructed M1: %.2e',...
    sum(abs(M1-M1new),'all'))
% decomposition (iii)
M = reshape(M2,[D*d,d*D]);
[U,S,V] = svd(M,'econ');
Lambda = S;
A = reshape(U,[D,d,size(S,1)]);
B = V';
B = reshape(B,[size(S,2),d,D]);
% check the isometry property of A
AA = contract(A,3,[1 2],conj(A),3,[1 2]);
I = getIdentity(A,3);
sprintf('Difference between A''A and the expected identity matrix: %.2e',...
    sum(abs(I-AA),'all'))
% check the isometry property of B
BB = contract(B,3,[2 3],conj(B),3,[2 3]);
I = getIdentity(B,1);
sprintf('Difference between BB'' and the expected identity matrix: %.2e',...
    sum(abs(I-BB),'all'))
% verify the correctness of the decomposition
M2new = contract(A,3,3,Lambda,2,1);
M2new = contract(M2new,3,3,B,3,1);
sprintf('Difference between the original M2 and the reconstructed M2: %.2e',...
    sum(abs(M1-M1new),'all'))

%%
clear
% % GS MPS from T02.3 (d) % %
L = 100; % maximum chain length
Nkeep = 300; % maximal number of states to keep
tol = Nkeep*100*eps; % numerical tolerance for degeneracy

[S,I] = getLocalSpace('Spin',1/2);
H0 = I*0; % Hamiltonian for only the 1st site
A0 = getIdentity(1,2,I,2); % 1st leg is dummy leg (vacuum)

% MPS of the GS
M = cell(L,1);

% initialization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
M{1} = contract(A0,3,3,V,2,1);
Hprev = D;

for itL = (2:L)
    % step [i-ii]
    % spin operator at the current site; to be used for generating
    % the coupling term at the next iteration
    Sprev = updateLeft([],[],M{itL-1},S,3,M{itL-1});
    
    % step [iii]
    % % add new site
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % update the Hamiltonian up to the last sites
    % to the enlarged Hilbert space
    
    % step [iv]
    % % spin-spin interaction
    Sprev_x = (Sprev(:,1,:)+Sprev(:,2,:))/sqrt(2);
    Sprev_y = (Sprev(:,1,:)-Sprev(:,2,:))/sqrt(2)/1i;
    Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
    Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
    Hxy = -updateLeft(Sprev_x,3,Anow,Sx,3,Anow) ...
        - updateLeft(Sprev_y,3,Anow,Sy,3,Anow);
    
    % step [v]
    Hnow = Hnow+Hxy;
    
    % diagonalize the current Hamiltonian
    [V,D] = eig((Hnow+Hnow')/2);
    % sort eigenvalues and eigenvectors in the order of increasing
    % eigenvalues
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    % % NOTE: tol is given
    oks = (D < (Etr + tol));
    % true: to keep, false: not to keep
    % keep all degenerate states up to tolerance
    
    if itL < L
        M{itL} = contract(Anow,3,3,V(:,oks),2,1);
    elseif itL == L
        M{itL} = contract(Anow,3,3,V(:,1),2,1); % select GS at the last step
    end
    Hprev = diag(D(oks));
    % you can activate below lines to display the progress
%     disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
%         'NK=',sprintf('%i/%i',[size(M{itL},3),size(Hnow,2)])]);
end
% % % %

% % random MPS % %
% L = 100;
% D = 300;
% d = 2;
% M = cell(L,1); % MPS; nth cell represents the nth tensor
% for itL = (1:L)
%     if itL == 1
%         M{itL} = rand(1,d,D);
%     elseif itL == L
%         M{itL} = rand(D,d,1);
%     else
%         M{itL} = rand(D,d,D);
%     end
% end
% % % %

for it = (1:L)
    % reshape M{it} and SVD
    T = M{it};
    T = reshape(T,[size(T,1)*size(T,2),size(T,3)]);
    [U,S,V] = svd(T,'econ');
    % reshape U into rank-3 tensor, and replace M{it} with it
    M{it} = reshape(U,[size(U,1)/size(M{it},2),size(M{it},2),size(U,2)]);
    if it < L
        % contract S and V' with M{it+1}
        M{it+1} = contract(S*V',2,2,M{it+1},3,1);
    else
        % R1: tensor which is the leftover after transforming the left
        % part. It will be SVD-ed and its left/right singular vectors 
        % will be contracted with the neighbouring M-tensors.
        R1 = S*V';
    end
end
[U,~,V] = svd(R1,'econ');
% V' is a single number which serves as the overall phase factor to the MPS.
% So we can pass over V' to U.
M{end} = contract(M{end},3,3,U*V',2,1);

%%
id = 50;
for it = (numel(M):-1:id+1)
    % reshape M{it} and SVD
    T = M{it};
    T = reshape(T,[size(T,1),size(T,2)*size(T,3)]);
    [U,S,V] = svd(T,'econ');
    % reshape V' into rank-3 tensor, replace M{it} with it
    M{it} = reshape(V',[size(V,2),size(M{it},2),size(V,1)/size(M{it},2)]);
    if it > (id+1)
        % contract U and S with M{it-1}
        M{it-1} = contract(M{it-1},3,3,U*S,2,1);
    else
        % R2: tensor which is the leftover after transforming the right
        % part. It will be contracted with the M tensor at the isometry
        % center.
        R2 = U*S;
    end
end
M{id} = contract(M{id},3,3,R2,2,1);

%%
for it = (numel(M):-1:1)
    % reshape M{it} and SVD
    T = M{it};
    T = reshape(T,[size(T,1),size(T,2)*size(T,3)]);
    [U,S,V] = svd(T,'econ');
    % reshape V' into rank-3 tensor, replace M{it} with it
    M{it} = reshape(V',[size(V,2),size(M{it},2),size(V,1)/size(M{it},2)]);
    if it > 1
        % contract U and S with M{it-1}
        M{it-1} = contract(M{it-1},3,3,U*S,2,1);
    else
        % R2: tensor which is the leftover after transforming the right
        % part. It will be contracted with the M tensor at the isometry
        % center.
        R2 = U*S;
    end
end
[U,~,V] = svd(R2,'econ');
% U is a single number which serves as the overall phase factor to the MPS.
% So we can pass over U to V'.
M{1} = contract(U*V',2,2,M{1},3,1);

%%
Sent_A = zeros(L-1,1); % entanglement entropy for rhoA
Sent_B = zeros(L-1,1); % entanglement entropy for rhoB
TrRhoA = zeros(L-1,1); % trace of rhoA
TrRhoB = zeros(L-1,1); % trace of rhoB

for it = (1:L-1)
    % % compute entanglement entropy for rhoA and trace of rhoA % %
    rho_A = contract(M{it},3,3,conj(M{it}),3,3);
    rho_A = reshape(rho_A,...
        [size(rho_A,1)*size(rho_A,2),size(rho_A,3)*size(rho_A,4)]);
    w = eig((rho_A+rho_A'))/2;
    w = w(w > 0);
    Sent_A(it) = sum(-w.*log(w));
    TrRhoA(it) = sum(w);
    % % % %

    % % compute entanglement entropy for rhoB and trace of rhoB % %
    rho_B = contract(M{it},3,[1 2],conj(M{it}),3,[1 2]);
    w = eig((rho_B+rho_B'))/2;
    w = w(w > 0);
    Sent_B(it) = sum(-w.*log(w));
    TrRhoB(it) = sum(w);
    % % % %

    % reshape M{it} and SVD
    T = M{it};
    T = reshape(T,[size(T,1)*size(T,2),size(T,3)]);
    [U,S,V] = svd(T,'econ');
    % reshape U into rank-3 tensor, and replace M{it} with it
    M{it} = reshape(U,[size(U,1)/size(M{it},2),size(M{it},2),size(U,2)]);
    if it < L
        % contract S and V' with M{it+1}
        M{it+1} = contract(S*V',2,2,M{it+1},3,1);
    else
        % R1: tensor which is the leftover after transforming the left
        % part. It will be SVD-ed and its left/right singular vectors 
        % will be contracted with the neighbouring M-tensors.
        R1 = S*V';
    end
end
[U,~,V] = svd(R1,'econ');
% V' is a single number which serves as the overall phase factor to the MPS.
% So we can pass over V' to U.
M{end} = contract(M{end},3,3,U*V',2,1);

% plot the result
figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot((1:L-1),Sent_A,'-o','MarkerSize',6);
plot((1:L-1),Sent_B,'--x','MarkerSize',6);
hold off;
box on;
grid on;
set(gca,'Xscale','lin');
set(gca,'Yscale','lin');
xlabel('$\ell$','Interpreter','latex','FontSize',16);
ylabel('$S_\mathrm{ent}$','Interpreter','latex','FontSize',16);
legend('for $\rho_A$', 'for $\rho_B$', ...
    'Interpreter','latex','FontSize',16,'Location','south');
set(gca,'FontSize',16);
set(gca,'XMinorTick','on');
set(gca,'TickLength',[0.03 0.025]);
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);

figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot((1:L-1),TrRhoA,'-o','MarkerSize',6);
plot((1:L-1),TrRhoB,'--x','MarkerSize',6);
hold off;
box on;
grid on;
set(gca,'Xscale','lin');
set(gca,'Yscale','lin');
xlabel('$\ell$','Interpreter','latex','FontSize',16);
ylabel('trace','Interpreter','latex','FontSize',16);
legend('for $\rho_A$', 'for $\rho_B$', ...
    'Interpreter','latex','FontSize',16,'Location','south');
ylim([.95 1.05]);
set(gca,'FontSize',16);
set(gca,'XMinorTick','on');
set(gca,'TickLength',[0.03 0.025]);
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);