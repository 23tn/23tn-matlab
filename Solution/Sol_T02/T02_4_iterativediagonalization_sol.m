clear
[S,I] = getLocalSpace('Spin',1/2);
% Sx = (S+ + S-)/2
% S(:,1,:) and (S,2,:) are S+/sqrt(2) and S-/sqrt(2)
Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
A = getIdentity(Sx,3,Sx,3);
T = contract(Sx,3,2,Sx,3,2);
T = contract(conj(A),3,[1 2],T,4,[1 3]); % complex conjugate on A
SxSx = contract(T,3,[2 3],A,3,[1 2]);

%%
EigVal = eig((SxSx+SxSx')/2) % Hermitianize

%%
% Sy = (S+ - S-)/2i
% S(:,1,:) and (S,2,:) are S+/sqrt(2) and S-/sqrt(2)
Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
T = contract(Sy,3,2,Sy,3,2);
T = contract(conj(A),3,[1 2],T,4,[1 3]); % complex conjugate on A
SySy = contract(T,3,[2 3],A,3,[1 2]);

%%
Hxy = -(SxSx+SySy);
EigVal = eig((Hxy+Hxy')/2) % Hermitianize

%%
L = 100; % maximum chain length
[S,I] = getLocalSpace('Spin',1/2);

% lowest energies at each iteration
E0 = zeros(1,L);

% initialization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
AK = contract(A0,3,3,V,2,1);
E0(1) = min(diag(D));
Hprev = D;

for itL = (2:L)
    % step [i-ii]
    % spin operator at the current site; to be used for generating
    % the coupling term at the next iteration
    Sprev = updateLeft([],[],AK,S,3,AK);
    
    % step [iii]
    % % add new site
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % update the Hamiltonian up to the last sites
    % to the enlarged Hilbert space
    
    % step [iv]
    % % spin-spin interaction
    Sprev_x = (Sprev(:,1,:)+Sprev(:,2,:))/sqrt(2);
    Sprev_y = (Sprev(:,1,:)-Sprev(:,2,:))/sqrt(2)/1i;
    Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
    Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
    Hxy = -updateLeft(Sprev_x,3,Anow,Sx,3,Anow) ...
        - updateLeft(Sprev_y,3,Anow,Sy,3,Anow);
    
    % step [v]
    Hnow = Hnow+Hxy;
    
    % diagonalize the current Hamiltonian
    [V,D] = eig((Hnow+Hnow')/2);
    % sort eigenvalues and eigenvectors in the order of increasing
    % eigenvalues
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    
    E0(itL) = min(D);
    
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    oks = (D <= Etr);
    % true: to keep, false: not to keep
    % keep all degenerate states up to tolerance
    
    AK = contract(Anow,3,3,V(:,oks),2,1);
    Hprev = diag(D(oks));
    
    % you can activate below lines to display the progress
%     disptime(['#',sprintf('%02i/%02i',[itN,N]),' : ', ...
%         'NK=',sprintf('%i/%i',[size(AK,3),size(Hnow,2)])]);
end

%%
Eiter = E0(end);
Eexact = (1/2)-1/(2*sin(pi/(2*(L+1)))); % exact GS energy for a given N

sprintf('GS energy (iterative diag): %.2f',Eiter)
sprintf('GS energy (exact result): %.2f',Eexact)

%%
sprintf('GS energy error: %.2f %%',100*abs(1-Eiter/Eexact))

%%
L = 100; % maximum chain length
[S,I] = getLocalSpace('Spin',1/2);

% lowest energies at each iteration
E0 = zeros(1,L);

% initialization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
AK = contract(A0,3,3,V,2,1);
E0(1) = min(diag(D));
Hprev = D;

for itL = (2:L)
    % step [i-ii]
    % spin operator at the current site; to be used for generating
    % the coupling term at the next iteration
    Sprev = updateLeft([],[],AK,S,3,AK);
    
    % step [iii]
    % % add new site
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % update the Hamiltonian up to the last sites
    % to the enlarged Hilbert space
    
    % step [iv]
    % % spin-spin interaction
    Sprev_x = (Sprev(:,1,:)+Sprev(:,2,:))/sqrt(2);
    Sprev_y = (Sprev(:,1,:)-Sprev(:,2,:))/sqrt(2)/1i;
    Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
    Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
    Hxy = -updateLeft(Sprev_x,3,Anow,Sx,3,Anow) ...
        - updateLeft(Sprev_y,3,Anow,Sy,3,Anow);
    
    % step [v]
    Hnow = Hnow+Hxy;
    
    % diagonalize the current Hamiltonian
    [V,D] = eig((Hnow+Hnow')/2);
    % sort eigenvalues and eigenvectors in the order of increasing
    % eigenvalues
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    
    E0(itL) = min(D);
    
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    % % NOTE: tol is given
    oks = (D <= (Etr + tol));
    % true: to keep, false: not to keep
    % keep all degenerate states up to tolerance
    
    AK = contract(Anow,3,3,V(:,oks),2,1);
    Hprev = diag(D(oks));
    
    % you can activate below lines to display the progress
%     disptime(['#',sprintf('%02i/%02i',[itN,N]),' : ', ...
%         'NK=',sprintf('%i/%i',[size(AK,3),size(Hnow,2)])]);
end

%%
Etol = E0(end);
sprintf('GS energy (with tol): %.2f',Etol)
sprintf('GS energy difference from tol: %.2f',Eiter-Etol)
sprintf('GS energy error (with tol): %.2f %%',100*abs(1-Etol/Eexact))

%%
L = 200; % maximum chain length
H0 = I*0; % Hamiltonian for only the 1st site
A0 = getIdentity(1,2,I,2); % 1st leg is dummy leg (vacuum)

[S,I] = getLocalSpace('Spin',1/2);

% lowest energies at even number of spins
E0 = zeros(1,L/2);

% initialization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
AK = contract(A0,3,3,V,2,1);
Hprev = D;

for itL = (2:L)
    % step [i-ii]
    % spin operator at the current site; to be used for generating
    % the coupling term at the next iteration
    Sprev = updateLeft([],[],AK,S,3,AK);
    
    % step [iii]
    % % add new site
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % update the Hamiltonian up to the last sites
    % to the enlarged Hilbert space
    
    % step [iv]
    % % spin-spin interaction
    Sprev_x = (Sprev(:,1,:)+Sprev(:,2,:))/sqrt(2);
    Sprev_y = (Sprev(:,1,:)-Sprev(:,2,:))/sqrt(2)/1i;
    Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
    Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
    Hxy = -updateLeft(Sprev_x,3,Anow,Sx,3,Anow) ...
        - updateLeft(Sprev_y,3,Anow,Sy,3,Anow);
    
    % step [v]
    Hnow = Hnow+Hxy;
    
    % diagonalize the current Hamiltonian
    [V,D] = eig((Hnow+Hnow')/2);
    % sort eigenvalues and eigenvectors in the order of increasing
    % eigenvalues
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    
    if mod(itL,2) == 0
        E0(itL/2) = min(D);
    end
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    % % NOTE: tol is given
    oks = (D < (Etr + tol));
    % true: to keep, false: not to keep
    % keep all degenerate states up to tolerance
    
    AK = contract(Anow,3,3,V(:,oks),2,1);
    Hprev = diag(D(oks));
end
Eiter = E0;
Eexact = (1/2)-1./(2*sin(pi./(2*((2:2:L)+1)))); % exact GS energies

% you can activate below lines to display the progress
% disptime(['#',sprintf('%02i/%02i',[itN,N]),' : ', ...
%    'NK=',sprintf('%i/%i',[size(AK,3),size(Hnow,2)])]);
figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot((2:2:L),Eiter./(2:2:L),'LineWidth',1,'LineStyle','-');
plot((2:2:L),Eexact./(2:2:L),'LineWidth',1,'LineStyle','--');
hold off;
set(gca,'LineWidth',1,'FontSize',16);
xlabel('Chain length');
ylabel('Ground-state energy per site');
legend({'Iterative diag','Exact result'}, ...
    'Interpreter','latex','Location','northeast');
grid on;
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);

figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot((2:2:L),100*abs(1-Eiter./Eexact),'LineWidth',1,'LineStyle','-');
hold off;
set(gca,'LineWidth',1,'FontSize',16);
xlabel('Chain length');
ylabel('GS energy error (%)');
grid on;
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);

%%
L = 100; % maximum chain length
H0 = I*0; % Hamiltonian for only the 1st site
A0 = getIdentity(1,2,I,2); % 1st leg is dummy leg (vacuum)

[S,I] = getLocalSpace('Spin',1/2);

% MPS of the GS
A = cell(L,1);

% initialization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
A{1} = contract(A0,3,3,V,2,1);
Hprev = D;

for itL = (2:L)
    % step [i-ii]
    % spin operator at the current site; to be used for generating
    % the coupling term at the next iteration
    Sprev = updateLeft([],[],A{itL-1},S,3,A{itL-1});
    
    % step [iii]
    % % add new site
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % update the Hamiltonian up to the last sites
    % to the enlarged Hilbert space
    
    % step [iv]
    % % spin-spin interaction
    Sprev_x = (Sprev(:,1,:)+Sprev(:,2,:))/sqrt(2);
    Sprev_y = (Sprev(:,1,:)-Sprev(:,2,:))/sqrt(2)/1i;
    Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
    Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
    Hxy = -updateLeft(Sprev_x,3,Anow,Sx,3,Anow) ...
        - updateLeft(Sprev_y,3,Anow,Sy,3,Anow);
    
    % step [v]
    Hnow = Hnow+Hxy;
    
    % diagonalize the current Hamiltonian
    [V,D] = eig((Hnow+Hnow')/2);
    % sort eigenvalues and eigenvectors in the order of increasing
    % eigenvalues
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    % % NOTE: tol is given
    oks = (D < (Etr + tol));
    % true: to keep, false: not to keep
    % keep all degenerate states up to tolerance
    
    if itL < L
        A{itL} = contract(Anow,3,3,V(:,oks),2,1);
    elseif itL == L
        A{itL} = contract(Anow,3,3,V(:,1),2,1); % select GS at the last step
    end
    Hprev = diag(D(oks));
    
    % you can activate below lines to display the progress
%     disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
%         'NK=',sprintf('%i/%i',[size(A{itN},3),size(Hnow,2)])]);
end

%%
for itL = (1:L)
    AA = contract(A{itL},3,[1 2],conj(A{itL}),3,[1 2]);
    IA = getIdentity(A{itL},3);
    err = sum(abs(AA-IA),'all');
    disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
        'err=',sprintf('%i',err)]);
end

%%
norm_GS_MPS = contract(A{1},3,[1 2],conj(A{1}),3,[1 2]);
for itL = (2:L)
    norm_GS_MPS = contract(norm_GS_MPS,2,2,A{itL},3,1);
    norm_GS_MPS = contract(norm_GS_MPS,3,[1 2],conj(A{itL}),3,[1 2]);
end
sprintf('The norm of the GS MPS: %.4f %',norm_GS_MPS)

%%
clear

J = 1; % spin-spin interaction strength

L = 50; % maximum chain length
Nkeep = 300; % maximal number of states to keep
tol = Nkeep*100*eps; % numerical tolerance for degeneracy

[S,I] = getLocalSpace('Spin',1/2);

H0 = I*0; % Hamiltonian for only the 1st site
A0 = getIdentity(1,2,I,2); % 1st leg is dummy leg (vacuum)

% lowest energies at each iteration
E0 = zeros(1,L);

for itL = (1:L)
    if itL == 1
        Hnow = H0;
        [V,D] = eig((Hnow+Hnow')/2);
        AK = contract(A0,3,3,V,2,1);
        E0(itL) = min(diag(D));
        Hprev = D;
    else
        % % add new site
        Anow = getIdentity(Hprev,2,I,2);
        Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
        % update the Hamiltonian up to the last sites
        % to the enlarged Hilbert space
        
        % % spin-spin interaction
        % Hermitian conjugate of the spin operator at
        % the current site
        Sn = permute(conj(S),[3 2 1]);
        HSS = updateLeft(Sprev,3,Anow,Sn,3,Anow);
        HSS = J*HSS;
        
        Hnow = Hnow+HSS;
        
        [V,D] = eig((Hnow+Hnow')/2);
        % sort eigenvalues and eigenvectors in the order of increasing
        % eigenvalues
        [D,ids] = sort(diag(D),'ascend');
        V = V(:,ids);
        
        E0(itL) = min(D);
        
        % truncation threshold for energy
        Etr = D(min([numel(D);Nkeep]));
        oks = (D < (Etr + tol));
        % true: to keep, false: not to keep
        % keep all degenerate states up to tolerance
        
        AK = contract(Anow,3,3,V(:,oks),2,1);
        Hprev = diag(D(oks));
    end
    
    % spin operator at the current site; to be used for 
    % generating the coupling term at the next iteration
    Sprev = updateLeft([],[],AK,S,3,AK);
    
    % you can activate below lines to display the progress
%     disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
%         'NK=',sprintf('%i/%i',[size(AK,3),size(Hnow,2)])]);
end

EG_iter = E0./(1:L);
Eexact = (1/4)-log(2); % exact GS energy, for infinite L

figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot((1:L),EG_iter,'LineWidth',1,'LineStyle','-');
plot((1:L),Eexact + zeros(1,L),'LineWidth',1,'LineStyle','--');
hold off;
set(gca,'LineWidth',1,'FontSize',16);
xlabel('Chain length');
ylabel('Ground-state energy per site');
legend({'Iterative diag','Exact ($L \to \infty$)'}, ...
    'Interpreter','latex','Location','northeast');
grid on;
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);

%%
clear

J = 1; % spin-spin interaction strength

L = 50; % maximum chain length
Nkeep = 300; % maximal number of states to keep
tol = Nkeep*100*eps; % numerical tolerance for degeneracy

% % changed this only! % %
% [S,I] = getLocalSpace('Spin',1/2);
[S,I] = getLocalSpace('Spin',1);
% % % % % % % % % % 

H0 = I*0; % Hamiltonian for only the 1st site
A0 = getIdentity(1,2,I,2); % 1st leg is dummy leg (vacuum)

% lowest energies at each iteration
E0 = zeros(1,L);

for itL = (1:L)
    if itL == 1
        Hnow = H0;
        [V,D] = eig((Hnow+Hnow')/2);
        AK = contract(A0,3,3,V,2,1);
        E0(itL) = min(diag(D));
        Hprev = D;
    else
        % % add new site
        Anow = getIdentity(Hprev,2,I,2);
        Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
        % update the Hamiltonian up to the last sites
        % to the enlarged Hilbert space
        
        % % spin-spin interaction
        % Hermitian conjugate of the spin operator at
        % the current site
        Sn = permute(conj(S),[3 2 1]);
        HSS = updateLeft(Sprev,3,Anow,Sn,3,Anow);
        HSS = J*HSS;
        
        Hnow = Hnow+HSS;
        
        [V,D] = eig((Hnow+Hnow')/2);
        % sort eigenvalues and eigenvectors in the order of increasing
        % eigenvalues
        [D,ids] = sort(diag(D),'ascend');
        V = V(:,ids);
        
        E0(itL) = min(D);
        
        % truncation threshold for energy
        Etr = D(min([numel(D);Nkeep]));
        oks = (D < (Etr + tol));
        % true: to keep, false: not to keep
        % keep all degenerate states up to tolerance
        
        AK = contract(Anow,3,3,V(:,oks),2,1);
        Hprev = diag(D(oks));
    end
    
    % spin operator at the current site; to be used for 
    % generating the coupling term at the next iteration
    Sprev = updateLeft([],[],AK,S,3,AK);
    
    % you can activate below lines to display t_he progress
%     disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
%         'NK=',sprintf('%i/%i',[size(AK,3),size(Hnow,2)])]);
end

EG_iter = E0./(1:L);
Eexact = -1.401484039; % exact GS energy, for infinite L

figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot((1:L),EG_iter,'LineWidth',1,'LineStyle','-');
plot((1:L),Eexact + zeros(1,L),'LineWidth',1,'LineStyle','--');
hold off;
set(gca,'LineWidth',1,'FontSize',16);
xlabel('Chain length');
ylabel('Ground-state energy per site');
legend({'Iterative diag','Exact ($N \to \infty$)'}, ...
    'Interpreter','latex','Location','northeast');
grid on;
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);