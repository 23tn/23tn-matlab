A = (1:3) % row vector
size(A)
ismatrix(A)
B = (4:6).' % column vector
size(B)
ismatrix(B)
C = 1 % scalar
size(C)
ismatrix(C)

%%
clear

% bond dimensions
Dset = (10:10:100);

% number of trials to be averaged
num_avg = 10;

% computational time for each pattern
time_1 = zeros(size(Dset)); % pattern (1)
time_2 = zeros(size(Dset)); % pattern (2)
time_3 = zeros(size(Dset)); % pattern (3)

% changing bond dimensions
for itD = (1:numel(Dset))
    D = Dset(itD);
    % each trial
    for itn = (1:num_avg)
        A = rand(D,D);
        B = rand(D,D,D);
        C = rand(D,D,D);
        % pattern (1)
        tobj = tic2;
        AB = contract(A,2,1,B,3,3);
        [~] = contract(AB,3,[1 3],C,3,[3 2]);
        [~,cput] = toc2(tobj); % get cpu time
        time_1(itD) = time_1(itD) + cput;
        % pattern (2)
        tobj = tic2;
        AC = contract(A,2,2,C,3,3);
        [~] = contract(AC,3,[1 2],B,3,[3 2]);
        [~,cput] = toc2(tobj); % get cpu time
        time_2(itD) = time_2(itD) + cput;
        % pattern (3)
        tobj = tic2;
        BC = contract(B,3,2,C,3,2);
        [~] = contract(A,2,[1 2],BC,4,[4 2]);
        [~,cput] = toc2(tobj); % get cpu time
        time_3(itD) = time_3(itD) + cput;
    end
end

% take averages
time_1 = time_1/num_avg;
time_2 = time_2/num_avg;
time_3 = time_3/num_avg;

% plot the result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 560 420]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [560 420]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 560 420]);
hold on;
plot(Dset,time_1,'ko','MarkerSize',6);
plot(Dset,time_2,'bsquare','MarkerSize',6);
plot(Dset,time_3,'r^','MarkerSize',6);
plot(Dset,time_1(end)*(Dset/max(Dset)).^4,'k--');
plot(Dset,time_3(end)*(Dset/max(Dset)).^5,'r--');
hold off;
box on;
grid on;
set(gca,'Xscale','log');
set(gca,'Yscale','log');
xlabel('$D$','Interpreter','latex','FontSize',16);
ylabel('average CPU time (sec.)','Interpreter','latex','FontSize',16);
set(gca,'FontSize',16);
set(gca,'XMinorTick','on');
set(gca,'TickLength',[0.03 0.025]);
set(gca,'units','points');
set(gca,'innerposition',[72.8 50 434 342.3]);
legend('pattern (1)','pattern (2)','pattern (3)',...
    '$\mathcal{O}(D^4)$','$\mathcal{O}(D^5)$',...
    'Interpreter','latex','FontSize',16,'Location','southeast');