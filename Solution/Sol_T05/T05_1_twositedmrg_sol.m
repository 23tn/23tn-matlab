% Exact GS energy Eex, GS of Hff from iterative diagonalization, MPO
% representation of Hff

% Exact GS energy Eex of Hff from numerical diagonalization
% row indices: |1>,|2>,..
% column indces: <1|,<2|,..
clear
L = 100; % chain length
H1 = zeros(L,L); % Single-particle Hamiltonian
H1 = H1 - diag(ones(L-1,1),1); % |l><l+1| terms
H1 = H1 - diag(ones(L-1,1),-1); % |l+1><l| terms
[U,E] = eig(H1);
E = diag(E); % E(i): ith lowest eigenenergy
% Eex: GS energy
% GS: electrons occupy eigenstates with E<0
Eex = sum(E(E<=0));
% display result
sprintf('Exact GS energy Eex: %.6f',Eex)

% GS and its energy of Hff from iterative diagonalization
[F,Z,I] = getLocalSpace('Fermion');
Nkeep = 50; % maximal # of states to keep
% % GS MPS for Hff: START % %
tol = Nkeep*100*eps; % numerical tolerance for degeneracy
H0 = I*0; % 1st site Hamiltonian
A0 = getIdentity(1,2,I,2); % 1st leg: dummy
MPSiter = cell(L,1); % GS MPS from iterative diagonalization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
MPSiter{1} = contract(A0,3,3,V,2,1);
Hprev = D;
for itL = (2:L)
    % Fermion aniihilation operator at the current site
    Fprev = updateLeft([],[],MPSiter{itL-1},F,3,MPSiter{itL-1});
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % hopping terms
    Hhop = -updateLeft(Fprev,3,Anow,permute(F,[3 2 1]),3,Anow);
    Hhop = Hhop + Hhop';
    Hnow = Hnow + Hhop;
    [V,D] = eig((Hnow+Hnow')/2);
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    % GS energy at the last step
    if itL == L
        Eiter = min(D);
    end
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    oks = (D <= (Etr + tol));
    if itL < L
        MPSiter{itL} = contract(Anow,3,3,V(:,oks),2,1);
    elseif itL == L
        % Choose GS at the last step
        MPSiter{itL} = contract(Anow,3,3,V(:,1),2,1);
    end
    Hprev = diag(D(oks));
    disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
        'NK=',sprintf('%i/%i',[size(MPSiter{itL},3),size(Hnow,2)])]);
end
% % GS MPS for Hff : END % %
% display result
sprintf('Eiter: %e',Eiter)
% err: Relative error
err = (Eiter-Eex)/abs(Eex);
sprintf('err: %.6f %%',100*err)

% MPO representation of Hff and comparing ground state energies
% % MPO for Hff: START % %
W = cell(L,1); % MPO of Hff
% First site
W{1} = zeros(1,2,4,2); % ordering: left bottom right top
W{1}(1,:,2,:) = -squeeze(F)'; % -creation
W{1}(1,:,3,:) = -squeeze(F); % -annihilation
W{1}(1,:,4,:) = I;
% Last site
W{L} = zeros(4,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = I;
W{L}(2,:,1,:) = squeeze(F); % annihilation
W{L}(3,:,1,:) = squeeze(F)'; % creation
% Other sites
for itL = (2:L-1)
    W{itL} = zeros(4,2,4,2); % ordering: left bottom right top
    W{itL}(1,:,1,:) = I;
    W{itL}(2,:,1,:) = squeeze(F); % annihilation
    W{itL}(3,:,1,:) = squeeze(F)'; % creation
    W{itL}(4,:,2,:) = -squeeze(F)'; % -creation
    W{itL}(4,:,3,:) = -squeeze(F); % -annihilation
    W{itL}(4,:,4,:) = I;
end
% Compare Eiter and E_MPO = <MPS|H_MPO|MPS>
E_MPO = 1;
for itL = (1:L)
    E_MPO = updateLeft(E_MPO,3,MPSiter{itL},W{itL},4,MPSiter{itL});
end
% display result
sprintf('Eiter - E_MPO : %e',Eiter-E_MPO)
% % MPO for Hff: END % %

%%
% Compare GS energies from iterative diagonalization and 2s Hamiltonian

% Shift isometry center for MPS from iterative diagonalization
Liso = 50; % isometry center site
MPSiso = MPSiter; % initialize MPSiso
% % get left canonical tensors : START % %
for itL = (1:Liso-1)
    [U,S,V] = svdTr(MPSiso{itL},3,[1 2],[],[]);
    MPSiso{itL} = U;
    MPSiso{itL+1} = contract(diag(S)*V,2,2,MPSiso{itL+1},3,1);
end
% % get left canonical tensors : END % %
% % get right canonical tensors : START % %
for itL = (L:-1:Liso+1)
    [U,S,V] = svdTr(MPSiso{itL},3,1,[],[]);
    MPSiso{itL} = V;
    MPSiso{itL-1} = contract(MPSiso{itL-1},3,3,U*diag(S),2,1);
end
% % get right canonical tensors : END % %
% Check left isometry for sites 1,..,49(=Liso-1)
for itL=(1:Liso-1)
    M = MPSiso{itL};
    MMdag = contract(M,3,[1 2],conj(M),3,[1 2]);
    I = getIdentity(M,3);
    sprintf('norm(|MMdag - I|) at site %d : %.2e',itL,norm(abs(I-MMdag)))
end
% Check right isometry for sites 51(=Liso+1),..,100(=L)
for itL=(Liso+1:L)
    M = MPSiso{itL};
    MMdag = contract(M,3,[2 3],conj(M),3,[2 3]);
    I = getIdentity(M,1);
    sprintf('norm(|MMdag - I|) at site %d : %.2e',itL,norm(abs(I-MMdag)))
end

% Compute left and right environments
Hleft = 1; % left environment
Hright = 1; % right environment
% % update left environment : START % %
for itL = (1:Liso-1)
    Mleft = MPSiso{itL}; % ordering : left bottom right
    Wleft = W{itL}; % ordering : left bottom right up
    Hleft = updateLeft(Hleft,3,Mleft,Wleft,4,Mleft);
end
% % update left environment : END % %
% % update right environment : START % %
for itL = (L:-1:Liso+2)
    % note: leg orderings are changed to use updateLeft
    Mright = permute(MPSiso{itL},[3 2 1]); % ordering : right bottom left
    Wright = permute(W{itL},[3 2 1 4]); % ordering : right bottom left up
    Hright = updateLeft(Hright,3,Mright,Wright,4,Mright);
end
% % update right environment : END % %

% Compare GS energies from iterative diagonalization and 2s Hamiltonian
Cinit = MPSiso{Liso}; % C at isometry center
Binit = MPSiso{Liso+1}; % B at the site Liso+1

% % % % Following the Hint: START % % % %
% Merging physical legs of C and B
I_CB = getIdentity(Cinit,2,Binit,2);
CB = contract(Cinit,3,2,I_CB,3,1);
CB = contract(CB,4,[2 3],Binit,3,[1 2]);
% Merging physcial legs of W and W
I_WW = getIdentity(W{Liso},2,W{Liso+1},2);
WW = contract(W{Liso},4,2,I_WW,3,1);
WW = contract(WW,5,[2 4],W{Liso+1},4,[1 2]);
WW = contract(WW,5,[2 5],I_WW,3,[1 2]);
% % % % Following the Hint: END % % % %

% HCB: contracting Hleft, Hright, WW, and CB
HCB = contract(Hleft,3,3,CB,3,1);
HCB = contract(HCB,4,[2 3],WW,4,[1 4]);
HCB = contract(HCB,4,[2 4],Hright,3,[3 2]);
% E: energy expectation value
E = contract(HCB,3,[1 2 3],conj(CB),3,[1 2 3]);
% display result
sprintf('E : %e',E)
sprintf('Eiter - E : %e',Eiter-E)

%%
% Lanczos method
NLZs = (1:10); % number of Lanczos steps
ELZs = zeros(size(NLZs)); % energy specfic number of steps
for itN = (1:numel(NLZs))
    N = NLZs(itN);
    % % % % Input CB and WW with merged physical legs: START % % % %
    [CBeff,Eeff] = eigs_1site(Hleft,WW,Hright,CB,'N',N);
    % % % % Input CB and WW with merged physical legs: END % % % %
    % energy
    ELZs(itN) = Eeff;
end
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 300]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 300]);
set(gca,'units','points');
set(gca,'innerposition',[100,50,300,225]);
plot(NLZs,(ELZs-Eex)/abs(Eex)*100,'-o','linewidth',2);
xlabel('$N_\mathrm{Lanczos}$','Interpreter','latex','fontsize',18);
ylabel('relative error (\%)','Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend('$(E_\mathrm{LZ}-E_\mathrm{ex})/|E_\mathrm{ex}|$', ...
    'Interpreter','latex','fontsize',18,'location','northeast');


%%
% Decomposing 2s wave function
NLZ = 5; % number of Lanczos steps
Dfs = (50:10:110); % final bond dimension
% Although the maximal value of Df is 100, we study Df up to 110 to
% explicitly check the maximal value
EDfs = zeros(size(Dfs)); % energies
dws = zeros(size(Dfs)); % discarded weights
% % % % Input CB and WW with merged physical legs: START % % % %
[CBeff,Eeff] = eigs_1site(Hleft,WW,Hright,CB,'N',N);
% % % % Input CB and WW with merged physical legs: END % % % %
% re-split the merged physical legs
CBeff = contract(CBeff,3,2,I_CB,3,3,[1 3 4 2]);
for itD = (1:numel(Dfs))
    [U,S,Vd,dws(itD)] = svdTr(CBeff,4,[1 2],Dfs(itD),[]);
    CB_Df = contract(U,3,3,diag(S),2,1);
    CB_Df = contract(CB_Df,3,3,Vd,3,1);
    CB_Df = contract(CB_Df,4,[2 3],I_CB,3,[1 2],[1 3 2]);
    % energy
    HCB_Df = contract(Hleft,3,3,CB_Df,3,1);
    HCB_Df = contract(HCB_Df,4,[2 3],WW,4,[1 4]);
    HCB_Df = contract(HCB_Df,4,[2 4],Hright,3,[3 2]);
    % E: energy expectation value
    EDfs(itD) = contract(HCB_Df,3,[1 2 3],conj(CB_Df),3,[1 2 3]);
end
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 1000 350]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [1000 350]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 1000 350]);
subplot(1,3,1);
set(gca,'units','points');
set(gca,'innerposition',[140,50,200,267]);
plot(Dfs,EDfs,'-o','linewidth',2);
xlabel('$D_\mathrm{f}$','Interpreter','latex','fontsize',18);
ylabel('GS energy $E$','Interpreter','latex','fontsize',18);
xlim([50 110]);
xticks([50 70 90 110]);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
subplot(1,3,2);
set(gca,'units','points');
set(gca,'innerposition',[440,50,200,267]);
semilogy(Dfs,dws,'-o','linewidth',2);
hold on;
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05], 'String',...
    {'$\xi=0$', 'for $D_\mathrm{f} \geq 100$'},'interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [520 270 200 25];
hold off;
xlabel('$D_\mathrm{f}$','Interpreter','latex','fontsize',18);
ylabel('discarded weights $\xi$','Interpreter','latex','fontsize',18);
xlim([50 110]);
xticks([50 70 90 110]);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
subplot(1,3,3);
set(gca,'units','points');
set(gca,'innerposition',[780,50,200,267]);
plot(Dfs,(EDfs-Eex)/abs(Eex)*100,'-o','linewidth',2);
xlabel('$D_\mathrm{f}$','Interpreter','latex','fontsize',18);
ylabel('relative error (\%)','Interpreter','latex','fontsize',18);
xlim([50 110]);
xticks([50 70 90 110]);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend('$(E-E_\mathrm{ex})/|E_\mathrm{ex}|$',...
    'Interpreter','latex','fontsize',18,'location','northeast');

%%
% Initial state of D=1
L = 100; % chain length
MPS = cell(L,1); % initial MPS
MPS(:) = {zeros(1,2,1)};
for itL = (1:L)
    if mod(itL,2)
        MPS{itL}(1,1,1) = 1; % occupied states for 1,3,5,...
    else
        MPS{itL}(1,2,1) = 1; % empty states for 2,4,6,...
    end
end
% verify MPS by local occupations
n = cell(size(MPS));
n(:) = {1};
for itL = (1:L)
    % electron number opeartor at site itL
    n_itL = zeros(1,2,1,2); % ordering: left down right up
    n_itL(:,1,:,1) = 1;
    for itL2 = (1:L)
        if itL2 == itL
            n{itL} = updateLeft(n{itL},3,MPS{itL2},n_itL,4,MPS{itL2});
        else
            n{itL} = updateLeft(n{itL},3,MPS{itL2},[],[],MPS{itL2});
        end
    end
end
n = cell2mat(n);
% plot reesult
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 700 220]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [700 220]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 800 220]);
set(gca,'units','points');
set(gca,'innerposition',[70,50,600,150]);
plot((1:L),n,'-o','linewidth',2);
xlabel('$\ell$','Interpreter','latex','fontsize',18);
ylabel('$\langle n_\ell\rangle$','Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);


%%
alpha = sqrt(2);
Dmax = [40 60 80 100];
Nsweep = 100; % number of DMRG sweeps will be 2*Nsweep
Econv = 1e-12; % Convergence criterion for energy
E0s = zeros(size(Dmax)); % GS energy for different Dfs
dws = cell(size(Dmax)); % discarded weight for different Dfs
MPS2s = cell(size(Dmax)); % GS MPS for different Dfs
for itD = (1:numel(Dmax))
    sprintf('%% %% %% %% 2s DMRG: START (Dmax=%d) %% %% %% %%',Dmax(itD))
    % DMRG sweep until reaching convergence
    [MPS2s{itD},E0s(itD),~,~] = ...
        DMRG_2site(W,MPS,alpha,Dmax(itD),Nsweep,'Econv',Econv);
    % one more DMRG sweep for collecting discarded weights
    [~,~,~,dws{itD}] = ...
        DMRG_2site(W,MPS2s{itD},alpha,Dmax(itD),1);
    dws{itD} = sum(dws{itD}(:));
    sprintf('%% %% %% %% 2s DMRG: END (Dmax=%d) %% %% %% %%',Dmax(itD))
end
dws = cell2mat(dws);
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 310]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 310]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 310]);
set(gca,'units','points');
set(gca,'innerposition',[100,60,300,225]);
lgd_set = cell(numel(Dmax),1); % set of legends for data points
for itD = 1:numel(Dmax)
    lgd_set{itD} = ['$D_\mathrm{max}=',sprintf('%d',Dmax(itD)),'$'];
end
hold on;
for itD = (1:numel(Dmax))
    scatter(dws(itD),(E0s(itD)-Eex)/abs(Eex),'linewidth',2);
end
hold off;
set(gca,'xscale','log','yscale','log');
set(gca,'box','on');
xlabel('$\xi_\mathrm{tot}$','Interpreter','latex','fontsize',18);
ylabel('$(E_\mathrm{GS}-E_\mathrm{ex})/|E_\mathrm{ex}|$',...
    'Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend(lgd_set,'Interpreter','latex','FontSize',16,'Location','southeast');

%%
% 2s DMRG: GS energy and discarded weight versus Dmax
% linear fitting
P = polyfit(dws(2:end),(E0s(2:end)-Eex)/abs(Eex),1);
if P(2) >= 0
    sprintf('Linear fitting: (rel. err.)=%.4e(weight) +%.4e%f',P(1),P(2))
else
    sprintf('Linear fitting: (rel. err.)=%.4e(weight)-%.4e%f',P(1),abs(P(2)))
end
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 310]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 310]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 310]);
set(gca,'units','points');
set(gca,'innerposition',[100,60,300,225]);
lgd_set = cell(numel(Dmax),1); % set of legends for data points
for itD = 1:numel(Dmax)
    lgd_set{itD} = ['$D_\mathrm{max}=',sprintf('%d',Dmax(itD)),'$'];
end
hold on;
for itD = (1:numel(Dmax))
    scatter(dws(itD),(E0s(itD)-Eex)/abs(Eex),'linewidth',2);
end
plot(dws,P(1)*dws+P(2),'--k','linewidth',1);
hold on;
if P(2) >= 0
    txtbox = annotation('textbox',[0.5 0.5 0.05 0.05], 'String',...
    {['linear fit: ',['$',sprintf('%.4f',P(1)),'\xi_\mathrm{tot}'],...
        [sprintf('+%.2e',P(2)),'$']]},'interpreter','latex','fontsize',18,...
    'edgecolor','none');
else
    txtbox = annotation('textbox',[0.5 0.5 0.05 0.05], 'String',...
    {['linear fit: ',['$',sprintf('%.4f',P(1)),'\xi_\mathrm{tot}'],...
        [sprintf('-%.2e',abs(P(2))),'$']]},'interpreter','latex','fontsize',18,...
    'edgecolor','none');
end
txtbox.Units = 'points';
txtbox.Position = [115 240 200 25];
hold off;
hold off;
set(gca,'xscale','log','yscale','log');
set(gca,'box','on');
xlabel('$\xi_\mathrm{tot}$','Interpreter','latex','fontsize',18);
ylabel('$(E_\mathrm{GS}-E_\mathrm{ex})/|E_\mathrm{ex}|$',...
    'Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend(lgd_set,'Interpreter','latex','FontSize',16,'Location','southeast');