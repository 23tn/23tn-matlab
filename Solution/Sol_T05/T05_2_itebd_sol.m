% Two-site Trotter gate
clear
beta = 20; % imaginary time interval
N = 2000; % # of imaginary time steps
[S,~] = getLocalSpace('Spin',1/2);
So = S; Se = S; % spin at odd and even sites
Ho = contract(So,3,2,conj(permute(Se,[3 2 1])),3,2); % odd bond Hamiltonian
He = contract(Se,3,2,conj(permute(So,[3 2 1])),3,2); % even bond Hamiltonian
Homat = reshape(permute(Ho,[1 3 2 4]),...
    [size(So,1)*size(Se,1) size(So,3)*size(Se,3)]); % matrix representation of Ho
Hemat = reshape(permute(He,[1 3 2 4]),...
    [size(Se,1)*size(So,1) size(Se,3)*size(So,3)]); % matrix representation of He
% exponentiate the matrix representation of Hamiltonian
expHo = expm(-beta/N*Homat);
expHe = expm(-beta/N*Hemat);
% reshape matrix -> rank-4 tensor
expHo = reshape(expHo,[size(So,1) size(Se,1) size(So,3) size(Se,3)]);
expHe = reshape(expHe,[size(Se,1) size(So,1) size(Se,3) size(So,3)]);

%%
% iTEBD: GS search

Ds = [4 10 100]; % bond dimensions
% Es{itD}(2*m-1,1) and Es{itD}(2*m-1,2): odd and even bond energies at the 
% m-th odd bond update for maximum bond dimension Ds(itD)
% Es{itD}(2*m,1) and Es{itD}(2*m,2): odd and even bond energies at the m-th
% even bond update for maximum bond dimension Ds(itD)
Es = cell(numel(Ds),1);
ES(:) = {zeros(2*N,2)};
Go = cell(numel(Ds),1);
Ge = cell(numel(Ds),1);
Lo = cell(numel(Ds),1);
Le = cell(numel(Ds),1);

% % % % iTEBD: START % % % %
for itD = (1:numel(Ds))
    sprintf('%% %% %% %% iTEBD: START (D=%d) %% %% %% %%',Ds(itD))
    % intial state of even and odd sites: spin up and down
    % initialize Gamma for even and odd sites
    Go{itD} = zeros(1,2,1); Go{itD}(:,1,:) = 1; % spin up
    Ge{itD} = zeros(1,2,1); Ge{itD}(:,2,:) = 1; % spin down
    Lo{itD} = 1; Le{itD} = 1; % initalize Lambda for odd and even bonds
    
    % check left isometry
    Ao = contract(diag(Le{itD}),2,2,Go{itD},3,1);
    AAdag = contract(Ao,3,[1 2],conj(Ao),3,[1 2]);
    I = getIdentity(Ao,3);
    sprintf('norm(|AAdag - I|) at odd site (D=%d): %.2e',Ds(itD),norm(abs(I-AAdag)))
    Ae = contract(diag(Lo{itD}),2,2,Ge{itD},3,1);
    AAdag = contract(Ae,3,[1 2],conj(Ae),3,[1 2]);
    I = getIdentity(Ae,3);
    sprintf('norm(|AAdag - I|) at even site (D=%d): %.2e',Ds(itD),norm(abs(I-AAdag)))
    
    % check right isometry
    Bo = contract(Go{itD},3,3,diag(Lo{itD}),2,1);
    BBdag = contract(Bo,3,[2 3],conj(Bo),3,[2 3]);
    I = getIdentity(Bo,1);
    sprintf('norm(|BBdag - I|) at odd site (D=%d): %.2e',Ds(itD),norm(abs(I-BBdag)))
    Be = contract(Ge{itD},3,3,diag(Le{itD}),2,1);
    BBdag = contract(Be,3,[2 3],conj(Be),3,[2 3]);
    I = getIdentity(Be,1);
    sprintf('norm(|BBdag - I|) at even site (D=%d): %.2e',Ds(itD),norm(abs(I-BBdag)))
    
    % iTEBD iteration
    for itN = (1:N)
        % % % odd bond update: START % % %
        % To: Le*Go*Lo*Ge*Le
        To = contract(diag(Le{itD}),2,2,Go{itD},3,1);
        To = contract(To,3,3,diag(Lo{itD}),2,1);
        To = contract(To,3,3,Ge{itD},3,1);
        To = contract(To,4,4,diag(Le{itD}),2,1);
        % contract exp(-beta/N*Ho) with To
        eHTo = contract(expHo,4,[3 4],To,4,[2 3],[3 1 2 4]);
        % do SVD: use svdTr
        [U,S,V] = svdTr(eHTo,4,[1 2],Ds(itD),[]);
        % normalize the singular value vector (so that the norm is 1)
        S = S/norm(S);
        % update Lo
        Lo{itD} = S;
        % update Go, Ge
        Go{itD} = contract(diag(1./Le{itD}),2,2,U,3,1);
        Ge{itD} = contract(V,3,3,diag(1./Le{itD}),2,1);
        % measure energy, for the bra/ket states of:
        % Le*Go*Lo*Ge*Le*Go*Lo
        % ----- -- -----
        %  =U   =S   =V
        
        % energy at the bond for Lo (the 3rd tensor in the above form)
        US = contract(U,3,3,diag(S),2,1);
        USV = contract(US,3,3,V,3,1);
        USV = reshape(USV,[size(USV,1) size(So,1)*size(Se,1) size(USV,4)]);
        H2 = updateLeft([],[],USV,Homat,2,USV);
        GL = contract(Go{itD},3,3,diag(Lo{itD}),2,1);
        Es{itD}(2*itN-1,1) = trace(updateLeft(H2,2,GL,[],[],GL));
        
        % energy at the bond for Le (the 5th tensor in the above form)
        H2 = updateLeft([],[],US,[],[],US);
        VGL = contract(V,3,3,GL,3,1);
        VGL = reshape(VGL,[size(VGL,1) size(So,1)*size(Se,1) size(VGL,4)]);
        Es{itD}(2*itN-1,2) = trace(updateLeft(H2,2,VGL,Hemat,2,VGL));
        
        % normalize by the norm of the bra/ket states
        T = updateLeft([],[],US,[],[],US);
        T = updateLeft(T,2,V,[],[],V);
        T = updateLeft(T,2,GL,[],[],GL);
        Es{itD}(2*itN-1,:) = Es{itD}(2*itN-1,:)/trace(T);
        if (mod(itN,100) == 0) && (itN < N)
            disptime(['#',sprintf('%i/%i',[itN N]),' (odd bond) '...
                ', E = ',sprintf('%.8g',mean(Es{itD}(2*itN-1,:),2))]);
        end
        % % % odd bond update: END % % %
        
        % % % even bond update: START % % %
        % Te: Lo*Ge*Le*Go*Lo
        Te = contract(diag(Lo{itD}),2,2,Ge{itD},3,1);
        Te = contract(Te,3,3,diag(Le{itD}),2,1);
        Te = contract(Te,3,3,Go{itD},3,1);
        Te = contract(Te,4,4,diag(Lo{itD}),2,1);
        % contract exp(-beta/N*He) with Te
        eHTe = contract(expHe,4,[3 4],Te,4,[2 3],[3 1 2 4]);
        % do SVD: use svdTr
        [U,S,V] = svdTr(eHTe,4,[1 2],Ds(itD),[]);
        % normalize the singular value vector (so that the norm is 1)
        S = S/norm(S);
        % update Le
        Le{itD} = S;
        % update Go, Ge
        Go{itD} = contract(V,3,3,diag(1./Lo{itD}),2,1);
        Ge{itD} = contract(diag(1./Lo{itD}),2,2,U,3,1);
        % measure energy, for the bra/ket states of:
        % Lo*Ge*Le*Go*Lo*Ge*Le
        % ----- -- -----
        %  =U   =S   =V
        
        % energy at the bond for Le (the 3rd tensor in the above form)
        US = contract(U,3,3,diag(S),2,1);
        USV = contract(US,3,3,V,3,1);
        USV = reshape(USV,[size(USV,1) size(So,1)*size(Se,1) size(USV,4)]);
        H2 = updateLeft([],[],USV,Hemat,2,USV);
        GL = contract(Ge{itD},3,3,diag(Le{itD}),2,1);
        Es{itD}(2*itN,1) = trace(updateLeft(H2,2,GL,[],[],GL));
        
        % energy at the bond for Lo (the 5th tensor in the above form)
        H2 = updateLeft([],[],US,[],[],US);
        VGL = contract(V,3,3,GL,3,1);
        VGL = reshape(VGL,[size(VGL,1) size(So,1)*size(Se,1) size(VGL,4)]);
        Es{itD}(2*itN,2) = trace(updateLeft(H2,2,VGL,Homat,2,VGL));
        
        % normalize by the norm of the bra/ket states
        T = updateLeft([],[],US,[],[],US);
        T = updateLeft(T,2,V,[],[],V);
        T = updateLeft(T,2,GL,[],[],GL);
        Es{itD}(2*itN,:) = Es{itD}(2*itN,:)/trace(T);
        if (mod(itN,100) == 0) && (itN < N)
            disptime(['#',sprintf('%i/%i',[itN N]),' (even bond)',...
                ', E = ',sprintf('%.8g',mean(Es{itD}(2*itN,:),2))]);
        end
        % % % even bond update: END % % %
    end
    disptime(['#',sprintf('%i/%i',[itN N]),...
        ', E = ',sprintf('%.8g',mean(Es{itD}(2*itN,:),2))]);
    sprintf('%% %% %% %% iTEBD: END (D=%d) %% %% %% %%',Ds(itD))
end
% % % % iTEBD: END % % % %

% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 700 300]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [700 300]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 700 300]);
subplot(1,2,1);
set(gca,'units','points');
set(gca,'innerposition',[100,50,230,230]);
lgd_set = cell(numel(Ds),1); % set of legends for data points
for itD = 1:numel(Ds)
    lgd_set{itD} = ['$D=',sprintf('%d',Ds(itD)),'$'];
end
hold on;
for itD = (1:numel(Ds))
    scatter(Ds(itD),mean(Es{itD}(end,:),2),'linewidth',2);
end
plot(Ds,ones(numel(Ds),1)*(1/4-log(2)),'--k','linewidth',1);
hold off;
set(gca,'box','on');
xlabel('$D$','Interpreter','latex','fontsize',18);
ylabel('GS energy','Interpreter','latex','fontsize',18);
xticks([0 50 100]);
ylim([-0.445 -0.44]);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend(lgd_set,'Interpreter','latex','FontSize',16,'Location','northeast');
subplot(1,2,2);
set(gca,'units','points');
set(gca,'innerposition',[450,50,230,230]);
hold on;
for itD = (1:numel(Ds))
    scatter(Ds(itD),(mean(Es{itD}(end,:),2)-(1/4-log(2)))/abs((1/4-log(2))),'linewidth',2);
end
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    '$E_\mathrm{site}=1/4-\mathrm{log}2$','interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [125 110 100 25];
hold off;
set(gca,'box','on');
xlabel('$D$','Interpreter','latex','fontsize',18);
ylabel('$(E(D)-E_\mathrm{site})/|E_\mathrm{site}|$','Interpreter','latex','fontsize',18);
set(gca,'yscale','log');
xticks([0 50 100]);
ylim([5e-4 1e-2]);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
lgd_set = cell(numel(Ds),1); % set of legends for data points
for itD = 1:numel(Ds)
    lgd_set{itD} = ['$D=',sprintf('%d',Ds(itD)),'$'];
end
legend(lgd_set,'Interpreter','latex','FontSize',16,'Location','northeast');

%%
% iTEBD: Canonicalization (OPTIONAL)

% Check that the GS MPS is not a canonical form
for itD = (1:numel(Ds))
    % check left isometry
    Ao = contract(diag(Le{itD}),2,2,Go{itD},3,1);
    AAdag = contract(Ao,3,[1 2],conj(Ao),3,[1 2]);
    I = getIdentity(Ao,3);
    % generally Nonzero
    sprintf('norm(|AAdag - I|) at odd site (D=%d): %.2e',Ds(itD),norm(abs(I-AAdag)))
    Ae = contract(diag(Lo{itD}),2,2,Ge{itD},3,1);
    AAdag = contract(Ae,3,[1 2],conj(Ae),3,[1 2]);
    I = getIdentity(Ae,3);
    % should be zero
    sprintf('norm(|AAdag - I|) at even site (D=%d): %.2e',Ds(itD),norm(abs(I-AAdag)))
    
    % check right isometry
    Bo = contract(Go{itD},3,3,diag(Lo{itD}),2,1);
    BBdag = contract(Bo,3,[2 3],conj(Bo),3,[2 3]);
    I = getIdentity(Bo,1);
    % should be zero
    sprintf('norm(|BBdag - I|) at odd site (D=%d): %.2e',Ds(itD),norm(abs(I-BBdag)))
    Be = contract(Ge{itD},3,3,diag(Le{itD}),2,1);
    BBdag = contract(Be,3,[2 3],conj(Be),3,[2 3]);
    I = getIdentity(Be,1);
    % generally nonzero
    sprintf('norm(|BBdag - I|) at even site (D=%d): %.2e',Ds(itD),norm(abs(I-BBdag)))
end

% Gammas and Lambdas of canonicalized MPS
Gocan = cell(numel(Ds),1);
Gecan = cell(numel(Ds),1);
Locan = cell(numel(Ds),1);
Lecan = cell(numel(Ds),1);

% Orthogonalize Vidal's Gamma-Lambda representation of infinite MPS,
% following the method given in [R. Orus & G. Vidal, Phys. Rev. B 78,
% 155117 (2008)]. Here the goal of the orthogonalization is to make the ket
% tensors of Lambda*Gamma type (Gamma*Lambda type) be left-normalized
% (right-normalized), i.e., to bring them into canonical forms.
%
% ->-diag(Lo)->-*->-Ge->-*->-diag(Le)->-*->-Go->-*->-diag(Lo)->-  
%  1          2   1 ^  3   1          2   1 ^  3   1          2 
%                   |2                      |2

for itD = (1:numel(Ds))
    sprintf('%% %% %% %% Canonicalization: START (D=%d) %% %% %% %%',Ds(itD))
    % % "Coarse grain" the tensors: contract the tensors for the unit cell
    % % altogether. Then the coarse-grained tensor will be orthogonalized.
    
    % % % % STEP 1 in [iTEBD.5]: START % % % %
    % T=Ge*Le*Go: coarsed-grained tensor
    T = Ge{itD};
    T = contract(T,3,3,diag(Le{itD}),2,1);
    T = contract(T,3,3,Go{itD},3,1);
    % % % STEP 1 in [iTEBD.5]: END % % %
    
    % % % % STEP 2 in [iTEBD.5]: START % % % %
    % ket tensor to compute transfer operator from right
    TR = contract(T,4,4,diag(Lo{itD}),2,1);
    % % step (i) in PRB 78 155117 (2008): START % %
    % find the dominant eigenvector for the transfer operator from right
%     XR = canonIMPS_domVec(TR,4,0);
    
    % Find the tensor X where X*X' originates from the dominant right
    % eigenvector of the transfer operator which is
    %
    %  1                 n+2
    %  -->-[     T     ]->--
    %      2|   ...   |n+1
    %       ^         ^
    %       *   ...   *
    %       ^         ^
    %      2|   ...   |n+1
    %  --<-[     T'    ]-<--
    %  1                 n+2
    %
    % isconj == 1 if transfer operator from left, == 0 from right
    rankT = 4;
    isconj = 0;

    D = size(TR,1);
    idT = (2:rankT-1);
    TR = contract(TR,rankT,idT,conj(TR),rankT,idT,[1 3 2 4]);
    TR = reshape(TR,[D*D D*D]);
    if isconj, TR = TR.'; end
    [V,~] = eigs(TR,1,'lm');
    V = reshape(V,[D D]);
    V = (V+V')/2; % Hermitianize for numerical stability
    [V1,D1] = eig(V);
    D1 = diag(D1);
    
    % V is equivalent up to overall sign; if the eigenvalues are mostly
    % negative, take the overall sign flip
    if sum(D1) < 0
        D1 = -D1; 
    end
    
    % remove negative eigenvalues, which come from numerical noise
    oks = (D1 <= 0);
    D1(oks) = [];
    V1(:,oks) = [];
    
    XR = V1*diag(sqrt(D1));
    if isconj, XR = XR.'; end

    % ket tensor to compute transfer operator from left
    TL = contract(diag(Lo{itD}),2,2,T,4,1);
    % find the dominant eigenvector for the transfer operator from left
%     XL = canonIMPS_domVec(TL,4,1);

    % Find the tensor X where X*X' originates from the dominant right
    % eigenvector of the transfer operator which is
    %
    %  1                 n+2
    %  -->-[     T     ]->--
    %      2|   ...   |n+1
    %       ^         ^
    %       *   ...   *
    %       ^         ^
    %      2|   ...   |n+1
    %  --<-[     T'    ]-<--
    %  1                 n+2
    %
    % isconj == 1 if transfer operator from left, == 0 from right
    rankT = 4;
    isconj = 1;

    D = size(TL,1);
    idT = (2:rankT-1);
    TL = contract(TL,rankT,idT,conj(TL),rankT,idT,[1 3 2 4]);
    TL = reshape(TL,[D*D D*D]);
    if isconj, TL = TL.'; end
    [V,~] = eigs(TL,1,'lm');
    V = reshape(V,[D D]);
    V = (V+V')/2; % Hermitianize for numerical stability
    [V1,D1] = eig(V);
    D1 = diag(D1);
    
    % V is equivalent up to overall sign; if the eigenvalues are mostly
    % negative, take the overall sign flip
    if sum(D1) < 0
        D1 = -D1; 
    end
    
    % remove negative eigenvalues, which come from numerical noise
    oks = (D1 <= 0);
    D1(oks) = [];
    V1(:,oks) = [];
    
    XL = V1*diag(sqrt(D1));
    if isconj, XL = XL.'; end

    % % step (i) in PRB 78 155117 (2008): END % %
    % % step (ii) in PRB 78 155117 (2008): START % %
    % do SVD in Fig. 2(ii) of [R. Orus & G. Vidal, Phys. Rev. B 78, 155117 (2008)]
    [U,SX,V] = svdTr(XL*diag(Lo{itD})*XR,2,1,[],[]);
    % % step (ii) in PRB 78 155117 (2008): END % %
    % % step (iii) in PRB 78 155117 (2008): START % %
    % orthogonalize the coarse-grained tensor
    T = contract(V/XR,2,2,T,4,1);
    T = contract(T,4,4,XL\U,2,1);
    Locan{itD} = SX/norm(SX);
    % % step (iii) in PRB 78 155117 (2008): START % %
    % % % % STEP 2 in [iTEBD.5]: END % % % %
    
    % % % % STEP 3 in [iTEBD.5]: START % % % %
    % decompose the orthogonalized coarse-grained tensor into the tensors
    % for individual sites
    % contract singular value tensors to the left and right ends, before
    % doing SVD.
    T = contract(diag(Locan{itD}),2,2,T,4,1);
    T = contract(T,4,4,diag(Locan{itD}),2,1);
    [U2,S2,V2] = svdTr(T,4,[1 2],[],[]);
    Lecan{itD} = S2/norm(S2);
    Gecan{itD} = contract(diag(1./Locan{itD}),2,2,U2,3,1);
    Gocan{itD} = contract(V2,3,3,diag(1./Locan{itD}),2,1);
    % % % % STEP 3 in [iTEBD.5]: END % % % %
    sprintf('%% %% %% %% Canonicalization: END (D=%d) %% %% %% %%',Ds(itD))
end

% Check that the GS is canonicalized
for itD = (1:numel(Ds))
    % check left isometry
    Ao = contract(diag(Lecan{itD}),2,2,Gocan{itD},3,1);
    AAdag = contract(Ao,3,[1 2],conj(Ao),3,[1 2]);
    I = getIdentity(Ao,3);
    % generally Nonzero
    sprintf('norm(|AAdag - I|) at odd site (D=%d; canonicalized): %.2e',...
        Ds(itD),norm(abs(I-AAdag)))
    Ae = contract(diag(Locan{itD}),2,2,Gecan{itD},3,1);
    AAdag = contract(Ae,3,[1 2],conj(Ae),3,[1 2]);
    I = getIdentity(Ae,3);
    % should be zero
    sprintf('norm(|AAdag - I|) at even site (D=%d; canonicalized): %.2e',...
        Ds(itD),norm(abs(I-AAdag)))
    
    % check right isometry
    Bo = contract(Gocan{itD},3,3,diag(Locan{itD}),2,1);
    BBdag = contract(Bo,3,[2 3],conj(Bo),3,[2 3]);
    I = getIdentity(Bo,1);
    % should be zero
    sprintf('norm(|BBdag - I|) at odd site (D=%d; canonicalized): %.2e',...
        Ds(itD),norm(abs(I-BBdag)))
    Be = contract(Gecan{itD},3,3,diag(Lecan{itD}),2,1);
    BBdag = contract(Be,3,[2 3],conj(Be),3,[2 3]);
    I = getIdentity(Be,1);
    % generally nonzero
    sprintf('norm(|BBdag - I|) at odd site (D=%d; canonicalized): %.2e',...
        Ds(itD),norm(abs(I-BBdag)))
end

% function X = canonIMPS_domVec (T,rankT,isconj)
% % Find the tensor X where X*X' originates from the dominant right
% % eigenvector of the transfer operator which is
% %
% %  1                 n+2
% %  -->-[     T     ]->--
% %      2|   ...   |n+1
% %       ^         ^
% %       *   ...   *
% %       ^         ^
% %      2|   ...   |n+1
% %  --<-[     T'    ]-<--
% %  1                 n+2
% %
% % isconj == 1 if transfer operator from left, == 0 from right
% 
% D = size(T,1);
% idT = (2:rankT-1);
% T = contract(T,rankT,idT,conj(T),rankT,idT,[1 3 2 4]);
% T = reshape(T,[D*D D*D]);
% if isconj, T = T.'; end
% [V,~] = eigs(T,1,'lm');
% V = reshape(V,[D D]);
% V = (V+V')/2; % Hermitianize for numerical stability
% [V1,D1] = eig(V);
% D1 = diag(D1);
% 
% % V is equivalent up to overall sign; if the eigenvalues are mostly
% % negative, take the overall sign flip
% if sum(D1) < 0
%     D1 = -D1; 
% end
% 
% % remove negative eigenvalues, which come from numerical noise
% oks = (D1 <= 0);
% D1(oks) = [];
% V1(:,oks) = [];
% 
% X = V1*diag(sqrt(D1));
% if isconj, X = X.'; end
% 
% end

%%
% iTEBD: Correlation function (OPTIONAL)
k = 100;
C = cell(k,numel(Ds));
C1 = 0.147715726853315; % Benckmark value for k=1
C2 = 0.060679769956435; % Benckmark value for k=2
% Using canonicalized Gammas amd Lambdas 
Ge = Gecan;
Go = Gocan;
Le = Lecan;
Lo = Locan;
for itD = (1:numel(Ds))
    Ao = contract(diag(Le{itD}),2,2,Go{itD},3,1);
    Ae = contract(diag(Lo{itD}),2,2,Ge{itD},3,1);
    for itk = (1:k)
        % compute correlator <Sz*Sz>
        % correlator is computed for spins at (\ell)th and (\ell+itk)th sites
        % (correlator for even \ell) and (correlator for odd \ell) are averaged
        Co = getIdentity(Ae,3); % correlator for even \ell
        Co = updateLeft(Co,2,Ao,squeeze(So(:,3,:)),2,Ao);
        Ce = getIdentity(Ao,3); % correlator for odd \ell
        Ce = updateLeft(Ce,2,Ae,squeeze(Se(:,3,:)),2,Ae);
        for itk2 = (1:k)
            % itk2 counts the (\ell + itk)th site
            if itk2 < itk
                if mod(itk2,2) == 1
                    Co = updateLeft(Co,2,Ae,[],[],Ae);
                    Ce = updateLeft(Ce,2,Ao,[],[],Ao);
                else
                    Co = updateLeft(Co,2,Ao,[],[],Ao);
                    Ce = updateLeft(Ce,2,Ae,[],[],Ae);
                end
            elseif itk2 == itk
                if mod(itk2,2) == 1
                    Co = updateLeft(Co,2,Ae,squeeze(Se(:,3,:)),2,Ae);
                    Co = contract(Co,2,1,diag(Le{itD}),2,1);
                    Co = contract(Co,2,1,diag(Le{itD}),2,1);
                    Ce = updateLeft(Ce,2,Ao,squeeze(So(:,3,:)),2,Ao);
                    Ce = contract(Ce,2,1,diag(Lo{itD}),2,1);
                    Ce = contract(Ce,2,1,diag(Lo{itD}),2,1);
                else
                    Co = updateLeft(Co,2,Ao,squeeze(So(:,3,:)),2,Ao);
                    Co = contract(Co,2,1,diag(Lo{itD}),2,1);
                    Co = contract(Co,2,1,diag(Lo{itD}),2,1);
                    Ce = updateLeft(Ce,2,Ae,squeeze(Se(:,3,:)),2,Ae);
                    Ce = contract(Ce,2,1,diag(Le{itD}),2,1);
                    Ce = contract(Ce,2,1,diag(Le{itD}),2,1);
                end
            end
        end
        % compute ket state
        To = getIdentity(Ae,3); % state for even \ell
        To = updateLeft(To,2,Ao,[],[],Ao);
        Te = getIdentity(Ao,3); % state for odd \ell
        Te = updateLeft(Te,2,Ae,[],[],Ae);
        for itk2 = (1:itk)
            if mod(itk2,2) == 1
                To = updateLeft(To,2,Ae,[],[],Ae);
                Te = updateLeft(Te,2,Ao,[],[],Ao);
            else
                To = updateLeft(To,2,Ao,[],[],Ao);
                Te = updateLeft(Te,2,Ae,[],[],Ae);
            end
            if itk2 == itk
                if mod(itk,2) == 1
                    To = contract(To,2,1,diag(Le{itD}),2,1);
                    To = contract(To,2,1,diag(Le{itD}),2,1);
                    Te = contract(Te,2,1,diag(Lo{itD}),2,1);
                    Te = contract(Te,2,1,diag(Lo{itD}),2,1);
                else
                    To = contract(To,2,1,diag(Lo{itD}),2,1);
                    To = contract(To,2,1,diag(Lo{itD}),2,1);
                    Te = contract(Te,2,1,diag(Le{itD}),2,1);
                    Te = contract(Te,2,1,diag(Le{itD}),2,1);
                end
            end
        end
        % compute <Sz> 
        So_ell = getIdentity(Ae,3); % <Sz> at \ell, for even \ell
        So_ell = updateLeft(So_ell,2,Ao,squeeze(So(:,3,:)),2,Ao);
        Se_ell = getIdentity(Ao,3); % <Sz> at \ell, for even \ell
        Se_ell = updateLeft(Se_ell,2,Ae,squeeze(Se(:,3,:)),2,Ae);
        So_ell_k = getIdentity(Ae,3); % <Sz> at \ell+itk, for even \ell
        So_ell_k = updateLeft(So_ell_k,2,Ao,[],[],Ao);
        Se_ell_k = getIdentity(Ao,3); % <Sz> at \ell+itk, for even \ell
        Se_ell_k = updateLeft(Se_ell_k,2,Ae,[],[],Ae);
        for itk2 = (1:k)
            if itk2 < itk
                if mod(itk2,2) == 1
                    So_ell = updateLeft(So_ell,2,Ae,[],[],Ae);
                    Se_ell = updateLeft(Se_ell,2,Ao,[],[],Ao);
                    So_ell_k = updateLeft(So_ell_k,2,Ae,[],[],Ae);
                    Se_ell_k = updateLeft(Se_ell_k,2,Ao,[],[],Ao);
                else
                    So_ell = updateLeft(So_ell,2,Ao,[],[],Ao);
                    Se_ell = updateLeft(Se_ell,2,Ae,[],[],Ae);
                    So_ell_k = updateLeft(So_ell_k,2,Ao,[],[],Ao);
                    Se_ell_k = updateLeft(Se_ell_k,2,Ae,[],[],Ae);
                end
            elseif itk2 == itk
                if mod(itk,2) == 1
                    So_ell = updateLeft(So_ell,2,Ae,[],[],Ae);
                    So_ell = contract(So_ell,2,1,diag(Le{itD}),2,1);
                    So_ell = contract(So_ell,2,1,diag(Le{itD}),2,1);
                    Se_ell = updateLeft(Se_ell,2,Ao,[],[],Ao);
                    Se_ell = contract(Se_ell,2,1,diag(Lo{itD}),2,1);
                    Se_ell = contract(Se_ell,2,1,diag(Lo{itD}),2,1);
                    So_ell_k = updateLeft(So_ell_k,2,Ae,squeeze(Se(:,3,:)),2,Ae);
                    So_ell_k = contract(So_ell_k,2,1,diag(Le{itD}),2,1);
                    So_ell_k = contract(So_ell_k,2,1,diag(Le{itD}),2,1);
                    Se_ell_k = updateLeft(Se_ell_k,2,Ao,squeeze(So(:,3,:)),2,Ao);
                    Se_ell_k = contract(Se_ell_k,2,1,diag(Lo{itD}),2,1);
                    Se_ell_k = contract(Se_ell_k,2,1,diag(Lo{itD}),2,1);
                else
                    So_ell = updateLeft(So_ell,2,Ao,[],[],Ao);
                    So_ell = contract(So_ell,2,1,diag(Lo{itD}),2,1);
                    So_ell = contract(So_ell,2,1,diag(Lo{itD}),2,1);
                    Se_ell = updateLeft(Se_ell,2,Ae,[],[],Ae);
                    Se_ell = contract(Se_ell,2,1,diag(Le{itD}),2,1);
                    Se_ell = contract(Se_ell,2,1,diag(Le{itD}),2,1);
                    So_ell_k = updateLeft(So_ell_k,2,Ao,squeeze(So(:,3,:)),2,Ao);
                    So_ell_k = contract(So_ell_k,2,1,diag(Lo{itD}),2,1);
                    So_ell_k = contract(So_ell_k,2,1,diag(Lo{itD}),2,1);
                    Se_ell_k = updateLeft(Se_ell_k,2,Ae,squeeze(Se(:,3,:)),2,Ae);
                    Se_ell_k = contract(Se_ell_k,2,1,diag(Le{itD}),2,1);
                    Se_ell_k = contract(Se_ell_k,2,1,diag(Le{itD}),2,1);
                end
            end
        end
        % correlator with even \ell and correlator with odd \ell are averaged
        C{itk,itD} = (-1)^itk*(trace(Co)/trace(To)+trace(Ce)/trace(Te))/2;
        % reduce numerical error of correlator by using <(S-<S>)(S'-<S'>)>=<SS'>-<S><S'>
        if mod(itk,2) == 1
            C{itk,itD} = C{itk,itD} - (-1)^itk*...
                (trace(So_ell)/trace(To) * trace(So_ell_k)/trace(Te) ...
                + trace(Se_ell)/trace(Te) * trace(Se_ell_k)/trace(To))/2;
        else
            C{itk,itD} = C{itk,itD} - (-1)^itk*...
                (trace(So_ell)/trace(To) * trace(So_ell_k)/trace(To) ...
                + trace(Se_ell)/trace(Te) * trace(Se_ell_k)/trace(Te))/2;
        end
    end
end
C = cell2mat(C);
sprintf('relative error of C(1) for D=%d: %2f %%',Ds(end),(C1-C(1,end))/C1*100)
sprintf('relative error of C(2) for D=%d: %2f %%',Ds(end),(C2-C(2,end))/C2*100)
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 500 400]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [500 400]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 500 400]);
set(gca,'units','points');
set(gca,'innerposition',[80,60,400,300]);
hold on;
plot((1:k),C(:,1),':^','color','k','linewidth',1);
plot((1:k),C(:,2),'--square','color','k','linewidth',1);
plot((1:k),C(:,3),'-o','color','k','linewidth',1);
P = polyfit(log((1:10)),log(C(1:10,end)),1);
loglog((1:20),exp(P(2))*(1:20).^P(1),'color','#D95319','linewidth',1.5);
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    ['$',sprintf('%.2f',exp(P(2))),'\times k^{',sprintf('%.2f',P(1)),'}$'],...
    'interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Color = '#D95319';
txtbox.Units = 'points';
txtbox.Position = [253 280 100 25];
P = polyfit((30:100),log(C(30:100,end)),1);
loglog((10:100),exp(P(2))*exp(P(1)).^(10:100),'color','#FF00FF','linewidth',1.5);
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    ['$',sprintf('%.2f',exp(P(2))),'\times',sprintf('%.2f',exp(P(1))),'^k$'],...
    'interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Color = '#FF00FF';
txtbox.Units = 'points';
txtbox.Position = [370 225 100 25];
scatter(1,C1,'blue','linewidth',2,'markerfacecolor','blue');
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    '$C(k=1)$','interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Color = 'blue';
txtbox.Units = 'points';
txtbox.Position = [92 323 100 25];
scatter(2,C2,'red','linewidth',2,'markerfacecolor','red');
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    '$C(k=2)$','interpreter','latex','fontsize',18,...
    'edgecolor','none');
txtbox.Color = 'red';
txtbox.Units = 'points';
txtbox.Position = [159 304 100 25];
hold off;
set(gca,'box','on');
xlabel('$k$','Interpreter','latex','fontsize',18);
ylabel('Correlation function','Interpreter','latex','fontsize',18);
ylim([1e-7 1e0]);
yticks([1e-6 1e-4 1e-2 1e0])
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'xscale','log','yscale','log');
set(gca,'ticklength',[0.04 0.1]);
legend('$D=4$','$D=10$','$D=100$','Interpreter','latex','FontSize',16,'Location','southwest');