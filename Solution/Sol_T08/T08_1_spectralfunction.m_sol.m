% SIAM: discrete fermionic spectral function
clear

% Hamiltonian parameters
Delta = 2.5e-5*pi; % hybridization function
U = 2e-3; % local interaction
epsd = -U/2; % d-level position

% NRG parameters
Lambda = 3; % discretization parameter
L = 55; % Wilson chain length
Nkeep = 300; % # of kept states

% temperature
T = 1e-10;

% construct local operators
[F,Z,S,I] = getLocalSpace('FermionS');

% particle number operator
NF = cat(3,contract(conj(F(:,1,:)),3,[1 2],F(:,1,:),3,[1 2]), ...
           contract(conj(F(:,2,:)),3,[1 2],F(:,2,:),3,[1 2]));

% impurity Hamiltonian
H0 = U*(NF(:,:,1)*NF(:,:,2)) + epsd*(NF(:,:,1)+NF(:,:,2));

% ket tensor for the impurity
A0 = getIdentity(1,2,I,2); % 1 for dummy leg

% logarithmic discretization
[ff,gg] = doCLD([-1 1],[1 1]*Delta/pi,Lambda,L);

% NRG iterative diagonalization
Inrg = NRG_IterDiag(H0,A0,Lambda,ff,F,gg,sum(NF,3),Z,Nkeep);

% full density matrix
Inrg = getRhoFDM(Inrg,T);

% discrete spectral function for spin-up
Fup = F(:,1,:); % d-level spin-up electron annihilation operator 
[odisc,Adisc] = getAdisc(Inrg,Fup,Z);
% discrete spectral function for spin-down
% Adisc is same for spin-up and -down, 
% which can be checked by activate below two lines and excute
% Fdown = F(:,1,:); % d-level spin-up electron annihilation operator 
% [odisc,Adisc] = getAdisc(Inrg,Fdown,Z);

% Re[Gd(w=0)]
% By using the Kramers-Kronig relations,
% Re[Gd] = (1/pi) P int_(-inf)^inf Im[Gd](w')/w' dw'
% where P denotes the Cauchy principal value and Adisc = (-1/pi) Im[Gd]
ReGd = -sum(Adisc./odisc);

% sum(Adisc >= 0) and sum(Adisc < 0)
fprintf(sprintf('sum(Adisc >= 0): %.4f\n',sum(Adisc(Adisc >= 0))));
fprintf(sprintf('sum(Adisc < 0): %.4f\n',sum(Adisc(Adisc < 0))));

% plot Adisc/|odisc|
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 320]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 320]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 320]);
set(gca,'units','points');
set(gca,'innerposition',[100,60,300,225]);
plot = semilogx(odisc(odisc>0),Adisc(odisc>0)./abs(odisc(odisc>0)),...
    'linewidth',1.5);
hold off
set(gca,'xscale','log');
xlim([T 1e4*T])
xlabel('$\omega$','Interpreter','latex','fontsize',18);
ylabel('$A_{d_s}(\omega)/|\omega|$',...
    'Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);

%%
% SIAM: spin susceptibility
clear

% Hamiltonian parameters
Delta = 2.5e-5*pi; % hybridization function
U = 2e-3; % local interaction
epsd = -U/2; % d-level position

% NRG parameters
Lambda = 3; % discretization parameter
L = 55; % Wilson chain length
Nkeep = 300; % # of kept states

% temperature and suscepttibility
TK = sqrt(U*Delta/2)*exp(pi*(Delta/2/U-U/8/Delta)); % Kondo temperature
Ts = [1e-10 10.^(-2:4)*TK];
chiS = zeros(size(Ts));

% construct local operators
[F,Z,S,I] = getLocalSpace('FermionS');

% particle number operator
NF = cat(3,contract(conj(F(:,1,:)),3,[1 2],F(:,1,:),3,[1 2]), ...
           contract(conj(F(:,2,:)),3,[1 2],F(:,2,:),3,[1 2]));

% impurity Hamiltonian
H0 = U*(NF(:,:,1)*NF(:,:,2)) + epsd*(NF(:,:,1)+NF(:,:,2));

% ket tensor for the impurity
A0 = getIdentity(1,2,I,2); % 1 for dummy leg

% logarithmic discretization
[ff,gg] = doCLD([-1 1],[1 1]*Delta/pi,Lambda,L);

% NRG iterative diagonalization
Inrg = NRG_IterDiag(H0,A0,Lambda,ff,F,gg,sum(NF,3),Z,Nkeep);

% % compute susceptibility at T
for itT = (1:numel(Ts))
    % full density matrix
    T = Ts(itT);
    Inrg = getRhoFDM(Inrg,T);
    
    % discrete spectral function
    Sz = S(:,3,:); % d-level spin-up electron annihilation operator 
    [odisc,Adisc] = getAdisc(Inrg,Sz,[]);
    
    % chiS = -Re[GSz(w=0)]
    % By using the Kramers-Kronig relations,
    % Re[GSz] = (1/pi) P int_(-inf)^inf Im[GSz](w')/w' dw'
    % where P denotes the Cauchy principal value and Adisc = (-1/pi) Im[GSz]
    chiS(itT) = sum(Adisc./odisc);
end

fprintf(sprintf('TK from the formula: %.4e\n',TK));
% chiS(T->0) = 1 / (4TK)
fprintf(sprintf('TK from the chiS: %.4e\n',1/4/chiS(1)));

% plot TK chiS
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 300]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 320]);
set(gca,'units','points');
set(gca,'innerposition',[100,60,300,225]);
loglog(Ts(2:end)/TK,TK*chiS(2:end),'-o','linewidth',1.5);
hold on
plot([1e-2 1e1],[1/4 1/4],'--','color',[0.4 0.4 0.4],'linewidth',1.5);
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'$1/4$','interpreter','latex','fontsize',18,...
'color',[0.4 0.4 0.4],'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [250 250 35 25];
plot([1e1 1e4],[1e-1 1e-4]/3,'--','color',[0.4 0.4 0.4],'linewidth',1.5);
plot([1e-2 1e1],[1/4 1/4],'--','color',[0.4 0.4 0.4],'linewidth',1.5);
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
'$\propto 1/T$','interpreter','latex','fontsize',18,...
'color',[0.4 0.4 0.4],'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [315 160 35 25];
xlim([Ts(2)/TK Ts(end)/TK]);
xticks([1e-2 1e0 1e2 1e4]);
yticks([1e-5 1e-4 1e-3 1e-2 1e-1 1e0])
xlabel('$T / T_\mathrm{K}$','Interpreter','latex','fontsize',18);
ylabel('$T_\mathrm{K} \: \chi_{S}(T)$',...
    'Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);

%%
% SIAM: continuous fermionic spectral function
clear

% Hamiltonian parameters
Delta = 2.5e-5*pi; % hybridization function
U = 2e-3; % local interaction
epsd = -U/2; % d-level position
e
% NRG parameters
Lambda = 3; % discretization parameter
L = 55; % Wilson chain length
Nkeep = 300; % # of kept states

% temperature and spectral function
TK = sqrt(U*Delta/2)*exp(pi*(Delta/2/U-U/8/Delta)); % Kondo temperature
Ts = 10.^(-2:4)*TK;
Acont = cell(size(Ts)); % continuous spectral function
ocont = cell(size(Ts)); % frequency grid for Acont

% construct local operators
[F,Z,S,I] = getLocalSpace('FermionS');

% particle number operator
NF = cat(3,contract(conj(F(:,1,:)),3,[1 2],F(:,1,:),3,[1 2]), ...
           contract(conj(F(:,2,:)),3,[1 2],F(:,2,:),3,[1 2]));

% impurity Hamiltonian
H0 = U*(NF(:,:,1)*NF(:,:,2)) + epsd*(NF(:,:,1)+NF(:,:,2));

% ket tensor for the impurity
A0 = getIdentity(1,2,I,2); % 1 for dummy leg

% logarithmic discretization
[ff,gg] = doCLD([-1 1],[1 1]*Delta/pi,Lambda,L);

% NRG iterative diagonalization
Inrg = NRG_IterDiag(H0,A0,Lambda,ff,F,gg,sum(NF,3),Z,Nkeep);

% % compute Acont at T
for itT = (1:numel(Ts))
    % full density matrix
    T = Ts(itT);
    Inrg = getRhoFDM(Inrg,T);
    
    % discrete spectral function
    Fup = F(:,1,:); % d-level spin-up electron annihilation operator 
    [odisc,Adisc] = getAdisc(Inrg,Fup,Z);
    
    % continuous spectral function
    [ocont{itT},Acont{itT}] = getAcont(odisc,Adisc,log(Lambda),T/5);
end
Acont = cell2mat(Acont);
ocont = cell2mat(ocont);

% plot Acont w.r.t. ocont
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 650 300]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [650 300]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 650 300]);
subplot(1,2,1);
set(gca,'units','points');
plot(ocont/U,pi*Delta*Acont,'linewidth',1.5);
xlim([-1 1]);
xlabel('$\omega/U$','Interpreter','latex','fontsize',18);
ylabel('$A(\omega) \pi \Delta$','Interpreter','latex','fontsize',18);
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    'Hubbard','fontsize',18,'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [84 106 100 25];
arrow1 = annotation('arrow','position',[0.5 0.5 0.05 0.05]);
arrow1.Units = 'points';
arrow1.Position = [133 99 14 -26];
arrow2 = annotation('arrow','position',[0.5 0.5 0.05 0.05]);
arrow2.Units = 'points';
arrow2.Position = [132 100 80 -26];
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    'Kondo','fontsize',18,'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [115 220 100 25];
set(gca,'innerposition',[70,60,220,220]);
set(gca,'FontSize',18);
lgd = legend('10^{-2}','10^{-1}','1','10^{1}','10^{2}','10^{3}','10^{4}');
title(lgd,'$T/T_\mathrm{K}$','interpreter','latex');
subplot(1,2,2);
set(gca,'units','points');
semilogx(ocont/TK,pi*Delta*Acont,'linewidth',1.5);
xlim([1e-3 1e6]);
xticks([1e-3 1e0 1e3 1e6]);
xlabel('$\omega / T_\mathrm{K}$','Interpreter','latex','fontsize',18);
ylabel('$A(\omega) \pi \Delta$','Interpreter','latex','fontsize',18);
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    'Hubbard','fontsize',18,'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [541 81 100 25];
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    'Kondo','fontsize',18,'edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [465 220 100 25];
set(gca,'innerposition',[400,60,220,220]);
set(gca,'FontSize',18);