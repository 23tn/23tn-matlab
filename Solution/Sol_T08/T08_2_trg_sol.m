% TRG: implementation
clear

beta = 0.5; % inverse temperature

% % construct tensor T
% matrix represenation of T
T = sinh(2*beta)*(diag([tanh(beta) 1 1 coth(beta)]) + flip(diag([1 1 1 1])));
% reshape T into a 4-leg tensor
T = reshape(T,[2 2 2 2]);

% % get coarse-grained tensor Tnew
% step (a): divide T into two tensors
[U,S,Vd] = svdTr(T,4,[1 4],[],[]);
Tr = contract(U,3,3,diag(sqrt(S)),2,1); % red tensor
Tb = contract(diag(sqrt(S)),2,2,Vd,3,1); % blue tensor
[U,S,Vd] = svdTr(T,4,[1 2],[],[]);
Tg = contract(U,3,3,diag(sqrt(S)),2,1); % green tensor
Ty = contract(diag(sqrt(S)),2,2,Vd,3,1); % yellow tensor
% step (b): contract tensors Tr Tb Tg Ty
Trg = contract(Tr,3,2,Tg,3,2); % Tr * Tg
Tyb = contract(Ty,3,3,Tb,3,2); % Ty * Tb
% step (c): coarsed-grained tensor
T = contract(Trg,4,[1 3],Tyb,4,[2 4],[1 2 4 3]);

%%
% TRG: free energy density
clear

% inverse temperature, final tensor, free energy
betas = (0.4:0.01:0.5); % inverse temperature
Tfin = cell(size(betas)); % final tensor after TRG
Ftrg = zeros(size(betas)); % free energy; using TRG
Fexact = zeros(size(betas)); % free energy; exact solution

% N: number of sites
N = 2^32;
% Ntrg: number of TRG steps
% since number of tensors is reduced by half at each step
Ntrg = log2(N);
D = 24; % bond dimension

for itb = (1:numel(betas))
    % % construct tensor T
    beta = betas(itb); % inverse temperature
    % matrix represenation of T
    T = sinh(2*beta)*(diag([tanh(beta) 1 1 coth(beta)]) + flip(diag([1 1 1 1])));
    % reshape T into a 4-leg tensor
    T = reshape(T,[2 2 2 2]);

    % % TRG iteration steps
    normT = zeros(1,Ntrg);
    for itN = (1:Ntrg)
        % step (a): divide T into two tensors
        [U,S,Vd] = svdTr(T,4,[1 4],D,[]);
        Tr = contract(U,3,3,diag(sqrt(S)),2,1); % red tensor
        Tb = contract(diag(sqrt(S)),2,2,Vd,3,1); % blue tensor
        [U,S,Vd] = svdTr(T,4,[1 2],D,[]);
        Tg = contract(U,3,3,diag(sqrt(S)),2,1); % green tensor
        Ty = contract(diag(sqrt(S)),2,2,Vd,3,1); % yellow tensor
        % step (b): contract tensors Tr Tb Tg Ty
        Trg = contract(Tr,3,2,Tg,3,2); % Tr * Tg
        Tyb = contract(Ty,3,3,Tb,3,2); % Ty * Tb
        % step (c): coarsed-grained tensor
        T = contract(Trg,4,[1 3],Tyb,4,[2 4],[1 2 4 3]);
        normT(itN) = trace(reshape(T,[size(T,1)*size(T,2) size(T,3)*size(T,4)]));
        T = T/normT(itN); % norm for 2^itN number of very initial tensors
    end
    % save final tensor after TRG
    Tfin{itb} = T;

    % partition function
    Z = trace(reshape(T,[size(T,1)*size(T,2) size(T,3)*size(T,3)]));
    Ftrg(itb) = -(log(Z)/N + sum(2.^(Ntrg-1:-1:0).*log(normT))/N)/beta;

    % % exact solution
    fun = @(x,beta0) log(cosh(2*beta0)^2 ...
        + sinh(2*beta0)*sqrt(sinh(2*beta0)^2+sinh(2*beta0)^-2-2*cos(2*x)));
    Fexact(itb) = -(log(2)/2 + (1/2/pi)*integral(@(x) fun(x,beta),0,pi))/beta;
end

% plot result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 320]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 320]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 320]);
set(gca,'units','points');
set(gca,'innerposition',[100,60,300,225]);
plot(betas,Ftrg,'o','linewidth',1.5);
hold on
plot(betas,Fexact,'--','linewidth',1.5,'color',[0.6 0.6 0.6]);
hold off
set(gca,'xscale','log');
xlabel('$\beta$','Interpreter','latex','fontsize',18);
ylabel('free energy',...
    'Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend('TRG','exact','interpreter','latex','fontsize',18,...
    'location','southeast');

%%
% TRG: detect phase transition
% Run TN_0013..._m02.m first and run this code without clear workspace

S = zeros(size(betas)); % entropy
X = zeros(size(betas)); % degeneracy

for itb = (1:numel(betas))
    % tensor after TRG
    T = Tfin{itb};
    % compute entropy
    Gamma = contract(T,4,[1 3],T,4,[3 1]);
    Gamma = contract(Gamma,4,[3 4],getIdentity(Gamma,3),2,[1 2]);
    normG = contract(Gamma,4,[1 2],getIdentity(Gamma,1),2,[1 2]);
    Gamma = Gamma/normG;
    lambda = eig(Gamma);
    S(itb) = -sum(abs(lambda).*log(abs(lambda)));
    % compute X
    X(itb) = trace(reshape(T,[size(T,1)*size(T,2) size(T,3)*size(T,4)]))^2;
    X(itb) = X(itb)/normG;
end

% plot result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 320]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 320]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 320]);
set(gca,'units','points');
set(gca,'innerposition',[100,60,300,225]);
plot(betas,S,'-o','linewidth',1.5);
hold on
plot(betas,X,'--x','linewidth',1.5);
plot([1 1]*log(1+sqrt(2))/2,[0 2.5],'-.','linewidth',1.5,'color',[0.6 0.6 0.6]);
txtbox = annotation('textbox',[0.5 0.5 0.05 0.05],'String',...
    '$\beta_\mathrm{c} = \frac{\mathrm{log}(1+\sqrt{2})}{2}$',...
    'interpreter','latex','fontsize',18,'backgroundcolor','white','edgecolor','none');
txtbox.Units = 'points';
txtbox.Position = [185 250 100 25];
hold off
set(gca,'xscale','log');
xlabel('$\beta$','Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend('entropy $S$','degeneracy $X$','interpreter','latex','fontsize',18,...
    'location','southeast');