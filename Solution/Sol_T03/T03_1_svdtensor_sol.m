%% Perform an SVD on the GHZ state
clear
GHZ3 = zeros(2,2,2); %[s1]x[s2]x[s3]
GHZ3(1,1,1) = 1/sqrt(2); % |000>
GHZ3(2,2,2) = 1/sqrt(2); % |111>
%% (1)
[U,S,Vd] = svdTr(GHZ3,3,1,inf,0);
%% (2)
% Verify U S V^\dagger = T_GHZ
T_GHZ = contract(U,2,1,contract(diag(S),2,1,Vd,3,1),3,1);
err = sum(GHZ3-T_GHZ,'all');
sprintf('SVD error: %f %%',err)
% Verify S = diag([1 1]) / \sqrt(2)
S
%% (3)
% Set NKeep = 1
[~,~,~,dw] = svdTr(GHZ3,3,1,1,0);
dw % Discarded weight = 0.5
%% (4)
[U,S,Vd] = svdTr(GHZ3,3,[1 2],inf,0);
size(U) % rank-3 tensor
size(S) % S is a vector
size(Vd) % rank-2 tensor (matrix)
% Verify U S V^\dagger = T_GHZ
T_GHZ = contract(U,3,3,contract(diag(S),2,1,Vd,2,1),2,1);
err = sum(GHZ3-T_GHZ,'all');
sprintf('SVD error: %f %%',err)
% Verify S = diag([1 1]) / \sqrt(2)
S

%%
clear
L = 10;
% initialize GHZ_L as a tensor of rank 10
GHZ_L = zeros(2,2);
for itL = (3:10)
    GHZ_L = cat(itL,GHZ_L,GHZ_L);
end
GHZ_L(1,1,1,1,1,1,1,1,1,1) = 1/sqrt(2); % |0,...,0>
GHZ_L(2,2,2,2,2,2,2,2,2,2) = 1/sqrt(2); % |1,...,1>
M = cell(L,1);
T = reshape(GHZ_L*sqrt(2), [1 2 numel(GHZ_L)/2]);
for itL = (1:L-1)
    [U,S,Vd] = svdTr(T,3,[1 2],inf,0);
    M{itL} = U;
    T = contract(diag(S),2,2,Vd,2,1);
    T = reshape(T, [size(T,1) 2 size(T,2)/2]);
end
M{end} = T;
% check left canonical
M_L = M{L}/sqrt(2);
for itL = (1:L)
    if itL < L
        MM = contract(M{itL},3,[1 2],conj(M{itL}),3,[1 2]);
    else
        MM = contract(M_L,3,[1 2],conj(M_L),3,[1 2]);
    end
    I = eye(size(MM,1));
    err = norm(MM-I);
    sprintf(['norm of diffrence between MM*' ...
        ' and the expected identity matrix' ...
        ' at site %d: %.4e'],itL,err)
end
% check right canonical
M_1 = M{1}/sqrt(2);
for itL = (1:L)
    if itL > 1
        MM = contract(M{itL},3,[2 3],conj(M{itL}),3,[2 3]);
    else
        MM = contract(M_1,3,[2 3],conj(M_1),3,[2 3]);
    end
    I = eye(size(MM,1));
    err = norm(MM-I);
    sprintf(['norm of diffrence between MM*' ...
        ' and the expected identity matrix' ...
        ' at site %d: %.4e'],itL,err)
end
% Now Nkeep is fixed as 2 and the discarded weights are computed
Nkeep = 2;
dw_set = zeros(L-1,1);
T = reshape(GHZ_L*sqrt(2), [1 2 numel(GHZ_L)/2]);
for itL = (1:L-1)
    [U,S,Vd,dw] = svdTr(T,3,[1 2],Nkeep,0);
    M{itL} = U;
    dw_set(itL) = dw;
    T = contract(diag(S),2,2,Vd,2,1);
    T = reshape(T, [size(T,1) 2 size(T,2)/2]);
    sprintf(['discarded weight of the SVD' ...
        ' on site %d: %f'],itL,dw_set(itL))
end
% Additionally, Nkeep is fixed as 1
Nkeep = 1;
dw_set = zeros(L-1,1);
T = reshape(GHZ_L*sqrt(2), [1 2 numel(GHZ_L)/2]);
for itL = (1:L-1)
    [U,S,Vd,dw] = svdTr(T,3,[1 2],Nkeep,0);
    M{itL} = U;
    dw_set(itL) = dw;
    T = contract(diag(S),2,2,Vd,2,1);
    T = reshape(T, [size(T,1) 2 size(T,2)/2]);
    % note that the discarded weight is nonzero for itL = 1
    sprintf(['discarded weight of the SVD' ...
        ' on site %d: %f'],itL,dw_set(itL))
end