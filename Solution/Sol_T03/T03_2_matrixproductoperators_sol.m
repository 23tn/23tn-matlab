%% Construct MPO of Ising model
clear
J = 1;
L = 4;
Sz = [1 0; 0 -1]/2;
W = cell(L,1); % MPO W
% First site
W{1} = zeros(1,2,3,2); % ordering: left bottom right top
W{1}(1,:,2,:) = J*Sz; % J*S_1^z
W{1}(1,:,3,:) = eye(2); % I
% Last site
W{L} = zeros(3,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = eye(2); % I
W{L}(2,:,1,:) = Sz; % S_L^z
% Other sites
for itW = (2:L-1)
    W{itW} = zeros(3,2,3,2); % ordering: left bottom right top
    W{itW}(1,:,1,:) = eye(2); % I
    W{itW}(2,:,1,:) = Sz; % S_itW^z
    W{itW}(3,:,2,:) = J*Sz; % J*S_itW^z
    W{itW}(3,:,3,:) = eye(2); % I
end
H_MPO = 1; % Hamiltonian from MPO
for itW = (1:L)
    H_MPO = contract(H_MPO,4,3,W{itW},4,1);
    H_MPO = permute(H_MPO,[1 2 4 5 3 6]);
    % Reshape H_MPO to be a rank-4 tensor
    H_MPO = reshape(H_MPO,[size(H_MPO,1) ...
                           size(H_MPO,2)*size(H_MPO,3) ...
                           size(H_MPO,4) ...
                           size(H_MPO,5)*size(H_MPO,6)]);
end
H_MPO = squeeze(H_MPO); % reshape H_MPO to be a matrix
% H_dir: Directly constructed Hamiltonian
Sz1 = kron(Sz,kron(eye(2),kron(eye(2),eye(2))));
Sz2 = kron(eye(2),kron(Sz,kron(eye(2),eye(2))));
Sz3 = kron(eye(2),kron(eye(2),kron(Sz,eye(2))));
Sz4 = kron(eye(2),kron(eye(2),kron(eye(2),Sz)));
H_dir = J*(Sz1*Sz2+Sz2*Sz3+Sz3*Sz4);
% Verify H_MPO = H_dir
diff = norm(H_dir-H_MPO);
sprintf('norm(H_dir - H_MPO): %e',diff)

%% Implement -H_{lambda=1}
clear
L = 6;
Sz = [1 0; 0 -1]/2;
lambda = 1;
W = cell(L,1); % MPO W
% first site
W{1} = zeros(1,2,3,2); % ordering: left bottom right top
W{1}(1,:,2,:) = -exp(-lambda)*Sz; % -e^(-lambda)*S_1^z
W{1}(1,:,3,:) = eye(2); % I
% last site
W{L} = zeros(3,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = eye(2); % I
W{L}(2,:,1,:) = Sz; % S_L^z
% other sites
for itW = (2:L-1)
    W{itW} = zeros(3,2,3,2); % ordering: left bottom right top
    W{itW}(1,:,1,:) = eye(2); % I
    W{itW}(2,:,1,:) = Sz; % S_itW^z
    W{itW}(2,:,2,:) = exp(-lambda)*eye(2); % e(^\lambda)*I
    W{itW}(3,:,2,:) = -exp(-lambda)*Sz; % -e^(-lambda)*S_itW^z
    W{itW}(3,:,3,:) = eye(2); % I
end
% |Psi> = |down_arrow ... down_arrow>
Psi = zeros(2^L,1);
Psi(end) = 1;
H_MPO = 1; % Hamiltonian from MPO
for itW = (1:L)
    H_MPO = contract(H_MPO,4,3,W{itW},4,1);
    H_MPO = permute(H_MPO,[1 2 4 5 3 6]);
    % Reshape H_MPO to be a rank-4 tensor
    H_MPO = reshape(H_MPO,[size(H_MPO,1) ...
                           size(H_MPO,2)*size(H_MPO,3) ...
                           size(H_MPO,4) ...
                           size(H_MPO,5)*size(H_MPO,6)]);
end
H_MPO = squeeze(H_MPO); % reshape H_MPO to be a matrix
HPsi = H_MPO*Psi; % H|Psi>
HPsi = HPsi/norm(HPsi); % Normalize H|Psi>
P_Psi = Psi'.*Psi; % Projection |Psi><Psi|
% If P(H|Psi>) = H|Psi> then H|Psi> = |Psi>
diff = norm(P_Psi*HPsi - HPsi);
sprintf('norm(PH|Psi> - H|Psi>): %e %',diff)