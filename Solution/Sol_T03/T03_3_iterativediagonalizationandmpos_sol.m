%% XY Hamiltonian with iterative diagonalization and MPO
clear
%% (1)
J = 1; % coupling strength
L = 30; % chain length
Nkeep = 100; % maximal # of states to keep
tol = Nkeep*100*eps; % numerical tolerance for degeneracy
[S,I] = getLocalSpace('Spin',1/2);
H0 = I*0; % 1st site Hamiltonian
A0 = getIdentity(1,2,I,2); % 1st leg: dummy
MPS = cell(L,1); % GS MPS
% Initialization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
MPS{1} = contract(A0,3,3,V,2,1);
Hprev = D;
for itL = (2:L)
    % Spin operator at the current site
    Sprev = updateLeft([],[],MPS{itL-1},S,3,MPS{itL-1});
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % Spin-spin interaction
    Sprev_x = (Sprev(:,1,:)+Sprev(:,2,:))/sqrt(2);
    Sprev_y = (Sprev(:,1,:)-Sprev(:,2,:))/sqrt(2)/1i;
    Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
    Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
    Hxy = J*updateLeft(Sprev_x,3,Anow,Sx,3,Anow) ...
        + J*updateLeft(Sprev_y,3,Anow,Sy,3,Anow);
    Hnow = Hnow+Hxy;
    [V,D] = eig((Hnow+Hnow')/2);
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    % GS energy at the last step
    if itL == L
        E_MPS = min(D);
    end
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    oks = (D <= (Etr + tol));
    if itL < L
        MPS{itL} = contract(Anow,3,3,V(:,oks),2,1);
    elseif itL == L
        % Choose GS at the last step
        MPS{itL} = contract(Anow,3,3,V(:,1),2,1);
    end
    Hprev = diag(D(oks));
    disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
        'NK=',sprintf('%i/%i',[size(MPS{itL},3),size(Hnow,2)])]);
end
sprintf('Ground state energy E_MPS: %e',E_MPS)
%% (2) 
% XY Hamiltonian MPO
W = cell(L,1); % MPO W
Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
% First site
W{1} = zeros(1,2,4,2); % ordering: left bottom right top
W{1}(1,:,2,:) = J*Sx; % J*S_1^x
W{1}(1,:,3,:) = J*Sy; % J*S_1^y
W{1}(1,:,4,:) = eye(2); % I
% Last site
W{L} = zeros(4,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = eye(2); % I
W{L}(2,:,1,:) = Sx; % S_L^x
W{L}(3,:,1,:) = Sy; % S_L^y
% Other sites
for itW = (2:L-1)
    W{itW} = zeros(4,2,4,2); % ordering: left bottom right top
    W{itW}(1,:,1,:) = eye(2); % I
    W{itW}(2,:,1,:) = Sx; % S_itW^x
    W{itW}(3,:,1,:) = Sy; % S_itW^y
    W{itW}(4,:,2,:) = J*Sx; % J*S_itW^x
    W{itW}(4,:,3,:) = J*Sy; % J*S_itW^y
    W{itW}(4,:,4,:) = eye(2); % I
end
%% (3)
E_MPO = 1;
for itL = (1:L)
    E_MPO = updateLeft(E_MPO,3,MPS{itL},W{itL},4,MPS{itL});
end
diff = E_MPS - E_MPO;
sprintf('E_MPS - E_MPO: %e',diff)

%% Interative diagoanlization on H_ff (Jordan-Wigner transf.)
% Run TN_0003..._m01.m first and run this code without clear workspace
t = 0.5; % hopping amplitude
L = 30; % chain length
Nkeep = 100; % maximal # of states to keep
tol = Nkeep*100*eps; % numerical tolerance for degeneracy
% GS MPS
MPS = cell(L,1);
% Initialization
Hnow = H0;
[V,D] = eig((Hnow+Hnow')/2);
MPS{1} = contract(A0,3,3,V,2,1);
Hprev = D;
% simga_+
Sp = sqrt(2)*S(:,1,:);
% sigma_-
Sm = sqrt(2)*S(:,2,:);
for itL = (2:L)
    % Spin operator at the current site
    Sp_prev = updateLeft([],[],MPS{itL-1},Sp,3,MPS{itL-1});
    Sm_prev = updateLeft([],[],MPS{itL-1},Sm,3,MPS{itL-1});
    Anow = getIdentity(Hprev,2,I,2);
    Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
    % Spin-spin interaction
    Hxy = -t*updateLeft(Sp_prev,3,Anow,sqrt(2)*S(:,2,:),3,Anow) ...
        - t*updateLeft(Sm_prev,3,Anow,sqrt(2)*S(:,1,:),3,Anow);
    Hnow = Hnow+Hxy;
    [V,D] = eig((Hnow+Hnow')/2);
    [D,ids] = sort(diag(D),'ascend');
    V = V(:,ids);
    % GS energy
    if itL == L
        E_MPS_ff = min(D); % E_MPS of H_ff
    end
    % truncation threshold for energy
    Etr = D(min([numel(D);Nkeep]));
    oks = (D <= (Etr + tol));
    if itL < L
        MPS{itL} = contract(Anow,3,3,V(:,oks),2,1);
    elseif itL == L
        % Choose GS at the last step
        MPS{itL} = contract(Anow,3,3,V(:,1),2,1);
    end
    Hprev = diag(D(oks));
    disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
        'NK=',sprintf('%i/%i',[size(MPS{itL},3),size(Hnow,2)])]);
end
sprintf('GS energy of Fermionic chain: %e',E_MPS_ff)
diff = E_MPS - E_MPS_ff;
sprintf('E_XY - E_ff: %e',diff)

%% MPO for free fermion Hamiltonian
% Run TN_0003..._m01.m  and ...m02.m first and run this code without clear
% workspace
W = cell(L,1); % MPO W
% First site
W{1} = zeros(1,2,4,2); % ordering: left bottom right top
W{1}(1,:,2,:) = -t*sqrt(2)*squeeze(S(:,1,:)); % t*sigma_1^+
W{1}(1,:,3,:) = -t*sqrt(2)*squeeze(S(:,2,:)); % t*sigma_1^-
W{1}(1,:,4,:) = I;
% Last site
W{L} = zeros(4,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = I;
W{L}(2,:,1,:) = sqrt(2)*squeeze(S(:,2,:)); % sigma_itW^-
W{L}(3,:,1,:) = sqrt(2)*squeeze(S(:,1,:)); % sigma_itW^+
% Other sites
for itW = (2:L-1)
    W{itW} = zeros(4,2,4,2); % ordering: left bottom right top
    W{itW}(1,:,1,:) = I;
    W{itW}(2,:,1,:) = sqrt(2)*squeeze(S(:,2,:)); % sigma_itW^-
    W{itW}(3,:,1,:) = sqrt(2)*squeeze(S(:,1,:)); % sigma_itW^+
    W{itW}(4,:,2,:) = -t*sqrt(2)*squeeze(S(:,1,:)); % t*sigma_itW^+
    W{itW}(4,:,3,:) = -t*sqrt(2)*squeeze(S(:,2,:)); % t*sigma_itW^-
    W{itW}(4,:,4,:) = I;
end
E_MPO_ff = 1;
for itL = (1:L)
    E_MPO_ff = updateLeft(E_MPO_ff,3,MPS{itL},W{itL},4,MPS{itL});
end
diff = E_MPS_ff - E_MPO_ff;
sprintf('(E_MPS of H_ff) - (E_MPO of H_ff): %e',diff)

%% MPO for one-particle eigenstates
clear
t = 0.5; % hopping amplitude
[F,Z,I] = getLocalSpace('Fermion');
% % % Varying L and Fixing Nkeep: Start % % %
Lset = [10 20 30]; % chain length
Nkeep = 100; % maximal # of states to keep
% Energy e_k
e_k = cell(numel(Lset),1);
for itL = (1:numel(Lset))
    e_k{itL} = cell(Lset(itL),1);
    e_k{itL}(:) = {1};
end
% % For-loop on L: START % %
for itL = 1:numel(Lset)
    L = Lset(itL);
    % GS MPS for Hff: START %
    tol = Nkeep*100*eps; % numerical tolerance for degeneracy
    H0 = I*0; % 1st site Hamiltonian
    A0 = getIdentity(1,2,I,2); % 1st leg: dummy
    MPS = cell(L,1);
    Hnow = H0;
    [V,D] = eig((Hnow+Hnow')/2);
    MPS{1} = contract(A0,3,3,V,2,1);
    Hprev = D;
    for itL2 = (2:L)
        % Fermion aniihilation operator at the current site
        Fprev = updateLeft([],[],MPS{itL2-1},F,3,MPS{itL2-1});
        Anow = getIdentity(Hprev,2,I,2);
        Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
        % hopping terms
        Hhop = t*updateLeft(Fprev,3,Anow,permute(F,[3 2 1]),3,Anow);
        Hhop = Hhop + Hhop';
        Hnow = Hnow + Hhop;
        [V,D] = eig((Hnow+Hnow')/2);
        [D,ids] = sort(diag(D),'ascend');
        V = V(:,ids);
        % GS energy at the last step
        if itL2 == L
            E_tot = min(D);
        end
        % truncation threshold for energy
        Etr = D(min([numel(D);Nkeep]));
        oks = (D <= (Etr + tol));
        if itL2 < L
            MPS{itL2} = contract(Anow,3,3,V(:,oks),2,1);
        elseif itL2 == L
            % Choose GS at the last step
            MPS{itL2} = contract(Anow,3,3,V(:,1),2,1);
        end
        Hprev = diag(D(oks));
%         disptime(['#',sprintf('%02i/%02i',[itL2,L]),' : ', ...
%             'NK=',sprintf('%i/%i',[size(MPS{itL2},3),size(Hnow,2)])]);
    end
    % GS MPS for Hff: END %
    % MPO for Hff: START %
    W = cell(L,1); % MPO W
    % First site
    W{1} = zeros(1,2,4,2); % ordering: left bottom right top
    W{1}(1,:,2,:) = t*squeeze(F)'; % t*creation
    W{1}(1,:,3,:) = t*squeeze(F); % t*annihilation
    W{1}(1,:,4,:) = I;
    % Last site
    W{L} = zeros(4,2,1,2); % ordering: left bottom right top
    W{L}(1,:,1,:) = I;
    W{L}(2,:,1,:) = squeeze(F); % annihilation
    W{L}(3,:,1,:) = squeeze(F)'; % creation
    % Other sites
    for itL2 = (2:L-1)
        W{itL2} = zeros(4,2,4,2); % ordering: left bottom right top
        W{itL2}(1,:,1,:) = I;
        W{itL2}(2,:,1,:) = squeeze(F); % annihilation
        W{itL2}(3,:,1,:) = squeeze(F)'; % creation
        W{itL2}(4,:,2,:) = t*squeeze(F)'; % t*creation
        W{itL2}(4,:,3,:) = t*squeeze(F); % t*annihilation
        W{itL2}(4,:,4,:) = I;
    end
    % Compare E_tot and E_MPO = <MPS|H_MPO|MPS>
    E_MPO = 1;
    for itL2 = (1:L)
        E_MPO = updateLeft(E_MPO,3,MPS{itL2},W{itL2},4,MPS{itL2});
    end
    diff = E_tot - E_MPO;
    sprintf('E_tot - E_MPO (L=%d, Nkeep=%d): %e',L,Nkeep,diff)
    % MPO for Hff: END %
    % e_k for each k: START %
    % For-loop for k=1,..,L/2
    for itk = (1:L/2)
        Wk = cell(L,1); % MPO for ck
        % First site
        Wk{1} = zeros(1,2,2,2); % ordering: left bottom right top
        Wk{1}(1,:,1,:) = sqrt(2/(L+1))*sin(1*itk*pi/(L+1))*squeeze(F)';
        Wk{1}(1,:,2,:) = I;
        % Last site
        Wk{L} = zeros(2,2,1,2); % ordering: left bottom right top
        Wk{L}(1,:,1,:) = Z;
        Wk{L}(2,:,1,:) = sqrt(2/(L+1))*sin(L*itk*pi/(L+1))*squeeze(F)';
        % Other sites
        for itL2 = (2:L-1)
            Wk{itL2} = zeros(2,2,2,2); % ordering: left bottom right top
            Wk{itL2}(1,:,1,:) = Z;
            Wk{itL2}(2,:,1,:) = sqrt(2/(L+1))*sin(itL2*itk*pi/(L+1))*squeeze(F)';
            Wk{itL2}(2,:,2,:) = I;
        end
        % Apply c_k^\dagger to |MPS>
        ckdag_MPS = cell(L,1);
        for itL2 = (1:L)
            ckdag_MPS{itL2} = contract(Wk{itL2},4,4,MPS{itL2},3,2,[1 4 2 3 5]);
            ckdag_MPS{itL2} = reshape(ckdag_MPS{itL2}, ...
                [size(Wk{itL2},1)*size(MPS{itL2},1), ...
                 size(Wk{itL2},2), ...
                 size(Wk{itL2},3)*size(MPS{itL2},3)]);
        end
        % compute <MPS|ck H_MPO ckdag |MPS>
        for itL2 = (1:L)
            e_k{itL}{itk} = updateLeft(e_k{itL}{itk},3,...
                ckdag_MPS{itL2},W{itL2},4,ckdag_MPS{itL2});
        end
        e_k{itL}{itk} = e_k{itL}{itk} - E_tot;
%         disptime(['#',sprintf('%02i/%02i',[itk,L/2]),' : ', ...
%             sprintf('e_k is %e',e_k{itL}{itk})]);
    end
    e_k{itL} = cell2mat(e_k{itL});
    % e_k for each k: END %
    diff = zeros(L/2,1);
    for itk = (1:L/2)
        diff(itk) = e_k{itL}(itk) - cos(itk*pi/(L+1));
%         sprintf('e_k - eps_k: %e',diff(itk))
    end
    sprintf('max(|e_k - eps_k|) for L=%d, Nkeep=%d: %.4f',L,Nkeep,max(abs(diff)))
end
% % For-loop on L: END % %
% % Plot result: START % %
figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 (numel(Lset)+1)*220+10 280]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [(numel(Lset)+1)*220+10 280]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 (numel(Lset)+1)*220+10 280]);
lgd_set = cell(numel(Lset),1); % set of legends for the last figure
for itL = 1:numel(Lset)
    lgd_set{itL} = ['$L=',sprintf('%d',Lset(itL)),'$'];
end
for itL = 1:numel(Lset)
    L = Lset(itL);
    % plot energies
    subplot(1,numel(Lset)+1,itL);
    set(gca,'units','points');
    set(gca,'innerposition',[220*itL-160,50,150,200]);
    plot((1:L/2),e_k{itL}(1:L/2),'-o','linewidth',2);
    hold on;
    plot((1:L/2),cos((1:L/2)*pi/(L+1)),'--x','linewidth',2);
    hold off;
    box on;
    grid on;
    xlabel('$k$','Interpreter','latex','FontSize',18);
    ylabel('energy','Interpreter','latex','FontSize',18);
    ylim([-0.02 1.02]);
    set(gca,'fontsize',18);
    legend('$e_k$','$\varepsilon_k$', ...
        'Interpreter','latex','FontSize',16,'Location','southwest');
    title(['$L=',sprintf('%d',L),', N_\mathrm{keep}=',sprintf('%d',Nkeep),'$'],...
        'interpreter','latex','fontsize',18);
    % plot energy differences
    subplot(1,numel(Lset)+1,numel(Lset)+1);
    if itL == numel(Lset)
        set(gca,'units','points');
        set(gca,'innerposition',[220*(numel(Lset)+1)-160,50,150,200]);
    end
    diff = zeros(L/2,1);
    for itk = (1:L/2)
        diff(itk) = e_k{itL}(itk) - cos(itk*pi/(L+1));
    end
    hold on;
    plot((1:L/2),diff(1:L/2),'-o','linewidth',2);
    hold off;
    box on;
    grid on;
    xlabel('$k$','Interpreter','latex','FontSize',18);
    ylabel('$e_k-\varepsilon_k$','Interpreter','latex','FontSize',18);
    ylim([-0.005 0.05]);
    set(gca,'fontsize',18);
    if itL == numel(Lset)
        title(['$N_\mathrm{keep}=',sprintf('%d',Nkeep),'$'],...
            'fontsize',18,'interpreter','latex');
        legend(lgd_set, ...
            'Interpreter','latex','FontSize',16,'Location','northwest');
    end
end
% % Plot result: END % %
% % % Varying L and Fixing Neep: END % % %
% % % Varying Nkeep and Fixing L: START % % %
L = 30; % chain length
Nset = [60 80 100]; % maximal # of states to keep
% Energy e_k
e_k = cell(numel(Nset),1);
for itN = (1:numel(Nset))
    e_k{itN} = cell(L,1);
    e_k{itN}(:) = {1};
end
% % For-loop on L: START % %
for itN = 1:numel(Nset)
    Nkeep = Nset(itN);
    % GS MPS for Hff: START %
    tol = Nkeep*100*eps; % numerical tolerance for degeneracy
    H0 = I*0; % 1st site Hamiltonian
    A0 = getIdentity(1,2,I,2); % 1st leg: dummy
    MPS = cell(L,1);
    Hnow = H0;
    [V,D] = eig((Hnow+Hnow')/2);
    MPS{1} = contract(A0,3,3,V,2,1);
    Hprev = D;
    for itL2 = (2:L)
        % Fermion aniihilation operator at the current site
        Fprev = updateLeft([],[],MPS{itL2-1},F,3,MPS{itL2-1});
        Anow = getIdentity(Hprev,2,I,2);
        Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
        % hopping terms
        Hhop = t*updateLeft(Fprev,3,Anow,permute(F,[3 2 1]),3,Anow);
        Hhop = Hhop + Hhop';
        Hnow = Hnow + Hhop;
        [V,D] = eig((Hnow+Hnow')/2);
        [D,ids] = sort(diag(D),'ascend');
        V = V(:,ids);
        % GS energy at the last step
        if itL2 == L
            E_tot = min(D);
        end
        % truncation threshold for energy
        Etr = D(min([numel(D);Nkeep]));
        oks = (D <= (Etr + tol));
        if itL2 < L
            MPS{itL2} = contract(Anow,3,3,V(:,oks),2,1);
        elseif itL2 == L
            % Choose GS at the last step
            MPS{itL2} = contract(Anow,3,3,V(:,1),2,1);
        end
        Hprev = diag(D(oks));
%         disptime(['#',sprintf('%02i/%02i',[itL2,L]),' : ', ...
%             'NK=',sprintf('%i/%i',[size(MPS{itL2},3),size(Hnow,2)])]);
    end
    % GS MPS for Hff: END %
    % MPO for Hff: START %
    W = cell(L,1); % MPO W
    % First site
    W{1} = zeros(1,2,4,2); % ordering: left bottom right top
    W{1}(1,:,2,:) = t*squeeze(F)'; % t*creation
    W{1}(1,:,3,:) = t*squeeze(F); % t*annihilation
    W{1}(1,:,4,:) = I;
    % Last site
    W{L} = zeros(4,2,1,2); % ordering: left bottom right top
    W{L}(1,:,1,:) = I;
    W{L}(2,:,1,:) = squeeze(F); % annihilation
    W{L}(3,:,1,:) = squeeze(F)'; % creation
    % Other sites
    for itL2 = (2:L-1)
        W{itL2} = zeros(4,2,4,2); % ordering: left bottom right top
        W{itL2}(1,:,1,:) = I;
        W{itL2}(2,:,1,:) = squeeze(F); % annihilation
        W{itL2}(3,:,1,:) = squeeze(F)'; % creation
        W{itL2}(4,:,2,:) = t*squeeze(F)'; % t*creation
        W{itL2}(4,:,3,:) = t*squeeze(F); % t*annihilation
        W{itL2}(4,:,4,:) = I;
    end
    % Compare E_tot and E_MPO = <MPS|H_MPO|MPS>
    E_MPO = 1;
    for itL2 = (1:L)
        E_MPO = updateLeft(E_MPO,3,MPS{itL2},W{itL2},4,MPS{itL2});
    end
    diff = E_tot - E_MPO;
    sprintf('E_tot - E_MPO (L=%d, Nkeep=%d): %e',L,Nkeep,diff)
    % MPO for Hff: END %
    % e_k for each k: START %
    % For-loop for k=1,..,L/2
    for itk = (1:L/2)
        Wk = cell(L,1); % MPO for ck
        % First site
        Wk{1} = zeros(1,2,2,2); % ordering: left bottom right top
        Wk{1}(1,:,1,:) = sqrt(2/(L+1))*sin(1*itk*pi/(L+1))*squeeze(F)';
        Wk{1}(1,:,2,:) = I;
        % Last site
        Wk{L} = zeros(2,2,1,2); % ordering: left bottom right top
        Wk{L}(1,:,1,:) = Z;
        Wk{L}(2,:,1,:) = sqrt(2/(L+1))*sin(L*itk*pi/(L+1))*squeeze(F)';
        % Other sites
        for itL2 = (2:L-1)
            Wk{itL2} = zeros(2,2,2,2); % ordering: left bottom right top
            Wk{itL2}(1,:,1,:) = Z;
            Wk{itL2}(2,:,1,:) = sqrt(2/(L+1))*sin(itL2*itk*pi/(L+1))*squeeze(F)';
            Wk{itL2}(2,:,2,:) = I;
        end
        % Apply c_k^\dagger to |MPS>
        ckdag_MPS = cell(L,1);
        for itL2 = (1:L)
            ckdag_MPS{itL2} = contract(Wk{itL2},4,4,MPS{itL2},3,2,[1 4 2 3 5]);
            ckdag_MPS{itL2} = reshape(ckdag_MPS{itL2}, ...
                [size(Wk{itL2},1)*size(MPS{itL2},1), ...
                 size(Wk{itL2},2), ...
                 size(Wk{itL2},3)*size(MPS{itL2},3)]);
        end
        % compute <MPS|ck H_MPO ckdag |MPS>
        for itL2 = (1:L)
            e_k{itN}{itk} = updateLeft(e_k{itN}{itk},3,...
                ckdag_MPS{itL2},W{itL2},4,ckdag_MPS{itL2});
        end
        e_k{itN}{itk} = e_k{itN}{itk} - E_tot;
%         disptime(['#',sprintf('%02i/%02i',[itk,L/2]),' : ', ...
%             sprintf('e_k is %e',e_k{itN}{itk})]);
    end
    e_k{itN} = cell2mat(e_k{itN});
    % e_k for each k: END %
    diff = zeros(L/2,1);
    for itk = (1:L/2)
        diff(itk) = e_k{itN}(itk) - cos(itk*pi/(L+1));
%         sprintf('e_k - eps_k: %e',diff(itk))
    end
    sprintf('max(|e_k - eps_k|) for L=%d, Nkeep=%d: %.4f',L,Nkeep,max(abs(diff)))
end
% % For-loop on L: END % %
% % Plot result: START % %
figure;
set(gcf,'units','points');
set(gcf,'position',[0 0 (numel(Nset)+1)*220+10 280]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [(numel(Nset)+1)*220+10 280]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 (numel(Nset)+1)*220+10 280]);
lgd_set = cell(numel(Nset),1); % set of legends for the last figure
for itN = 1:numel(Nset)
    lgd_set{itN} = ['$N_\mathrm{keep}=',sprintf('%d',Nset(itN)),'$'];
end
for itN = 1:numel(Nset)
    Nkeep = Nset(itN);
    % plot energies
    subplot(1,numel(Nset)+1,itN);
    set(gca,'units','points');
    set(gca,'innerposition',[220*itN-160,50,150,200]);
    plot((1:L/2),e_k{itN}(1:L/2),'-o','linewidth',2);
    hold on;
    plot((1:L/2),cos((1:L/2)*pi/(L+1)),'--x','linewidth',2);
    hold off;
    box on;
    grid on;
    xlabel('$k$','Interpreter','latex','fontsize',18);
    ylabel('energy','Interpreter','latex','fontsize',18);
    ylim([-0.02 1.02]);
    set(gca,'fontsize',18);
    legend('$e_k$','$\varepsilon_k$', ...
        'Interpreter','latex','fontsize',18,'Location','southwest');
    title(['$L=',sprintf('%d',L),', N_\mathrm{keep}=',sprintf('%d',Nkeep),'$'],...
        'interpreter','latex','fontsize',18);
    % plot energy differences
    subplot(1,numel(Nset)+1,numel(Nset)+1);
    if itN == numel(Nset)
        set(gca,'units','points');
        set(gca,'innerposition',[220*(numel(Nset)+1)-160,50,150,200]);
    end
    diff = zeros(L/2,1);
    for itk = (1:L/2)
        diff(itk) = e_k{itN}(itk) - cos(itk*pi/(L+1));
    end
    hold on;
    plot((1:L/2),diff(1:L/2),'-o','linewidth',2);
    hold off;
    box on;
    grid on;
    xlabel('$k$','Interpreter','latex','fontsize',18);
    ylabel('$e_k-\varepsilon_k$','Interpreter','latex','fontsize',18);
    ylim([-0.02 0.12]);
    set(gca,'fontsize',18);
    if itN == numel(Nset)
        title(['$L=',sprintf('%d',L),'$'],'fontsize',18,'interpreter','latex');
        legend(lgd_set, ...
            'Interpreter','latex','FontSize',18,'Location','northwest');
    end
end
% % Plot result: END % %
% %     % Varying Nkeep and Fixing L: END % % %