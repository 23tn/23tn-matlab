% test doCLD
clear
Lambda = 2;
N = 100;
[ff,gg] = doCLD([-1 1],[1 1],Lambda,N);
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 300]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 300]);
set(gca,'units','points');
set(gca,'innerposition',[100,50,300,225]);
plot((1:N),ff.*2.^((1:N)'/2),'-','linewidth',2);
hold on
plot((1:N),gg.*2.^((1:N)'/2),'--','linewidth',2);
hold off
xlabel('$\ell$','Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend('$t_\ell \cdot \Lambda^{\ell/2}$',...
    '$\epsilon_\ell \cdot \Lambda^{\ell/2}$', ...
    'Interpreter','latex','fontsize',18,'location','east');