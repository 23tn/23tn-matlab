% Lanczos method for the matrix exponential
% To test you code, we take an arbitrary matrix of 1000x1000
ndim = 1000;
Ham = magic(ndim);
Ham = Ham+Ham';
phi = repmat([1;-1], [ndim/2,1]);
phi = phi/norm(phi);
tau = 0.1i;
% % % Lanczos method: START % % %
m = 5; % number of Lanczos vectors
% Minimum absolute value of the 1st diagonal element of the Hamiltonian in 
% the Krylov subspace
minH = 1e-10;
phi = [phi zeros(ndim,m-1)]; % Krylov basis
% In the Krylov basis, the Hamiltonian becomes tridiagonal
ff = zeros(m-1,1); % 1st diagonal
gg = zeros(m,1); % main diagonal
for itm = (1:m)
    % contract Hamiltonian with ket tensor
    Ctmp = Ham*phi(:,itm);
    gg(itm) = phi(:,itm)'*Ctmp; % diagonal element; "on-site energy"
    if itm < m
        % orthogonalize Atmp w.r.t. the previous ket tensors
        Ctmp = Ctmp - phi(:,(1:itm))*(phi(:,(1:itm))'*Ctmp);
        % twice, to reduce numerical noise
        Ctmp = Ctmp - phi(:,(1:itm))*(phi(:,(1:itm))'*Ctmp);
        % norm
        ff(itm) = norm(Ctmp);
        if ff(itm) > minH
            phi(:,itm+1) = Ctmp/ff(itm);
        else
            % stop iteration; truncate ff, gg
            ff(itm:end) = [];
            gg(itm+1:end) = [];
            phi(:,(itm+1):end) = [];
            break;
        end
    end
end
% Hamiltonian in the Krylov basis
Hkr = diag(ff,1);
Hkr = Hkr + Hkr' + diag(gg);
% % % Lanczos method: END % % %
% % % convergence check: START % % %
phi_tau = expm(-tau*Ham)*phi(:,1);
phikr_tau = phi*expm(-tau*Hkr);
phikr_tau = phikr_tau(:,1);
err = norm(phi_tau-phikr_tau);
fprintf([sprintf('err (m=%d',m),sprintf('): %.4e',err),'\n']);
% % % convergence check: END % % %

%%
% Domain wall motion in XY model
clear
% exact values of magnetization in the infinite chain limit 
% Ref: T. Antal, et al., Phys.~Rev.~E 59, 4912 (1999)
Lsite = 50;
Tstep = [0:0.05:20];
fvals = zeros(numel(Tstep),Lsite-1);
for it = (1:size(fvals,2))
  fvals(:,it) = (besselj(it-(Lsite/2),Tstep(:))).^2;
end
fvals = -0.5*fvals;
Oexact = zeros(numel(Tstep),Lsite/2); % exact values
for it = (1:(Lsite/2))
  Oexact(:,it) = sum(fvals(:,(Lsite/2-it+1):(it+Lsite/2-1)),2);
end
Oexact = [-fliplr(Oexact),Oexact]; % format:[Time]x[Position]

% % % initialization: START % % %
% Local operators
[S,I] = getLocalSpace('Spin',1/2);

% XY model
% XY Hamiltonian MPO
Hs = cell(Lsite,1); % MPO Hs
Sx = (S(:,1,:)+S(:,2,:))/sqrt(2);
Sy = (S(:,1,:)-S(:,2,:))/sqrt(2)/1i;
% First site
Hs{1} = zeros(1,2,4,2); % ordering: left bottom right top
Hs{1}(1,:,2,:) = Sx; % S_1^x
Hs{1}(1,:,3,:) = Sy; % S_1^y
Hs{1}(1,:,4,:) = eye(2); % I
% Last site
Hs{Lsite} = zeros(4,2,1,2); % ordering: left bottom right top
Hs{Lsite}(1,:,1,:) = eye(2); % I
Hs{Lsite}(2,:,1,:) = Sx; % S_L^x
Hs{Lsite}(3,:,1,:) = Sy; % S_L^y
% Other sites
for itH = (2:Lsite-1)
    Hs{itH} = zeros(4,2,4,2); % ordering: left bottom right top
    Hs{itH}(1,:,1,:) = eye(2); % I
    Hs{itH}(2,:,1,:) = Sx; % S_itW^x
    Hs{itH}(3,:,1,:) = Sy; % S_itW^y
    Hs{itH}(4,:,2,:) = Sx; % S_itW^x
    Hs{itH}(4,:,3,:) = Sy; % S_itW^y
    Hs{itH}(4,:,4,:) = eye(2); % I
end

% operator to measure magnetization
O = squeeze(S(:,3,:));

m = 5; % number of Lanczos vectors
% Minimum absolute value of the 1st diagonal element of the Hamiltonian in 
% the Krylov subspace
minH = 1e-10;
% % % initialization: END % % %

% % % TDVP: START % % %
dt = 0.05;
Nstep = numel(Tstep)-1;
Nkeep = 20;
ts = dt*(1:Nstep).';

% initialize MPS: product state such that the left half of the chain is
% up-polarized and the right half is down-polarized.
M = cell(1,Lsite);
M{1} = zeros(1,size(I,2),Nkeep);
M(2:Lsite-1) = {zeros(Nkeep,size(I,2),Nkeep)};
M{Lsite} = zeros(Nkeep,size(I,2),1);
for itL = (1:Lsite)
    if itL <= (Lsite/2)
        M{itL}(1,1,1) = 1;
    else
        M{itL}(1,2,1) = 1;
    end
end

% show message
disp('TDVP : Real-time evolution with local measurements');
fprintf(['Lsite = ',sprintf('%i',Lsite),', Nkeep = ',sprintf('%i',Nkeep), ...
    ', dt = ',sprintf('%.4g',dt),', tmax = ',sprintf('%g',ts(end)), ...
    ' (',sprintf('%.4g',Nstep),' steps)\n']);

% results
Otdvp = zeros(Nstep+1,Lsite);
% Measurement of local operators O; currently M is in site-canonical
% with respect to site 1
MM = []; % contraction of bra/ket tensors from left
for itL = (1:Lsite)
    Otdvp(1,itL) = trace(updateLeft(MM,2,M{itL},O,2,M{itL}));
    MM = updateLeft(MM,2,M{itL},[],[],M{itL});
end

% bring into site-canonical form with respect to site 1
[M,S] = canonForm(M,1,'-k'); % one should not truncate zero singular values 
        % and the corresponding singular vectors, since it will decrease
        % the Hilbert space, which should be avoided for the one-site
        % update algorithms (like this one-site TDVP)
M{1} = contract(M{1},3,3,diag(S),2,1);

disptime('Time evolution: start.');
tobj = tic2;
for itt = (1:Nstep)
    [M,Ovals] = TDVP_1site (M,Hs,O,Nkeep,dt);
    Otdvp(itt+1,:) = Ovals;
    if (mod(itt,round(Nstep/10)) == 0) || (itt == Nstep)
        disptime(['#',sprintf('%i/%i',[itt,Nstep]), ...
            ' : t = ',sprintf('%g/%g',[ts(itt),ts(end)])]);
    end
end
toc2(tobj,'-v');
chkmem;
% % % TDVP: END % % %

% display results
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 970 310]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [970 310]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 970 310]);
subplot(1,3,1);
set(gca,'units','points');
imagesc((1:Lsite),Tstep,Oexact);
xlabel('site $\ell$','Interpreter','latex','fontsize',18);
ylabel('time $t$','Interpreter','latex','fontsize',18);
colorbar('eastoutside');
set(gca,'innerposition',[70,50,220,220]);
set(gca,'FontSize',18);
title('$\langle S_\mathrm{z}(t) \rangle_\mathrm{exact}$','interpreter','latex');
subplot(1,3,2);
set(gca,'units','points');
imagesc((1:Lsite),Tstep,real(Otdvp));
xlabel('site $\ell$','Interpreter','latex','fontsize',18);
ylabel('time $t$','Interpreter','latex','fontsize',18);
colorbar('eastoutside');
set(gca,'innerposition',[400,50,220,220]);
set(gca,'FontSize',18);
title('$\langle S_\mathrm{z}(t) \rangle_\mathrm{TDVP}$','interpreter','latex');
subplot(1,3,3);
set(gca,'units','points');
imagesc((1:Lsite),Tstep,Oexact-real(Otdvp));
xlabel('site $\ell$','Interpreter','latex','fontsize',18);
ylabel('time $t$','Interpreter','latex','fontsize',18);
colorbar('eastoutside');
set(gca,'innerposition',[730,50,220,220]);
title(['$\langle S_\mathrm{z}(t) \rangle_\mathrm{exact}',...
    '-\langle S_\mathrm{z}(t) \rangle_\mathrm{TDVP}$'],'interpreter','latex');
set(gca,'FontSize',18);