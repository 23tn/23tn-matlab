% one-site energy variance
clear
L = 100; % chain length
Dset = [10 20 30 40]; % bound dimensions
var1s = zeros(numel(Dset),1); % one-site energy variance
[F,Z,I] = getLocalSpace('Fermion'); % operators of a single site
% MPO representation of spinless free fermions
% % MPO: START % %
W = cell(L,1); % MPO
% First site
W{1} = zeros(1,2,4,2); % ordering: left bottom right top
W{1}(1,:,2,:) = -squeeze(F)'; % -creation
W{1}(1,:,3,:) = -squeeze(F); % -annihilation
W{1}(1,:,4,:) = I;
% Last site
W{L} = zeros(4,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = I;
W{L}(2,:,1,:) = squeeze(F); % annihilation
W{L}(3,:,1,:) = squeeze(F)'; % creation
% Other sites
for itL = (2:L-1)
    W{itL} = zeros(4,2,4,2); % ordering: left bottom right top
    W{itL}(1,:,1,:) = I;
    W{itL}(2,:,1,:) = squeeze(F); % annihilation
    W{itL}(3,:,1,:) = squeeze(F)'; % creation
    W{itL}(4,:,2,:) = -squeeze(F)'; % -creation
    W{itL}(4,:,3,:) = -squeeze(F); % -annihilation
    W{itL}(4,:,4,:) = I;
end
% % MPO: END % %
for itD = (1:numel(Dset))
    D = Dset(itD);
    % % initial GS MPS: START % %
    tol = D*100*eps; % numerical tolerance for degeneracy
    H0 = I*0; % 1st site Hamiltonian
    A0 = getIdentity(1,2,I,2); % 1st leg: dummy
    MPSinit = cell(L,1); % initial GS MPS from iterative diagonalization
    Hnow = H0;
    [V,E] = eig((Hnow+Hnow')/2);
    MPSinit{1} = contract(A0,3,3,V,2,1);
    Hprev = E;
    for itL = (2:L)
        % Fermion aniihilation operator at the current site
        Fprev = updateLeft([],[],MPSinit{itL-1},F,3,MPSinit{itL-1});
        Anow = getIdentity(Hprev,2,I,2);
        Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
        % hopping terms
        Hhop = -updateLeft(Fprev,3,Anow,permute(F,[3 2 1]),3,Anow);
        Hhop = Hhop + Hhop';
        Hnow = Hnow + Hhop;
        [V,E] = eig((Hnow+Hnow')/2);
        [E,ids] = sort(diag(E),'ascend');
        V = V(:,ids);
        % truncation threshold for energy
        Etr = E(min([numel(E);D]));
        oks = (E <= (Etr + tol));
        if itL < L
            MPSinit{itL} = contract(Anow,3,3,V(:,oks),2,1);
        elseif itL == L
            % Choose GS at the last step
            MPSinit{itL} = contract(Anow,3,3,V(:,1),2,1);
        end
        Hprev = diag(E(oks));
        disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
            'NK=',sprintf('%i/%i',[size(MPSinit{itL},3),size(Hnow,2)])]);
    end
    % % initial GS MPS: END % %
    % % 1s DMRG: BEGIN % %
    Nsweep = 100; % number of DMRG sweeps will be 2*Nsweep
    Econv = 1e-10; % Convergence criterion for energy
    [MPS1s,E0,~] = DMRG_1site(W,MPSinit,D,Nsweep,'Econv',Econv);
    % % 1s DMRG: END % %
    % % one-site energy variance: START % %
    for itL=(1:L)
        % initialize MPSs
        MPS = MPS1s; % initialize MPS1s
        % % get left canonical tensors : START % %
        for itL2 = (1:itL-1)
            [U,S,V] = svdTr(MPS{itL2},3,[1 2],[],[]);
            MPS{itL2} = U;
            MPS{itL2+1} = contract(diag(S)*V,2,2,MPS{itL2+1},3,1);
        end
        % % get left canonical tensors : END % %
        % % get right canonical tensors : START % %
        for itL2 = (L:-1:itL+1)
            [U,S,V] = svdTr(MPS{itL2},3,1,[],[]);
            MPS{itL2} = V;
            MPS{itL2-1} = contract(MPS{itL2-1},3,3,U*diag(S),2,1);
        end
        % % get right canonical tensors : END % %
        Hleft = 1; % left environment for MPS1s
        Hright = 1; % right environment for MPS1s
        % % update left environments : START % %
        for itL2 = (1:itL-1)
            M = MPS{itL2}; % ordering : left bottom right
            Wleft = W{itL2}; % ordering : left bottom right up
            Hleft = updateLeft(Hleft,3,M,Wleft,4,M);
        end
        % % update left environments : END % %
        % % update right environments : START % %
        for itL2 = (L:-1:itL+1)
            % note: leg orderings are changed to use updateLeft
            M = permute(MPS{itL2},[3 2 1]); % ordering : right bottom left
            Wright = permute(W{itL2},[3 2 1 4]); % ordering : right bottom left up
            Hright = updateLeft(Hright,3,M,Wright,4,M);
        end
        % % update right environments : END % %
        % % get variance : START % %
        C = MPS{itL}; % C at isometry center for MPS1s
        Hloc = W{itL}; % W at isometry center
        % HC: contracting Hleft, Hright, Wcenter, and Ccenter
        HC = contract(Hleft,3,3,C,3,1);
        HC = contract(HC,4,[2 3],Hloc,4,[1 4]);
        HC = contract(HC,4,[2 4],Hright,3,[3 2]);
        % A: left isometry from C
        A = svdTr(C,3,[1 2],[],[]);
        AHC = contract(conj(A),3,[1 2],HC,3,[1 2]);
        AHC = contract(A,3,3,AHC,2,1);
        PHC = HC - AHC;
        var1s(itD) = var1s(itD) + norm(PHC(:))^2;
        % % get variance : END % %
    end
    fprintf('\n%% %% %% 1s var (D=%d) : %4g %% %% %%\n\n',D,var1s(itD));
    % % one-site energy variance: END % %
end

%%
% two-site energy variance
clear
L = 100; % chain length
Dset = [10 20 30 40]; % bound dimensions
var2s = zeros(numel(Dset),1); % two-site energy variance
[F,Z,I] = getLocalSpace('Fermion'); % operators of a single site
% MPO representation of spinless free fermions
% % MPO: START % %
W = cell(L,1); % MPO
% First site
W{1} = zeros(1,2,4,2); % ordering: left bottom right top
W{1}(1,:,2,:) = -squeeze(F)'; % -creation
W{1}(1,:,3,:) = -squeeze(F); % -annihilation
W{1}(1,:,4,:) = I;
% Last site
W{L} = zeros(4,2,1,2); % ordering: left bottom right top
W{L}(1,:,1,:) = I;
W{L}(2,:,1,:) = squeeze(F); % annihilation
W{L}(3,:,1,:) = squeeze(F)'; % creation
% Other sites
for itL = (2:L-1)
    W{itL} = zeros(4,2,4,2); % ordering: left bottom right top
    W{itL}(1,:,1,:) = I;
    W{itL}(2,:,1,:) = squeeze(F); % annihilation
    W{itL}(3,:,1,:) = squeeze(F)'; % creation
    W{itL}(4,:,2,:) = -squeeze(F)'; % -creation
    W{itL}(4,:,3,:) = -squeeze(F); % -annihilation
    W{itL}(4,:,4,:) = I;
end
% % MPO: END % %
for itD = (1:numel(Dset))
    D = Dset(itD);
    % % initial GS MPS: START % %
    tol = D*100*eps; % numerical tolerance for degeneracy
    H0 = I*0; % 1st site Hamiltonian
    A0 = getIdentity(1,2,I,2); % 1st leg: dummy
    MPSinit = cell(L,1); % initial GS MPS from iterative diagonalization
    Hnow = H0;
    [V,E] = eig((Hnow+Hnow')/2);
    MPSinit{1} = contract(A0,3,3,V,2,1);
    Hprev = E;
    for itL = (2:L)
        % Fermion aniihilation operator at the current site
        Fprev = updateLeft([],[],MPSinit{itL-1},F,3,MPSinit{itL-1});
        Anow = getIdentity(Hprev,2,I,2);
        Hnow = updateLeft(Hprev,2,Anow,[],[],Anow);
        % hopping terms
        Hhop = -updateLeft(Fprev,3,Anow,permute(F,[3 2 1]),3,Anow);
        Hhop = Hhop + Hhop';
        Hnow = Hnow + Hhop;
        [V,E] = eig((Hnow+Hnow')/2);
        [E,ids] = sort(diag(E),'ascend');
        V = V(:,ids);
        % truncation threshold for energy
        Etr = E(min([numel(E);D]));
        oks = (E <= (Etr + tol));
        if itL < L
            MPSinit{itL} = contract(Anow,3,3,V(:,oks),2,1);
        elseif itL == L
            % Choose GS at the last step
            MPSinit{itL} = contract(Anow,3,3,V(:,1),2,1);
        end
        Hprev = diag(E(oks));
        disptime(['#',sprintf('%02i/%02i',[itL,L]),' : ', ...
            'NK=',sprintf('%i/%i',[size(MPSinit{itL},3),size(Hnow,2)])]);
    end
    % % initial GS MPS: END % %
    % % 1s DMRG: BEGIN % %
    Nsweep = 100; % number of DMRG sweeps will be 2*Nsweep
    Econv = 1e-10; % Convergence criterion for energy
    [MPS1s,E0,~] = DMRG_1site(W,MPSinit,D,Nsweep,'Econv',Econv);
    % % 1s DMRG: END % %
    % % one-site energy variance: START % %
    for itL=(1:L-1)
        % initialize MPSs
        MPS = MPS1s; % initialize MPS1s
        % % get left canonical tensors : START % %
        for itL2 = (1:itL-1)
            [U,S,V] = svdTr(MPS{itL2},3,[1 2],[],[]);
            MPS{itL2} = U;
            MPS{itL2+1} = contract(diag(S)*V,2,2,MPS{itL2+1},3,1);
        end
        % % get left canonical tensors : END % %
        % % get right canonical tensors : START % %
        for itL2 = (L:-1:itL+1)
            [U,S,V] = svdTr(MPS{itL2},3,1,[],[]);
            MPS{itL2} = V;
            MPS{itL2-1} = contract(MPS{itL2-1},3,3,U*diag(S),2,1);
        end
        % % get right canonical tensors : END % %
        Hleft = 1; % left environment for MPS1s
        Hright = 1; % right environment for MPS1s
        % % update left environments : START % %
        for itL2 = (1:itL-1)
            M = MPS{itL2}; % ordering : left bottom right
            Wleft = W{itL2}; % ordering : left bottom right up
            Hleft = updateLeft(Hleft,3,M,Wleft,4,M);
        end
        % % update left environments : END % %
        % % update right environments : START % %
        for itL2 = (L:-1:itL+2)
            % note: leg orderings are changed to use updateLeft
            M = permute(MPS{itL2},[3 2 1]); % ordering : right bottom left
            Wright = permute(W{itL2},[3 2 1 4]); % ordering : right bottom left up
            Hright = updateLeft(Hright,3,M,Wright,4,M);
        end
        % % update right environments : END % %
        % % get variance : START % %
        C = MPS{itL}; % C at isometry center
        B = MPS{itL+1}; % B at the site itL+1
        % Merging physical legs of C and B
        I_CB = getIdentity(C,2,B,2);
        CB = contract(C,3,2,I_CB,3,1);
        CB = contract(CB,4,[2 3],B,3,[1 2]);
        % Merging physcial legs of W and W
        I_WW = getIdentity(W{itL},2,W{itL+1},2);
        WW = contract(W{itL},4,2,I_WW,3,1);
        WW = contract(WW,5,[2 4],W{itL+1},4,[1 2]);
        WW = contract(WW,5,[2 5],I_WW,3,[1 2]);
        % HCB: contracting Hleft, Hright, WW, and CB
        HCB = contract(Hleft,3,3,CB,3,1);
        HCB = contract(HCB,4,[2 3],WW,4,[1 4]);
        HCB = contract(HCB,4,[2 4],Hright,3,[3 2]);
        HCB = contract(HCB,3,2,I_WW,3,3,[1 3 4 2]);
        % A: left isometry from C
        A = svdTr(C,3,[1 2],[],[]);
        AHCB = contract(conj(A),3,[1 2],HCB,4,[1 2]);
        AHCB = contract(A,3,3,AHCB,3,1);
        BHCB = contract(HCB,4,[3 4],conj(B),3,[2 3]);
        BHCB = contract(BHCB,3,3,B,3,1);
        ABHCB = contract(AHCB,4,[3 4],conj(B),3,[2 3]);
        ABHCB = contract(ABHCB,3,3,B,3,1);
        PHCB = HCB - AHCB - BHCB + ABHCB;
        var2s(itD) = var2s(itD) + norm(PHCB(:))^2;
        % % get variance : END % %
    end
    fprintf('\n%% %% %% 2s var (D=%d) : %4g %% %% %%\n\n',D,var2s(itD));
    % % two-site energy variance: END % %
end
% display result
figure
set(gcf,'units','points');
set(gcf,'position',[0 0 450 300]);
set(gcf,'paperunits', 'points');
set(gcf,'papersize', [450 300]);
set(gcf,'paperpositionmode', 'manual');
set(gcf,'paperposition', [0 0 450 300]);
set(gca,'units','points');
set(gca,'innerposition',[100,50,300,225]);
semilogy(Dset,var2s,'-o','linewidth',2);
xlabel('$D$','Interpreter','latex','fontsize',18);
ylabel('energy variance','Interpreter','latex','fontsize',18);
set(gca,'FontSize',18);
set(gca,'yminortick','on');
set(gca,'ticklength',[0.04 0.1]);
legend('$\Delta^{2\perp}_E$', ...
    'Interpreter','latex','fontsize',18,'location','northeast');