clear;

% input parameters
v = 1; 
L = 50;

H = tightbindinghamiltonian(L,v);

% eigenvalue decomposition
[U,D] = eig(H);
D = diag(D);

p1 = figure;
ax1 = subplot(1,1,1,'Parent',p1);
box on; grid on;
plot(1:L,D,'LineWidth',3,'LineStyle','-');
xlabel('$\alpha$','Interpreter','latex');
ylabel('$E_{\alpha}/v$','Interpreter','latex');
ax1.LineWidth = 1;
ax1.FontSize = 15;

%%
N = 20;

dens = localdensity(U,N);
particlenumber = sum(dens);
sprintf('Particle number is %.2f should be %i.',particlenumber,N)

p2 = figure;
ax2 = subplot(1,1,1,'Parent',p2);
box on; grid on;
plot(1:L,dens,'LineWidth',3,'LineStyle','-');
xlabel('site $\ell$','Interpreter','latex');
ylabel('density $n_\ell$','Interpreter','latex');
ax2.LineWidth = 1;
ax2.FontSize = 15;

%%
densdens = densitydensitycorrelator(U,N);

p3 = figure;
ax3 = subplot(1,1,1,'Parent',p3);
box on; grid on;
plot(1:L,densdens,'LineWidth',3,'LineStyle','-');
xlabel('site','Interpreter','latex');
ylabel('density','Interpreter','latex');
ax3.LineWidth = 1;
ax3.FontSize = 15;

%%
% Plot the real and imaginary part as well as the absolute value of the
% propagator.

times = (0:0.1:30);
sites = [1 floor(L/3) floor(L/2)];

propag1 = zeros(numel(times),1);
propag2 = zeros(numel(times),1);
propag3 = zeros(numel(times),1);

for t = (1:numel(times))
    propag_tmp = propagator(E,U,N,times(t));
    propag1(t) = propag_tmp(sites(1),floor(L/2));

    propag_tmp = propagator(E,U,N,times(t));
    propag2(t) = propag_tmp(sites(2),floor(L/2));
    
    propag_tmp = propagator(E,U,N,times(t));
    propag3(t) = propag_tmp(sites(3),floor(L/2));
end

p4 = figure;
ax41 = subplot(3,1,1,'Parent',p4);
hold on; box on; grid on;
plot(times,real(propag1),'LineWidth',2,'LineStyle','-');
plot(times,real(propag2),'LineWidth',2,'LineStyle','-');
plot(times,real(propag3),'LineWidth',2,'LineStyle','-');
hold off;
xlabel('t','Interpreter','latex');
ylabel('$\Re G_{\ell,\mathcal{L}/2}(t)$','Interpreter','latex');
ax41.LineWidth = 1;
ax41.FontSize = 15;
legend('$\ell=1$','$\ell=\mathcal{L}/3$','$\ell=\mathcal{L}/2$', ...
    'interpreter','latex','NumColumns',3);

ax42 = subplot(3,1,2,'Parent',p4);
hold on; box on; grid on;
plot(times,imag(propag1),'LineWidth',2,'LineStyle','-');
plot(times,imag(propag2),'LineWidth',2,'LineStyle','-');
plot(times,imag(propag3),'LineWidth',2,'LineStyle','-');
hold off;
xlabel('t','Interpreter','latex');
ylabel('$\Im G_{\ell,\mathcal{L}/2}(t)$','Interpreter','latex');
ax42.LineWidth = 1;
ax42.FontSize = 15;

ax43 = subplot(3,1,3,'Parent',p4);
hold on; box on; grid on;
plot(times,abs(propag1),'LineWidth',2,'LineStyle','-');
plot(times,abs(propag2),'LineWidth',2,'LineStyle','-');
plot(times,abs(propag3),'LineWidth',2,'LineStyle','-');
hold off;
xlabel('t','Interpreter','latex');
ylabel('$\left| G_{\ell,\mathcal{L}/2}(t) \right|$','Interpreter','latex');
ax43.LineWidth = 1;
ax43.FontSize = 15;
