# 2023 Tensor Networks Lecture - MATLAB Code Repository

This is the git repository for MATLAB version of the [2023 tensor networks lecture at LMU Munich](https://moodle.lmu.de/course/view.php?id=27771).

Please read readme.pdf or readme.mlx for the instruction.