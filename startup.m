% < Description >
% startup.m for the lecture course "TVI/TMP: Tensor Networks". It
% (i) resets the path; 
% (ii) add the subdirectories in the directory (in which this 'startup.m'
%       file lies) to the path;
% (iii) clear variables in workspace;
% (iv) and show a startup message.
%
% Written by S.Lee (Apr.21,2017)
% Updated by S.Lee (Apr.17,2019): Updated documentation. Show a startup
%       message.
% Updated by S.Lee (May 28,2019;Jul.08,2019): Added new directories to the
%       path. 
% Updated by S.Lee (Apr 20,2020): Minor fix.


% % Reset path
% temporarily turn off warning; if warning is not turned off above, MATLAB
% might give warning
warning('off','MATLAB:dispatcher:pathWarning');
restoredefaultpath; % reset path to default
warning('on','MATLAB:dispatcher:pathWarning'); % turn on warning back
% NOTE: if you want to keep custom path besides TN folder, comment out the
% above three commands.

% % Add to path
addpath(genpath(fileparts(mfilename('fullpath'))));
% add the directory (at which this "startup.m" lies) and its subdirectories
% to the path

% % Clear variables in memory
clear

% % startup messeage
fprintf('startup.m | "Tensor Networks" tutorial material\n');
chkmem;
