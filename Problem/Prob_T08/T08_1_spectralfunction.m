function [odisc,Adisc] = getAdisc (Inrg,Op,Z,varargin)
% < Description >
%
% [odisc,Adisc] = getAdisc (Inrg,Op,Z [, option]); % for anti-commuting operators
% [odisc,Adisc] = getAdisc (Inrg,Op,[] [, option]); % for commuting operators
%
% Obtain the discrete spectral function, given the operators at the
% impurity (site s00) and the NRG information. Using the Lehmann
% representation and the complete basis, we can obtain the spectral
% function as a collection of delta functions with corresponding weights. The
% spectral weights are binned in the vector Adisc, where each bin of Adisc
% represents the narrow frequency interval centered at the corresponding
% element of odisc.
%
% < Input >
% Inrg : [struct] NRG information obtained after running NRG_IterDiag.
% Op : [tensor] Operator acting at the impurity (site s00).
% Z : [tensor] Fermionic sign operator. The sign \zeta is 1 if the input
%		Z is not empty or -1 (fermionic) if Z is empty (bosonic).
%		Thus, if the operator Op is bosonic, set Z as empty []. 
%
% < Option >
% 'emin', .. : [number] Minimum (in absolute value) frequency limit.
%               (Default: 1e-12)
% 'emax', .. : [number] Maximum (in absolute value) frequency limit.
%               (Default: 1e2)
% 'estep', .. : [number] Number of bins per decade (frequency value 1 ->
%               10).
%               (Default: 200)
%
% < Output >
% odisc : [vector] Logarithmic frequency grid.
% Adisc : [vector] Discrete spectral weights corresponding to odisc.

%%
logemin = log10(emin);
logemax = log10(emax);

odisc = 10.^(((logemin*estep):(logemax*estep))/estep);
Adisc = zeros(numel(odisc),2); % column 1: negative freq., col 2: positive freq.

%%
% indexing for frequency
ids = round((log10(abs(E21))+log10(EScale)-logemin)*estep)+1;
ids(ids<1) = 1;
ids(ids>nodisc) = numel(odisc);

% indexing for sign
sgn = (E21 >= 0)+1;

Adisc = Adisc + accumarray([ids,sgn],A21,[numel(odisc),2]);