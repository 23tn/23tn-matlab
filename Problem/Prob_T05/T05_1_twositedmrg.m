function [M,E0,Eiter,dw] = DMRG_2site(Hs,Minit,alpha,Nkeep,Nsweep,varargin)
% < Description >
%
% [M,E0,Eiter,dw] = DMRG_2site(Hs,Minit,alpha,Nkeep,Nsweep [, option])
%
% Two-site density-matrix renormalization group (DMRG)
% calculation to search for the ground state and its energy
% of a one-dimensional system, whose Hamiltonian is given
% by the matrix product operator Hs. Based on modifications 
% of DMRG_1site.
%
% < Input >
% Hs : [1 x N cell array] Matrix product operator (MPO) of the
%       Hamiltonian. Each Hs{n} acts on site n, and is a rank-4
%       tensor. The order of legs of Hs{n} is
%       left-bottom-right-top, where bottom (top) leg is to be
%       contracted with bra (ket) tensor. The length N of Hs,
%       i.e., numel(Hs), determines the chain length.
% Minit : [1 x N cell array] Initial MPS from which to start the
%       ground state search
% alpha : [numeric] bond growth factor. The bond dimension is 
%	grown from its initial value Di to its final value 
%	Df = min(\alpha*Di,Nkeep)
% Nkeep : [numeric] Maximum bond dimension of the matrix product
%       state (MPS) to consider.
% Nsweep : [numeric] Number of sweeps will be 2*Nsweep, as there
%       are Nsweep times of round trip
%       (right -> left, left -> right).
%
% <Option>
% 'Econv',.. : [numeric] Convergence criterion for energy. If 
%	Einit - Efin < Econv, stop sweeping even if less than
%	Nsweep sweeps have been done so far. Here, Einit and
%	Efin are the energies before and after one 
%	(right -> left, left -> right) round trip, respectively.
%	(Default: -inf, i.e. no energy convergence criterion.)
%
% < Output >
% M : [1 x N cell array] The result MPS which is obtained
%       variationally to have the minimum expectation value of the
%       Hamiltonian H. It is in *left-canonical* form, since the
%       last sweep is from left to right.
% E0 : [numeric] The energy of M.
% Eiter : [N x (2*Nsweep) numeric array] Each element Eiter(m,n)
%       means the variational energy in the m-th iteration within
%       the n-th sweep. Odd n is for right-to-left sweep and even n
%       for left-to-right sweep. Note that the iteration index m
%       matches with the site index for the left-to-right sweep;
%       the iteration m corresponds to the site (N+1-m) for
%       the right-to-left sweep.
% dw : [N x (2*Nsweep) numeric array] Each element dw(m,n) is the
%	discarded weight in the m-th iteration within
%       the n-th sweep. Odd n is for right-to-left sweep and even n
%       for left-to-right sweep. Note that the iteration index m
%       matches with the site index for the left-to-right sweep;
%       the iteration m corresponds to the site (N+1-m) for
%       the right-to-left sweep.
