function [Ceff,Eeff] = eigs_1site(Hleft,Hloc,Hright,Cinit,varargin)
% < Description >
%
% [Ceff,Eeff] = eigs_1site(Hleft,Hloc,Hright,Cinit [, option])
%
% Obtain the ground state and its energy for the effective 
% Hamiltonian for the site-canonical MPS, by using the Lanczos 
% method.
%
% < Input >
% Hleft, Hloc, Hright: [tensors] The Hamiltonian for the left, site
%	 and right parts of the chain. They form the effective 
%	 Hamiltonian in the site-canonical basis.
% Cinit : [tensor] Ket tensor at a lattice site. It becomes an 
%	initial vector for the Lanczos method.
%
% The input tensors can be visualized as follows:
% (numbers are the order of legs, * means contraction)
%
%
%      	    1 -->-[ Cinit ]-<-- 3
%                     |
%                     ^ 2
%                     |
%
%
%     /--->- 3        | 4        3 -<---\
%     |               ^                 |
%     |    2     1    |    3     2      |
%   Hleft-->- * -->- Hloc->-- * ->-- Hright
%     |               |                 |
%     |               ^                 |
%     \---<- 1        | 2        1 ->---/
%
% < Option >
% 'N', .. : [numeric] Maximum number of Lanczos vectors (in 
%	addition to those given by Cinit) to be considered 
%	for the Krylov subspace.
%       (Default: 5)
% 'minH', .. : [numeric] Minimum absolute value of the 1st diagonal 
%	(i.e., superdiagonal) element of the Hamiltonian in the 
%	Krylov subspace. If a 1st-diagonal element whose absolute 
%	value is smaller than minH is encountered, the iteration 
%	stops. Then the ground-state vector and energy is obtained 
%	from the tridiagonal matrix constructed so far.
%       (Default: 1e-10)
%
% < Output >
% Ceff : [tensor] A ket tensor as the ground state of the 
%	effective Hamiltonian.
% Eeff : [numeric] The energy eigenvalue corresponding to Ceff.

%%
% default parameter
N = 5;
minH = 1e-10;

% parsing option
while ~isempty(varargin)
    switch varargin{1}
        case 'N'
            N = varargin{2};
            varargin(1:2) = [];
        case 'minH'
            minH = varargin{2};
            varargin(1:2) = [];
        otherwise
            error('ERR: check input!');
    end
end

%%
function [M,E0,Eiter] = DMRG_1site(Hs,Minit,Nkeep,Nsweep)
% < Description >
%
% [M,E0,Eiter] = DMRG_1site(Hs,Minit,Nkeep,Nsweep)
%
% Single-site density-matrix renormalization group (DMRG) 
% calculation to search for the ground state and its energy
% of a one-dimensional system, whose Hamiltonian is given 
% by the matrix product operator Hs.
%
% < Input >
% Hs : [1 x N cell array] Matrix product operator (MPO) of the
%	Hamiltonian. Each Hs{n} acts on site n, and is a rank-4
%	tensor. The order of legs of Hs{n} is 
%	left-bottom-right-top, where bottom (top) leg is to be 
%	contracted with bra (ket) tensor. The length N of Hs, 
%	i.e., numel(Hs), determines the chain length.
% Minit : [1 x N cell array] Initial MPS from which to start the 
%	ground state search
% Nkeep : [numeric] Maximum bond dimension of the matrix product 
%	state (MPS) to consider.
% Nsweep : [numeric] Number of sweeps will be 2*Nsweep, as there 
%	are Nsweep times of round trip 
%	(right -> left, left -> right).
%
% < Output >
% M : [1 x N cell array] The result MPS which is obtained 
%	variationally to have the minimum expectation value of the
%	Hamiltonian H. It is in *left-canonical* form, since the 
%	last sweep is from left to right.
% E0 : [numeric] The energy of M.
% Eiter : [N x (2*Nsweep) numeric array] Each element Eiter(m,n) 
%	means the variational energy in the m-th iteration within 
%	the n-th sweep. Odd n is for right-to-left sweep and even n 
%	for left-to-right sweep. Note that the iteration index m 
%	matches with the site index for the left-to-right sweep; 
%	the iteration m corresponds to the site (N+1-m) for 
%	the right-to-left sweep.