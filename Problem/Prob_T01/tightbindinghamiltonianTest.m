% test matrices 
v = 1;
L = 2;

% precondition - check if the Hamiltonian is an LxL matrix 
H = tightbindinghamiltonian(L,v);
assert(isequal(size(H),[L,L]))


%% testing whether only superdiagonal and supdiagonal elements are non-zero 
H = tightbindinghamiltonian(L,v);
for i = 1:L
    assert(H(i,i)==0)
end
for i = 1:L-2
    assert(H(i,i+2)==0)
end

%% hermiticity of the Hamiltonian
H = tightbindinghamiltonian(L,v);
assert(isequal(H',H))