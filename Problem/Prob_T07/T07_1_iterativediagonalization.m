function Inrg = NRG_IterDiag (H0,A0,Lambda,ff,F,gg,NF,Z,Nkeep)
% < Description >
%
% Inrg = NRG_IterDiag (H0,A0,Lambda,ff,F,gg,NF,Z,Nkeep)
%
% Iterative diagonalization of the numerical renormalization group (NRG)
% method. Here the first chain site, associated with the second leg of A0,
% is for the impurity. The second chain site, which is to be included
% along the iterative diagonalization, is the first bath site. The hopping
% between the first and second sites is given by ff(1).
%
% This NRG style of the iterative diagonalization differs from the
% iterative diagonalization covered in earlier tutorial in that (i) the
% Hamiltonian is rescaled by the energy scale factors [see the output
% Irng.EScale below for detail], and (ii) the energy eigenvalues are
% shifted so that the lowest energy eigenvalue becomes zero.
%
%
% < Input >
% H0 : [rank-2 tensor] Impurity Hamiltonian which acts on the space of the
%       third (i.e., right) leg of A0. 
% A0 : [rank-3 tensor] Isometry for the impurity. The first (i.e., left)
%       and second (i.e., bottom) legs span the local spaces, and the third
%       (i.e., right) leg spans the full impurity Hilbert space.
% Lambda : [number] Logarithmic discretization parameter.
% ff : [vector] Hopping amplitudes of the Wilson chain. ff(1) is the
%       hopping between the impurity and the first bath site; ff(2) is
%       the hopping between the first and second bath sites, etc.
% F : [rank-3 tensor] Fermion annihilation operator.
% gg : [vector] On-site energies of the Wilson chain. gg(1) is the
%       on-site energy of the first bath site; gg(2) is the on-site
%       energy of the second bath site, etc.
% NF : [rank-2 tensor] Particle number operator associated with the on-site
%       energy.
% Z : [rank-2 tensor] Fermion anti-commutation sign operator.
% Nkeep : [number] Number of states to be kept. To have better separation
%       of the low-lying kept states and the high-lying discarded states,
%       the truncation threshold is set at the mostly separated states,
%       among the states starting from the Nkeep-th lowest-lying state to
%       the (Nkeep*1.1)-th lowest-lying state. The factor 1.1 is controlled
%       by a variable 'Nfac' defined below.
%
% < Output >
% Inrg : [struct] NRG result.
%   .Lambda : [number] Given by input.
%   .EScale : [vector] Energy scale to rescale the exponentially decaying
%             energy scale. It rescales the last hopping amplitude to be 1.
%   .EK : [cell array] Column vector of kept energy eigenvalues. These
%             energy eigenvalues are rescaled by Inrg.EScale and shifted by
%             Inrg.E0. As the result of shifting, the smallest value of EK
%             is zero.
%   .AK : [cell array] Rank-3 tensor for kept energy eigenstates.
%   .ED : [cell array] Column vector of discarded energy eigenvalues. These
%             energy eigenvalues are rescaled by Inrg.EScale and shifted by
%             Inrg.E0.
%   .AD : [cell array] Rank-3 tensor for discarded energy eigenstates.
%   .E0 : [vector] Ground-state energy at every iteration. Inrg.E0(n) is in
%             the unit of the energy scale given by Inrg.EScale(n).
%   The n-th elements, EScale(n), EK{n}, AK{n}, ED{n}, AD{n}, and E0(n),
%   are associated with the same iteration n. At iteration n, the part of
%   the chain which consists of the impurity and n-1 bath sites is
%   considered.


Nfac = 0.1; % up to 10% more states can be kept

% % error checking
if numel(size(H0)) ~= 2
    error('ERR: ''H0'' should be of rank 2.');
elseif numel(size(A0)) ~= 3
    error('ERR: ''A0'' should be of rank 3.');
end

Inrg = struct; % result
Inrg.Lambda = Lambda;
L = numel(ff)+1; % Length of the Wilson chain

% Rescaling factor (to divide the energy values):
% EScale(1) = 1 (no rescaling for the impurity), EScale(end) rescales
% ff(end) to be 1.
Inrg.EScale = [1, (Lambda.^(((L-2):-1:0)/2))*ff(end)];
