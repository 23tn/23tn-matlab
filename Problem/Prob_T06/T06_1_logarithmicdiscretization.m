function [ff,gg] = doCLD (ozin,RhoV2in,Lambda,L,varargin)
% < Description >
%
% [ff,gg] = doCLD (ozin,RhoV2in,Lambda,L [, option] )
%
% This function performs the Campo--Oliveira logarithmic discretization [V.
% L. Campo and L. N. Oliveira, Phys. Rev. B 72, 104432 (2005)] of the input
% hybridization function (paramterized by the inputs 'ozin' and 'RhoV2in')
% and maps the resulting star-geometry Hamiltonian onto the chain-geometry
% Hamiltonian, the so-called Wilson chain. The output 'ff' and 'gg'
% describe the hopping amplitudes and the on-site energies of the chain.
%
% < Input >
% ozin, RhoV2in : [numeric vector] The values of definining the hybridization
%       function \Gamma(\omega). RhoV2in(n) is the value of the
%       hybridization function at frequency ozin(n). The first and last
%       elements of ozin define the bandwidth of the bath.
% L : [integer] Wilson chain length.
% Lambda : [numeric] Discretization parameter.
%
% < Option >
% 'estep', ... : [numeric] Number of step for resolving frequencies for
%       each discretization interval.
%       (Default: 10)
% 'emax', ... : [numeric] Maximum frequency value in defining the
%       hybridization function.
%       (Default: max(abs(ozin)))
% 'emin', ... : [numeric] Minimum frequency value in defining the
%       hybridization function.
%       (Default: 1e-5*eps)
%
% < Output >
% ff, gg : [numeric vectors] Hopping amplitudes and on-site energies of
%       the Wilson chain, respectively. The hopping amplitudes correspond
%       to the superdiagonals [diag(..,+1) and diag(..,-1)] of the
%       tridiagonal matrix representation of a single-particle Hamiltonian;
%       the on-site energies correspond to the diagonals of the tridiagonal
%       matrix.

%%
% logarithmic grid
xs = ((log(emax)/log(Lambda)*estep):-1:(log(emin)/log(Lambda)*estep))/estep;
xs = flipud(xs(:)); % increasing, column
oz = Lambda.^xs; % increasing
