% To test you code, we take an arbitrary symmetric matrix of 1000x1000
ndim = 1000;
Ham = magic(ndim);
Ham = Ham+Ham';
phi = repmat([1;-1], [ndim/2,1]);
phi = phi/norm(phi);
tau = 0.1i;

%%
function [M,Ovals] = TDVP_1site (M,Hs,O,Nkeep,dt)
% < Description >
%
% [M,Ovals] = TDVP_1site (M,Hs,O,Nkeep,dt)
%
% Time-dependent variational principle (TDVP) method for simulating
% real-time evolution of matrix product state (MPS). The expectation
% values of local operator O for individual sites are evaluated for
% discrete time instances. The 1D chain system is described by the matrix
% product operator (MPO) Hamiltonian Hs.
%
% < Input >
% M : [cell] The initial state as the MPS. The length of M, i.e., numel(M),
%       defines the chain length. The leg convention of M{n} is as follows:
%
%    1      3   1      3         1        3
%   ---M{1}---*---M{2}---* ... *---M{end}---
%       |          |                 |
%       ^2         ^2                ^2
%
% Hs : [cell] MPO description of the Hamiltonian. Each Hs{n} acts on site n
%       , and is rank-4 tensor. The order of legs of Hs{n} is left-bottom-
%       right-top, where bottom (top) leg is to be contracted with bra
%       (ket) tensor:
%
%       |4          |4
%    1  |   3    1  |   3
%   ---Hs{1}---*---Hs{2}---*--- ...
%       |           |
%       |2          |2
%
% O : [matrix] Rank-2 tensor as a local operator acting on a site. The
%       expectation value of this operator at each chain site is to be
%       computed; see the description of the output 'Ovals' for detail.
% Nkeep : [integer] Maximum bond dimension.
% dt : [numeric] Real time step size. Each real-time evolution by step dt
%       consists of one pair of sweeps (left-to-right and right-to-left).
%
% < Output >
% M : [cell] The final MPS after real-time evolution.
% Ovals : [matrix] Ovals(1,n) indicates the expectation value of local 
%       operator O (input) at the site n after the time step dt (input).

%%
%% exact values of magnetization in the infinite chain limit 
%% Ref: T. Antal, et al., Phys.~Rev.~E 59, 4912 (1999)
Lsite = 50;
Tstep = [0:0.05:20];
fvals = zeros(numel(Tstep),Lsite-1);
for it = (1:size(fvals,2))
  fvals(:,it) = (besselj(it-(Lsite/2),Tstep(:))).^2;
end
fvals = -0.5*fvals;
Oexact = zeros(numel(Tstep),Lsite/2); % exact values
for it = (1:(Lsite/2))
  Oexact(:,it) = sum(fvals(:,(Lsite/2-it+1):(it+Lsite/2-1)),2);
end
Oexact = [-fliplr(Oexact),Oexact]; % format:[Time]x[Position]