U = diag([1; 1; 1]);
assert(all(localdensity(U,3) == [1; 1; 1]));
assert(all(localdensity(U,2) == [1; 1; 0]));

U = [0 0 1; 0 1 0; 1 0 0];
assert(all(localdensity(U,3) == [1; 1; 1]));
assert(all(localdensity(U,2) == [0; 1; 1]));