% unit test to test "eigs_1site" function for small system size 
% The Lanczos method and ED results of the ground state energy are compared
% for a tight-binding chain with 3 sites

L = 3; % chain length
N = 2^L; % number of Krylov steps needed to span the full Krylov basis

E_ED = -1.414213562373095; % ground state energy from the ED

Hleft = zeros(2,4,2); Hleft(1,4,1) = 1; Hleft(2,2,1) = -1; Hleft(1,3,2) = -1; Hleft(2,4,2) = 1;
Hright = zeros(2,4,2); Hright(1,1,1) = 1; Hright(2,3,1) = 1; Hright(1,2,2) = 1; Hright(2,1,2) = 1;
Hloc = zeros(4,2,4,2); Hloc(1,1,1,1) = 1; Hloc(3,2,1,1) = 1; Hloc(4,2,2,1) = -1; Hloc(4,1,4,1) = 1;
                       Hloc(1,2,1,2) = 1; Hloc(2,1,1,2) = 1; Hloc(4,1,3,2) = -1; Hloc(4,2,4,2) = 1;
Cinit = zeros(2,2,2); Cinit(1,2,1) = 1/sqrt(2); Cinit(2,1,1) = 0.5; Cinit(1,1,2) = 0.5;

[~,E_LZ] = eigs_1site(Hleft,Hloc,Hright,Cinit,'N',N);

%% comparing the ground state energy from the Lanczos method and the ED
fprintf('|E_LZ - E_ED| : %.4e\n',abs(E_LZ-E_ED));
assert(abs(E_LZ-E_ED)<1e-14);