[U,E] = eig(tightbindinghamiltonian(4,1));

E = diag(E);

for N = 1:4
    for t = [0 0.1 1231 -0.12 -74]
        propag = propagator(E,U,N,t);
        diff = norm(propag.' - propag);
        assert(diff < 1e-14); 
        diff = norm(propagator(E,U,N,-t) - conj(propag));
        assert(diff < 1e-14);
    end
end
