% unit test to test "DMRG_1site" function for small system size 
% The DMRG and ED results of the ground state energy are compared for a
% tight-binding chain with 3 sites

L = 3; % chain length

E_ED = -1.414213562373095; % ground state energy from the ED

W = cell(3,1);
W{1} = zeros(1,2,4,2); W{1}(1,2,2,1) = -1; W{1}(1,1,4,1) = 1; W{1}(1,1,3,2) = -1; W{1}(1,2,4,2) = 1;
W{2} = zeros(4,2,4,2); W{2}(1,1,1,1) = 1; W{2}(3,2,1,1) = 1; W{2}(4,2,2,1) = -1; W{2}(4,1,4,1) = 1;
                       W{2}(1,2,1,2) = 1; W{2}(2,1,1,2) = 1; W{2}(4,1,3,2) = -1; W{2}(4,2,4,2) = 1;
W{3} = zeros(4,2,1,2); W{3}(1,1,1,1) = 1; W{3}(3,2,1,1) = 1; W{3}(1,2,1,2) = 1; W{3}(2,1,1,2) = 1;

MPS = cell(3,1);
MPS{1} = zeros(1,2,2); MPS{1}(1,1,1) = 1; MPS{1}(1,2,2) = 1;
MPS{2} = zeros(2,2,4); MPS{2}(:,:,1) = [0 -1/sqrt(2); -1/sqrt(2) 0]; MPS{2}(1,1,2) = 1; MPS{2}(2,2,3) = 1; MPS{2}(:,:,4) = [0 1/sqrt(2); -1/sqrt(2) 0];
MPS{3} = zeros(4,2); MPS{3}(1,1) = 0.853553390593273; MPS{3}(2,2) = -0.5; MPS{3}(4,1) = -0.146446609406726;

% % 1site DMRG : START % %
Nkeep = 100; % maximal # of states to keep
Nsweep = 100; % number of DMRG sweeps will be 2*Nsweep
Econv = 1e-14; % Convergence criterion for energy
[~,E_DMRG,~] = DMRG_1site(W,MPS,Nkeep,Nsweep,'Econv',Econv);

%% comparing the ground state energy from the DMRG and the ED
fprintf('|E_DMRG - E_ED| : %.4e\n',abs(E_DMRG-E_ED));
assert(abs(E_DMRG-E_ED)<1e-14);












