% unit test to test "doCLD" function
t0 = sqrt(2);
t1 = 0.545287470842630;
t2 = 0.415509468291744;
t3 = 0.321899177676100;

Lambda = 2;
N = 4;
ff = doCLD([ -1 1],[1 1],Lambda,4);

assert(abs(ff(1)-t0)<2e-14);
assert(abs(ff(2)-t1)<2e-14);
assert(abs(ff(3)-t2)<2e-14);
assert(abs(ff(4)-t3)<2e-14);