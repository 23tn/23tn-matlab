[U,~] = eig(tightbindinghamiltonian(4,1));

for i = 1:4
    densdens = densitydensitycorrelator(U,i);
    diff = norm(conj(densdens) - densdens);
    assert(diff < 1e-14);
end