% unit test runner file

runtests('test_getIdentity')
runtests('test_getLocalSpace')
runtests('test_eigs_1site')
runtests('test_DMRG_1site')
runtests('test_DMRG_2site')
runtests('test_doCLD')