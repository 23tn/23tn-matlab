% test tensor 
B = zeros(2,2,2); % rank-3 tensor for tests
B(:,:,1) = [0.8147 0.9134; 0.9058 0.6324];
B(:,:,2) = [0.4854 0.4218; 0.8003 0.9157];
C = [ 0.5211 0.6241; 0.2316 0.6791; 0.4889 0.3955]; % rank-2 tensor for tests


%% testing that the resulting rank-2 identity tensor has appropriate size
for idB = 1:numel(size(B))
    A = getIdentity(B,idB);
    assert(size(A,1)==size(B,idB));
    assert(size(A,2)==size(B,idB));
end

%% testing that the resulting rank-3 identity tensor has appropriate size
A = getIdentity(B,3,C,1); % here we choose specific legs of tensors B and C
assert(size(A,1) == size(B,3));
assert(size(A,2) == size(C,1));
assert(size(A,3) == size(B,3)*size(C,1));
