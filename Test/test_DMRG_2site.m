% unit test to test "DMRG_2site" function for small system size 
% The DMRG and ED results of the ground state energy are compared for a
% tight-binding chain with 4 sites

L = 4; % chain length

E_ED = -2.236067977499789; % ground state energy from the ED

W = cell(4,1);
W{1} = zeros(1,2,4,2); W{1}(1,2,2,1) = -1; W{1}(1,1,4,1) = 1; W{1}(1,1,3,2) = -1; W{1}(1,2,4,2) = 1;

W{2} = zeros(4,2,4,2); W{2}(1,1,1,1) = 1; W{2}(3,2,1,1) = 1; W{2}(4,2,2,1) = -1; W{2}(4,1,4,1) = 1;
                       W{2}(1,2,1,2) = 1; W{2}(2,1,1,2) = 1; W{2}(4,1,3,2) = -1; W{2}(4,2,4,2) = 1;

W{3} = zeros(4,2,4,2); W{3}(1,1,1,1) = 1; W{3}(3,2,1,1) = 1; W{3}(4,2,2,1) = -1; W{3}(4,1,4,1) = 1;
                       W{3}(1,2,1,2) = 1; W{3}(2,1,1,2) = 1; W{3}(4,1,3,2) = -1; W{3}(4,2,4,2) = 1;

W{4} = zeros(4,2,1,2); W{4}(1,1,1,1) = 1; W{4}(3,2,1,1) = 1; W{4}(1,2,1,2) = 1; W{4}(2,1,1,2) = 1;

MPS = cell(L,1); % initial MPS
MPS(:) = {zeros(1,2,1)};
for itL = (1:L)
    if mod(itL,2)
        MPS{itL}(1,1,1) = 1; % occupied states for itL = 1, 3
    else
        MPS{itL}(1,2,1) = 1; % empty states for itL = 2, 4
    end
end

% % 2site DMRG : START % %
alpha = sqrt(2); % bond growth factor
Nkeep = 100; % maximal # of states to keep
Nsweep = 100; % number of DMRG sweeps will be 2*Nsweep
Econv = 1e-14; % Convergence criterion for energy
[~,E_DMRG,~] = DMRG_2site(W,MPS,alpha,Nkeep,Nsweep,'Econv',Econv);

%% comparing the ground state energy from the DMRG and the ED
fprintf('|E_DMRG - E_ED| : %.4e\n',abs(E_DMRG-E_ED));
assert(abs(E_DMRG-E_ED)<1e-14);












