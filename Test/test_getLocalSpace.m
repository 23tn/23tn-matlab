% unit test to test getLocalSpace function for spin 
spins = [0.5, 1, 1.5, 3];


%% test the dimensions of the appropriate tensors 
for s = spins    
    [S,I] = getLocalSpace("Spin", s);
    assert(size(S,1) == round(2*s+1));
    assert(size(S,2) == 3);
    assert(size(S,3) == round(2*s+1));
    for i = 1:2
        assert(size(I,i)==round(2*s+1));
    end
end

%% test the commutation relations for spin operators
for s = spins    
    [S,I] = getLocalSpace("Spin", s);            
    Sx = squeeze((S(:,1,:)+S(:,2,:))/sqrt(2));
    Sy = squeeze((S(:,1,:)-S(:,2,:))/sqrt(2)/1i);
    Sz = squeeze(S(:,3,:));
    assert(all(reshape(abs(commutator(Sx,Sy)-1i*Sz)<1e-14,1,[])));
    assert(all(reshape(abs(commutator(Sy,Sz)-1i*Sx)<1e-14,1,[])));
    assert(all(reshape(abs(commutator(Sz,Sx)-1i*Sy)<1e-14,1,[])));

end


function C = commutator(A,B) 
        C = A*B - B*A;
 end
